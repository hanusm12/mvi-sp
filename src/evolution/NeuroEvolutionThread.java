/*package evolution;

import poker.game.GameRunner;
import poker.game.cardDealer.SpecifiedCardDealerFactory;
import poker.player.Player;
import poker.player.PlayerNetworkForEachStartingHand;
import settings.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class NeuroEvolutionThread implements Callable<Object> {
    private List<Player> players;
    private PlayerNetworkForEachStartingHand playertoTrain;

    public NeuroEvolutionThread(List<Player> players, PlayerNetworkForEachStartingHand playertoTrain) {
        this.players = new ArrayList<>();
        for (Player player : players) {
            if (!(player instanceof PlayerNetworkForEachStartingHand)) {
                //this.players.add(Player.clonePlayer(player));
            } else {
                this.players.add(player);
            }
        }



        this.playertoTrain = playertoTrain;
    }

    @Override
    public Object call() throws Exception {
        GameRunner gameRunner = new GameRunner();
        gameRunner.evaluateSpecifiedAgent(players, playertoTrain, new SpecifiedCardDealerFactory(Settings.HAND1VAL, Settings.HAND2VAL, Settings.SUITED == 1));
        return 0;
    }
}
*/