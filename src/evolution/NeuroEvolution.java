/*package evolution;

import com.sun.org.apache.xpath.internal.operations.Mult;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.params.DefaultParamInitializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.indexaccum.IMax;
import org.nd4j.linalg.api.rng.distribution.impl.NormalDistribution;
import org.nd4j.linalg.factory.Nd4j;
import poker.game.GameRunner;
import poker.game.cardDealer.SpecifiedCardDealerFactory;
import poker.player.Player;
import poker.player.PlayersLoader;
import settings.ExecutorServiceHandler;
import settings.Settings;

import static org.nd4j.linalg.ops.transforms.Transforms.*;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NeuroEvolution {
    List<Player> opponents;

    INDArray testInputs;

    List<Individual> population;

    List<Individual> library;


    double bestWinrate = -Double.MAX_VALUE;
    MultiLayerNetwork bestNetwork = null;


    public NeuroEvolution(List<Player> opponents) {
        this.opponents = new ArrayList<>(opponents);
        this.library = new ArrayList<>();
        generateTestInputs();
    }

    private void generateTestInputs() {
        GameRunner gameRunner = new GameRunner();
        PlayerGeneratingInputs player = new PlayerGeneratingInputs(Settings.playerToTrain);
        gameRunner.simplePlayout(opponents, player, 1000, new SpecifiedCardDealerFactory(Settings.HAND1VAL, Settings.HAND2VAL, Settings.SUITED == 1));
        testInputs = Nd4j.create(player.getGeneratedActions());
    }

    public void nextGeneration() {
        if (population == null) {
            createFirstPopulation();
        }
        doNextGeneration();
    }

    private void doNextGeneration() {
        GameRunner gameRunner = new GameRunner();

        calcFitness();

        Collections.sort(population);

        fillLibrary();

        List<Individual> newPopulation = new ArrayList<>();
        newPopulation.add(new Individual(population.get(0).network.clone()));
        int max_index = (int) (NeuroEvolutionSettings.POPULATION_SIZE * 0.2);

        while (newPopulation.size() < NeuroEvolutionSettings.POPULATION_SIZE) {
            MultiLayerNetwork newNetwork = population.get(Settings.RANDOM.nextInt(max_index)).network.clone();

            Layer[] layers = newNetwork.getLayers();
            for (Layer layer : layers) {
                INDArray bias = layer.getParam(DefaultParamInitializer.BIAS_KEY);
                INDArray weights = layer.getParam(DefaultParamInitializer.WEIGHT_KEY);

                bias.addi(Nd4j.rand(bias.shape(), new NormalDistribution()));
                weights.addi(Nd4j.rand(weights.shape(), new NormalDistribution()));
            }
            newPopulation.add(new Individual(newNetwork));
        }

        System.out.println("Best fitness: " + population.get(0).fitness);
        this.population = newPopulation;


    }

    private void fillLibrary() {

        for (int i = 0; i < population.size(); i++) {
            Individual individual = population.get(i);
            if (individual.fitness > 100) {
                library.add(individual);
            }
        }

    }

    protected void calcFitness() {
       /* List<Future> futures = new ArrayList<>();
        List<Individual> fitness = new ArrayList<>();

        for (int i = 0; i < networks.size(); i++) {
            PlayerNetworkForEachStartingHand newPlayer = new PlayerNetworkForEachStartingHand(Settings.playerToTrain.getName(), new MultiLayerNetwork[13][13][2], Settings.playerToTrain.getObserver(), null);
            newPlayer.getNetworks()[Settings.HAND1VAL - 2][Settings.HAND2VAL - 2][Settings.SUITED] = networks.get(i);
            futures.add(ExecutorServiceHandler.submitTask(new NeuroEvolutionThread(opponents, newPlayer)));
        }

        for (int i = 0; i < futures.size(); i++) {
            if (System.currentTimeMillis() >= Settings.TRAINING_STARTED + Settings.TIME_TO_TRAIN) {
                return null;
            }
            Future future = futures.get(i);
            try {
                Double winrate = (Double) future.get();
                if (winrate > bestWinrate) {
                    bestWinrate = winrate;
                    bestNetwork = networks.get(i);
                }
                fitness.add(new Individual(networks.get(i), winrate));
                System.out.println("Network: " + i + ". Worst possible winrate: " + winrate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }*/
/*

        for (int i = 0; i < population.size(); i++) {
            Individual individual = population.get(i);
            MultiLayerNetwork network = individual.network;
            INDArray output = network.output(testInputs);
            INDArray argMax = Nd4j.getExecutioner().exec(new IMax(output), 1);
            individual.moves = argMax;
        }
        if (!library.isEmpty()) {
            for (Individual individual : population) {
                for (Individual libraryIndividual : library) {
                    INDArray difference = individual.moves.sub(libraryIndividual.moves);
                    INDArray sum = abs(difference, false).sum(0);
                    double distance = sum.getDouble(0);
                    if (distance < individual.fitness) {
                        individual.fitness = distance;
                    }
                }
            }
        }
        if (library.size() == 0) {
            for (int i = 0; i < population.size(); i++) {
                Individual individual = population.get(i);
                for (int j = 0; j < population.size(); j++) {
                    if (i != j) {
                        INDArray difference = individual.moves.sub(population.get(j).moves);
                        INDArray sum = abs(difference, false).sum(0);
                        double distance = sum.getDouble(0);
                        if (distance < individual.fitness) {
                            individual.fitness = distance;
                        }
                    }
                }
            }
        }
    }

    private void createFirstPopulation() {
        population = new ArrayList<>();
        for (int i = 0; i < NeuroEvolutionSettings.POPULATION_SIZE; i++) {
            MultiLayerNetwork network = PlayersLoader.createNetwork(Settings.DEFAULT_OBSERVER_FOR_EACH);
            population.add(new Individual(network));
        }
    }

    class Individual implements Comparable<Individual> {
        public MultiLayerNetwork network;
        public INDArray moves;
        public double fitness = Double.MAX_VALUE;

        public Individual(MultiLayerNetwork network) {
            this.network = network;
        }

        @Override
        public int compareTo(Individual o) {
            return -Double.compare(fitness, o.fitness);
        }
    }
}
*/