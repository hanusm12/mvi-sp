/*
package evolution;

import agent.Agent;
import environment.EnvironmentRunner;
import neuralnetwork.NeuralNetworks;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import poker.game.GameRunner;
import poker.player.Player;
import settings.ExecutorServiceHandler;
import settings.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class EvolutionGameRunner extends GameRunner {

    public void train(Object context, Player agent, int steps) {
        Settings.playerToTrain = agent;
        Settings.TRAINING_STARTED = System.currentTimeMillis();
        List<Player> opponents = (List<Player>) context;
        NeuroEvolution neuroEvolution = new NeuroEvolution(opponents);
        for (int i = 0; i < 50; i++) {
            neuroEvolution.nextGeneration();
            System.out.println("Generation " + i + " generated.");
            System.out.println("Library size: " + neuroEvolution.library.size());
        }

        findBestPlayer(neuroEvolution.library, opponents);

        PlayerNetworkForEachStartingHand plr = (PlayerNetworkForEachStartingHand) agent;
        plr.getNetworks()[Settings.HAND1VAL - 2][Settings.HAND2VAL - 2][Settings.SUITED] = neuroEvolution.bestNetwork;

    }

    private void findBestPlayer(List<NeuroEvolution.Individual> population, List<Player> opponents) {
        List<Future> futures = new ArrayList<>();

        for (int i = 0; i < population.size(); i++) {
            PlayerNetworkForEachStartingHand newPlayer = new PlayerNetworkForEachStartingHand(Settings.playerToTrain.getName(), new MultiLayerNetwork[13][13][2], Settings.playerToTrain.getObserver(), null);
            newPlayer.getNetworks()[Settings.HAND1VAL - 2][Settings.HAND2VAL - 2][Settings.SUITED] = population.get(i).network;
            futures.add(ExecutorServiceHandler.submitTask(new NeuroEvolutionThread(opponents, newPlayer)));
        }

        for (int i = 0; i < futures.size(); i++) {
            Future future = futures.get(i);
            try {
                future.get();
                //Double winrate = (Double) future.get();
                //population.get(i).fitness = winrate;
                //System.out.println("Network: " + i + ". Worst possible winrate: " + winrate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
*/