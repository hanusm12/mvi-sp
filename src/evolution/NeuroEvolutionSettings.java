package evolution;

public class NeuroEvolutionSettings {
    public static int POPULATION_SIZE = 20;
    public static int TABLES_TO_PLAY = 10000;
}
