package reinforcementLearning;

public enum ReinforcementLearningType {
    Q_LEARNING_ONE_STEP_SYNC, Q_LEARNING_N_STEP_SYNC
}
