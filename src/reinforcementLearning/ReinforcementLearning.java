package reinforcementLearning;

import environment.EnvironmentFactory;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;

/**
 * Created by Marek on 30/06/2017.
 */
public interface ReinforcementLearning {
    SelectActionResult selectAction(double[] currentState);
    void moveEnded(Experience experience);
    void epochEnded(Experience experiences);
    int getCount();
    void train(EnvironmentFactory environmentFactory);
    ReinforcementLearning clone();
}
