package reinforcementLearning;

import org.nd4j.linalg.api.ndarray.INDArray;

public class SelectActionResult {
    public int action;
    public INDArray outputs[];

    public SelectActionResult(int action, INDArray[] outputs) {
        this.action = action;
        this.outputs = outputs;
    }
}
