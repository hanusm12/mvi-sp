package reinforcementLearning.sync.qLearning.oneStep;

import agent.Agent;
import agent.deep4j.Deep4jAgentAI;
import environment.Environment;
import environment.EnvironmentFactory;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.SelectActionResult;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.*;
import settings.Settings;
import settings.Utils;

import java.util.Random;

/**
 * Created by Marek on 30/06/2017.
 */
public class QLearningOneStepSync implements ReinforcementLearning {
    protected int count = 0;
    protected double explorationRatio;

    private ExperienceReplay experienceReplay;

    protected double decreaseExplorationRatioBy;
    protected int intervalToDecreaseExplorationRatio;
    protected long lastExplorationRatioUpdate;

    private Random random;
    protected MultiLayerNetwork currentNetwork;
    protected MultiLayerNetwork targetNetwork;

    protected int lastTargetNetworkUpdate = 0;

    public QLearningOneStepSync() {
        this.random = new Random();
        this.explorationRatio = QLearningOneStepSyncSettings.EXPLORATION_RATIO_MAX;
        this.decreaseExplorationRatioBy = (QLearningOneStepSyncSettings.EXPLORATION_RATIO_MAX - QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN) / (double) 100;
        this.intervalToDecreaseExplorationRatio = (int) ((Settings.TIME_TO_TRAIN * QLearningOneStepSyncSettings.SPEND_EXPLORING) / 100);


        switch (QLearningOneStepSyncSettings.EXPERIENCE_REPLAY_TYPE) {
            case RANDOM: {
                this.experienceReplay = new RandomExperienceReplay();
            }
            break;
            case MY_TYPE: {
                this.experienceReplay = new MyExperienceReplay();
            }
            break;
            case RANK_BASED: {
                throw new UnsupportedOperationException();
            }
            //break;
            case PROPORTIONAL: {
                this.experienceReplay = new ProportionalExperienceReplay();
            }
            break;
        }
    }

    @Override
    public SelectActionResult selectAction(double[] currentState) {
        int selectedAction;
        INDArray outputs[] = new INDArray[]{currentNetwork.output(Nd4j.create(currentState))};
        if (random.nextDouble() < explorationRatio) {
            selectedAction = random.nextInt(Settings.DEFAULT_OBSERVER.getNumberOfNetworkOutputs());
        } else {
            selectedAction = Utils.getIndexOfBiggest(outputs[0]);
        }
        return new SelectActionResult(selectedAction, outputs);
    }

    @Override
    public void moveEnded(Experience experience) {
        storeExperience(experience);
        if (canTrain()) {
            currentNetwork.fit(getTrainingData());
        }
    }

    @Override
    public void epochEnded(Experience experience) {
        storeExperience(experience);
        if (canTrain()) {
            currentNetwork.fit(getTrainingData());
            //Settings.score.addValue(network.score());
            //System.out.println(network.score());
        }
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void train(EnvironmentFactory environmentFactory) {
        long start = System.currentTimeMillis();
        Agent agentToTrain = environmentFactory.getAgentToTrain();
        agentToTrain.setTrain(true);
        currentNetwork = ((Deep4jAgentAI) agentToTrain.getAI()).getNetwork();
        targetNetwork = currentNetwork.clone();


        Environment environment = environmentFactory.environment();
        while ((System.currentTimeMillis() - start) < Settings.TIME_TO_TRAIN) {
            Agent agentToAct = environment.getAgentToAct();
            int action = agentToAct.getAction(environment);
            environment.playSingleMove(action);
            if (environment.needNewInstance()) {
                environment = environmentFactory.environment();
            }
            if (count % 100 == 0 && agentToAct.equals(agentToTrain)) {
                System.out.println("Step: " + count);
            }
        }

        agentToTrain.setTrain(false);
        long end = System.currentTimeMillis();
        System.out.println("Finished training with speed: " + (count * 1000 / (double) (end - start)) + " steps/ms");
    }

    @Override
    public ReinforcementLearning clone() {
        throw new UnsupportedOperationException("Not implemented");
    }

    private void storeExperience(Experience experience) {
        if (experience.currentState[12] == 1 && experience.currentState[25] == 1 && experience.currentState[28] == 1 && experience.currentState[45] == 1) {
            System.out.println(experience.outputs[0]);
        }
        experienceReplay.storeExperience(experience);

        count++;
        if (count == QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES) {
            targetNetwork = currentNetwork.clone();
            lastTargetNetworkUpdate = count;
            lastExplorationRatioUpdate = System.currentTimeMillis();
        }

        checkIfExplorationRatioShouldBeUpdated();
        checkIfTargetNetworkShouldBeUpdated();
    }


    protected void checkIfTargetNetworkShouldBeUpdated() {
        if (count > QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES && count == lastTargetNetworkUpdate + QLearningOneStepSyncSettings.INTERVAL_TO_UPDATE_TARGET_NETWORK) {
            targetNetwork = currentNetwork.clone();
            lastTargetNetworkUpdate = count;
        }
    }

    protected void checkIfExplorationRatioShouldBeUpdated() {
        if (count > QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES && System.currentTimeMillis() >= lastExplorationRatioUpdate + intervalToDecreaseExplorationRatio) {
            explorationRatio -= decreaseExplorationRatioBy;
            if (explorationRatio <= QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN) {
                explorationRatio = QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN;
            }
            lastExplorationRatioUpdate = System.currentTimeMillis();
            System.out.println("Exploiration ratio updated to: " + explorationRatio);
        }
    }

    private DataSet getTrainingData() {
        Experience selectedExperiences[] = experienceReplay.getTrainingExperiences(explorationRatio);

        double futureStateInputsArray[][] = new double[selectedExperiences.length][];
        double currentStateInputsArray[][] = new double[selectedExperiences.length][];

        for (int i = 0; i < selectedExperiences.length; i++) {
            if (selectedExperiences[i].futureState == null) {
                futureStateInputsArray[i] = selectedExperiences[i].currentState;
            } else {
                futureStateInputsArray[i] = selectedExperiences[i].futureState;
            }
            currentStateInputsArray[i] = selectedExperiences[i].currentState;
        }

        INDArray futureStateInputs = Nd4j.create(futureStateInputsArray);
        INDArray currentStateInputs = Nd4j.create(currentStateInputsArray);

        INDArray currentStateOutputsOriginal = currentNetwork.output(currentStateInputs, false);
        INDArray bestFutureValues = null;
        switch (QLearningOneStepSyncSettings.Q_LEARNING_TYPE) {
            case SIMPLE: {
                INDArray futureStateOutputsOriginal = currentNetwork.output(futureStateInputs, false);
                bestFutureValues = Nd4j.max(futureStateOutputsOriginal, 1);
            }
            break;
            case TARGET_NETWORK: {
                INDArray futureStateOutputsTarget = targetNetwork.output(futureStateInputs, false);
                bestFutureValues = Nd4j.max(futureStateOutputsTarget, 1);
            }
            break;
            case DOUBLE_Q_NETWORK: {
                INDArray futureStateOutputsOriginal = currentNetwork.output(futureStateInputs, false);
                INDArray bestActions = Nd4j.argMax(futureStateOutputsOriginal, 1);
                INDArray futureStateOutputsTarget = targetNetwork.output(futureStateInputs, false);
                bestFutureValues = Nd4j.create(1, selectedExperiences.length);
                for (int i = 0; i < selectedExperiences.length; i++) {
                    bestFutureValues.putScalar(0, i, futureStateOutputsTarget.getDouble(i, bestActions.getInt(i)));
                }
            }
            break;
        }


        for (int i = 0; i < selectedExperiences.length; i++) {
            Experience experience = selectedExperiences[i];

            double targetValue = experience.immediateReward;
            if (experience.futureState != null) {
                targetValue += QLearningOneStepSyncSettings.DISCOUNT_FACTOR * bestFutureValues.getDouble(i);
            }

            experience.priority = Math.pow(currentStateOutputsOriginal.getDouble(i, experience.selectedAction) - targetValue, 2);

            currentStateOutputsOriginal.putScalar(i, experience.selectedAction, targetValue);
        }

        experienceReplay.updatePriorities(selectedExperiences);
        DataSet dataSet = new DataSet(currentStateInputs, currentStateOutputsOriginal);

        return dataSet;
    }

    private boolean canTrain() {
        return count >= QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES;
    }

}
