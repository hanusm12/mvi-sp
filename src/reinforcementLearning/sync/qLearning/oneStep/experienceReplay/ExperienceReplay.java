package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

/**
 * Created by Marek on 11/07/2017.
 */
public interface ExperienceReplay {
    Experience[] getTrainingExperiences(double explorationRatio);
    void storeExperience(Experience experience);
    void updatePriorities(Experience[] experiences);
}
