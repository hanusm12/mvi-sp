package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

/**
 * Created by Marek on 11/07/2017.
 */
public class MyExperienceReplay extends RandomExperienceReplay {

    public MyExperienceReplay() {
        super();
    }

    @Override
    public Experience[] getTrainingExperiences(double explorationRatio) {
        Experience trainingExperiences[] = super.getTrainingExperiences(explorationRatio);

        int minimumEndStates = (int) (trainingExperiences.length * explorationRatio);
        int endStates = 0;
        for (Experience experience : trainingExperiences) {
            if (experience.futureState == null) {
                endStates++;
            }
        }
        int endStatesToAdd = minimumEndStates - endStates;
        int index = 0;
        while (endStatesToAdd > 0) {
            if (trainingExperiences[index].futureState != null) {
                while (trainingExperiences[index].futureState != null) {
                    trainingExperiences[index] = experiences[random.nextInt(experiences.length)];
                }
                endStatesToAdd--;
            }
            index++;
        }

        return trainingExperiences;

    }
}
