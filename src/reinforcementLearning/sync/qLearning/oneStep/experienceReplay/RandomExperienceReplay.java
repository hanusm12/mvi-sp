package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSyncSettings;

import java.util.Random;

/**
 * Created by Marek on 11/07/2017.
 */
public class RandomExperienceReplay implements ExperienceReplay {
    private int currentNewExperienceIndex = 0;
    protected Experience experiences[];

    protected Random random;

    public RandomExperienceReplay() {
        experiences = new Experience[QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES];
        random = new Random();
    }

    @Override
    public Experience[] getTrainingExperiences(double explorationRatio) {
        Experience[] trainingExperiences = new Experience[QLearningOneStepSyncSettings.BATCH_SIZE];
        for (int i = 0; i < trainingExperiences.length; i++) {
            trainingExperiences[i] = experiences[random.nextInt(experiences.length)];
        }
        return trainingExperiences;
    }

    @Override
    public void storeExperience(Experience experience) {
        experiences[currentNewExperienceIndex] = experience;
        currentNewExperienceIndex++;
        if (currentNewExperienceIndex >= experiences.length) {
            currentNewExperienceIndex = 0;
        }
    }

    @Override
    public void updatePriorities(Experience[] experiences) {

    }
}
