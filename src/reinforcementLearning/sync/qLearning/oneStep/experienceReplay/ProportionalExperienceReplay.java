package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSyncSettings;

import java.util.Random;

/**
 * This experience replay is based on https://jaromiru.com/2016/11/07/lets-make-a-dqn-double-learning-and-prioritized-experience-replay/
 * and has some differences from original paper
 */
public class ProportionalExperienceReplay implements ExperienceReplay {
    private static final double EPSILON = 0.01;
    private static final double ALPHA = 0.6;

    private Object[] tree;
    private SelectedExperiences[] indexesOfSelectedExperiences;

    private int currentExperienceIndex;
    private boolean experienceFilled;

    private Random random;

    public ProportionalExperienceReplay() {
        this.tree = new Object[2 * QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1];
        for (int i = 0; i < QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1; i++) {
            this.tree[i] = (double) 0;
        }
        for (int i = QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1; i < this.tree.length; i++) {
            this.tree[i] = new Experience(null, null, null, 0, 0);
        }
        this.indexesOfSelectedExperiences = new SelectedExperiences[QLearningOneStepSyncSettings.BATCH_SIZE];
        for (int i = 0; i < indexesOfSelectedExperiences.length; i++) {
            this.indexesOfSelectedExperiences[i] = new SelectedExperiences();
        }
        this.random = new Random();
        this.currentExperienceIndex = QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1;
    }

    @Override
    public Experience[] getTrainingExperiences(double explorationRatio) {
        Experience result[] = new Experience[indexesOfSelectedExperiences.length];
        for (int i = 0; i < indexesOfSelectedExperiences.length; i++) {
            double selectedPriority = random.nextDouble() * (double) tree[0];
            int currentIndex = 0;
            while (currentIndex < QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1) {
                int leftChild = currentIndex * 2 + 1;

                if (leftChild < QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1) {
                    //NOT leaf, continue recursion
                    if ((double) tree[leftChild] > selectedPriority) {
                        currentIndex = leftChild;
                    } else {
                        currentIndex = leftChild + 1;
                        selectedPriority -= (double) tree[leftChild];
                    }
                } else {
                    //Leaf, return
                    if (((Experience) tree[leftChild]).priority > selectedPriority) {
                        currentIndex = leftChild;
                    } else {
                        currentIndex = leftChild + 1;
                    }
                }
            }
            if (!contains(result, (Experience) tree[currentIndex])) {
                result[i] = (Experience) tree[currentIndex];
                indexesOfSelectedExperiences[i].priority = ((Experience) tree[currentIndex]).priority;
                indexesOfSelectedExperiences[i].index = currentIndex;
            } else {
                i--;
            }
        }
        return result;
    }

    private boolean contains(Experience[] result, Experience experience) {
        for (int i = 0; i < result.length; i++) {
            Experience res = result[i];
            if (res == null) {
                return false;
            }
            if (res == experience) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void storeExperience(Experience experience) {
        if ((double) tree[0] < 0) {
            System.out.println("a");
        }
        if (!experienceFilled) {
            experience.priority = Math.pow(Math.pow(experience.immediateReward, 2) + EPSILON, ALPHA);
        } else {
            //Experience is new, add it average priority
            double averagePriority = (double) tree[0] / (double) QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES;
            experience.priority = averagePriority;
        }

        double priorityDelta = experience.priority;
        if (tree[currentExperienceIndex] != null) {
            priorityDelta = experience.priority - ((Experience) tree[currentExperienceIndex]).priority;
        }
        tree[currentExperienceIndex] = experience;

        backPropagatePriority(priorityDelta, currentExperienceIndex);

        currentExperienceIndex++;
        if (currentExperienceIndex >= tree.length) {
            currentExperienceIndex = QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES - 1;
            experienceFilled = true;
        }
    }

    @Override
    public void updatePriorities(Experience[] experiences) {
        for (int i = 0; i < experiences.length; i++) {
            //First, calculate priority from error
            experiences[i].priority = Math.pow(experiences[i].priority + EPSILON, ALPHA);
            //calculate difference
            double priorityDelta = experiences[i].priority - indexesOfSelectedExperiences[i].priority;
            backPropagatePriority(priorityDelta, indexesOfSelectedExperiences[i].index);

            if ((double) tree[0] < 0) {
                System.out.println("a");
            }
        }
    }

    private void backPropagatePriority(double priorityDelta, int index) {
        int correctTreeIndex = (index - 1) / 2;
        while (correctTreeIndex > 0) {
            if (tree[correctTreeIndex * 2 + 1] instanceof Experience) {
                tree[correctTreeIndex] = ((Experience) tree[correctTreeIndex * 2 + 1]).priority + ((Experience) tree[correctTreeIndex * 2 + 2]).priority;
            } else {
                tree[correctTreeIndex] = (double) tree[correctTreeIndex * 2 + 1] + (double) tree[correctTreeIndex * 2 + 2];
            }
            correctTreeIndex = (correctTreeIndex - 1) / 2;
        }
        tree[0] = (double) tree[1] + (double) tree[2];
    }

    class SelectedExperiences {
        public int index;
        public double priority;
    }
}
