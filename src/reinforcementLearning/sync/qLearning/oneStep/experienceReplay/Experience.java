package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

import org.nd4j.linalg.api.ndarray.INDArray;

/**
 * Created by Marek on 11/07/2017.
 */
public class Experience {
    public double[] currentState;
    public double[] futureState;
    public INDArray[] outputs;
    public int selectedAction;
    public double immediateReward;
    public double priority;

    public Experience(double[] currentState, double[] futureState, INDArray[] outputs, int selectedAction, double immediateReward) {
        this.currentState = currentState;
        this.futureState = futureState;
        this.outputs = outputs;
        this.selectedAction = selectedAction;
        this.immediateReward = immediateReward;
    }
}
