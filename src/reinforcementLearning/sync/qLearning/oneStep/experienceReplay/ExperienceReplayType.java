package reinforcementLearning.sync.qLearning.oneStep.experienceReplay;

/**
 * Created by Marek on 11/07/2017.
 */
public enum ExperienceReplayType {
    RANDOM, PROPORTIONAL, RANK_BASED, MY_TYPE
}
