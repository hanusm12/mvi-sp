package reinforcementLearning.sync.qLearning.oneStep;

/**
 * Created by Marek on 10/07/2017.
 */
public enum QLearningOneStepSyncType {
    SIMPLE, TARGET_NETWORK, DOUBLE_Q_NETWORK
}
