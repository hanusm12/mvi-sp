package reinforcementLearning.sync.qLearning.oneStep;

import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.ExperienceReplayType;

/**
 * Created by Marek on 30/06/2017.
 */
public class QLearningOneStepSyncSettings {
    public static double SPEND_EXPLORING = 0.8;
    public static double EXPLORATION_RATIO_MAX = 1;
    public static double EXPLORATION_RATIO_MIN = 0.01;
    public static double DISCOUNT_FACTOR = 0.99;
    public static QLearningOneStepSyncType Q_LEARNING_TYPE = QLearningOneStepSyncType.SIMPLE;
    public static int INTERVAL_TO_UPDATE_TARGET_NETWORK = 1000;

    public static ExperienceReplayType EXPERIENCE_REPLAY_TYPE = ExperienceReplayType.RANDOM;


    public static int BATCH_SIZE = 16;
    public static int MAXIMUM_EXPERIENCES = 2500;
}


