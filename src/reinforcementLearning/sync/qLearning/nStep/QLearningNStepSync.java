package reinforcementLearning.sync.qLearning.nStep;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSync;
import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSyncSettings;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;

import java.util.ArrayList;
import java.util.List;

public class QLearningNStepSync extends QLearningOneStepSync {
    protected List<Experience> experiences;

    public QLearningNStepSync() {
        super();
        this.experiences = new ArrayList<>();
        lastExplorationRatioUpdate = System.currentTimeMillis();

    }

    @Override
    public void moveEnded(Experience experience) {
        experiences.add(experience);
    }

    @Override
    public void epochEnded(Experience experience) {
        experiences.add(experience);

        double[][] inputs = new double[experiences.size()][];
        INDArray targets = Nd4j.create(experiences.size(), experience.outputs[0].columns());
        double reward = 0;

        for (int i = experiences.size() - 1; i >= 0; i--) {
            Experience exp = experiences.get(i);
            reward += exp.immediateReward;


            inputs[i] = exp.currentState;
            exp.outputs[0].putScalar(exp.selectedAction, reward);
            targets.putRow(i, exp.outputs[0]);
        }

        fit(Nd4j.create(inputs), targets);

        count++;
        if (count == QLearningOneStepSyncSettings.MAXIMUM_EXPERIENCES) {
            targetNetwork = currentNetwork.clone();
            lastTargetNetworkUpdate = count;
        }

        checkIfExplorationRatioShouldBeUpdated();
        checkIfTargetNetworkShouldBeUpdated();
        experiences = new ArrayList<>();
    }

    @Override
    protected void checkIfExplorationRatioShouldBeUpdated() {
        if (System.currentTimeMillis() >= lastExplorationRatioUpdate + intervalToDecreaseExplorationRatio) {
            explorationRatio -= decreaseExplorationRatioBy;
            if (explorationRatio <= QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN) {
                explorationRatio = QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN;
            }
            lastExplorationRatioUpdate = System.currentTimeMillis();
        }
    }

    protected void fit(INDArray inputs, INDArray targets) {
        currentNetwork.fit(inputs, targets);
    }



}
