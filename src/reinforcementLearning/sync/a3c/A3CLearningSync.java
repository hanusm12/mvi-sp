package reinforcementLearning.sync.a3c;

import agent.Agent;
import agent.deep4j.Deep4jAgentAIActorCritic;
import environment.Environment;
import environment.EnvironmentFactory;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.SelectActionResult;
import reinforcementLearning.exploration.EGreedyExploration;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;
import settings.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.nd4j.linalg.ops.transforms.Transforms.abs;

public class A3CLearningSync implements ReinforcementLearning {
    protected List<List<Experience>> experiences;
    protected List<Experience> currentEpochExperiences;
    protected ComputationGraph network;
    protected EGreedyExploration exploration;
    protected int storedExperiences = 0;
    private Random random = new Random();


    protected int count = 0;

    public A3CLearningSync() {
        this.experiences = new ArrayList<>();
        this.currentEpochExperiences = new ArrayList<>();
        this.exploration = new EGreedyExploration();
    }

    @Override
    public SelectActionResult selectAction(double[] currentState) {
        INDArray[] output = network.output(Nd4j.create(currentState));
        if (random.nextDouble() < exploration.getExplorationRatio()) {
            return new SelectActionResult(random.nextInt(output[1].columns()), output);
        }
        double val = random.nextDouble();
        for (int i = 0; i < output[1].length(); i++) {
            if (val < output[1].getDouble(i)) {
                return new SelectActionResult(i, output);
            } else {
                val -= output[1].getDouble(i);
            }
        }
        throw new RuntimeException("There was an error somewhere");
    }

    @Override
    public void moveEnded(Experience experience) {
        currentEpochExperiences.add(experience);
    }

    @Override
    public void epochEnded(Experience experience) {
        currentEpochExperiences.add(experience);
        experiences.add(currentEpochExperiences);
        storedExperiences += currentEpochExperiences.size();
        currentEpochExperiences = new ArrayList<>();

        if (storedExperiences > 0) {


            double[][] inputs = new double[storedExperiences][];
            INDArray criticLabels = Nd4j.create(storedExperiences, 1);
            INDArray actorLabels = Nd4j.create(storedExperiences, experience.outputs[1].columns());

            int index = storedExperiences - 1;
            for (int i = experiences.size() - 1; i >= 0; i--) {
                List<Experience> currExp = this.experiences.get(i);
                double reward = 0;
                for (int j = currExp.size() - 1; j >= 0; j--) {

                    Experience exp = currExp.get(j);


                    reward += exp.immediateReward;

                    inputs[index] = exp.currentState;

                    criticLabels.putScalar(index, reward);

                    double expectedV = exp.outputs[0].getDouble(0);
                    double advantage = reward - expectedV;

                    actorLabels.putScalar(index, exp.selectedAction, advantage);

                    index--;
                }
            }

            INDArray inputsINDArray = Nd4j.create(inputs);
            fit(new INDArray[]{inputsINDArray}, new INDArray[]{criticLabels, actorLabels});

            exploration.countIncrease();
            count++;
            experiences = new ArrayList<>();
            storedExperiences = 0;
        }


    }

    protected void fit(INDArray[] inputs, INDArray[] labels) {
        network.fit(inputs, labels);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void train(EnvironmentFactory environmentFactory) {
        long start = System.currentTimeMillis();

        Agent agentToTrain = environmentFactory.getAgentToTrain();

        agentToTrain.setTrain(true);
        network = ((Deep4jAgentAIActorCritic) agentToTrain.getAI()).getNetwork();

        Environment environment = environmentFactory.environment();
        while (System.currentTimeMillis() - start < Settings.TIME_TO_TRAIN) {
            Agent agentToAct = environment.getAgentToAct();
            int action = agentToAct.getAction(environment);
            environment.playSingleMove(action);
            if (environment.needNewInstance()) {
                environment = environmentFactory.environment();
            }
            if (count % 100 == 0 && agentToAct.equals(agentToTrain)) {
                System.out.println("Step: " + count);
            }
        }

        agentToTrain.setTrain(false);
        long end = System.currentTimeMillis();
        System.out.println("Finished training with speed: " + (count * 1000 / (double) (end - start)) + " steps/ms");
    }

    @Override
    public ReinforcementLearning clone() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
