package reinforcementLearning.exploration;

import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSyncSettings;
import settings.Settings;

public class EGreedyExploration {

    private long start;
    private double explorationRatio;
    private double decreaseExplorationRatioBy;
    private int intervalToDecreaseExplorationRatio;
    protected long lastExplorationRatioUpdate;

    public EGreedyExploration() {
        this.explorationRatio = QLearningOneStepSyncSettings.EXPLORATION_RATIO_MAX;
        this.decreaseExplorationRatioBy = (QLearningOneStepSyncSettings.EXPLORATION_RATIO_MAX - QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN) / (double) 100;
        this.intervalToDecreaseExplorationRatio = (int) ((Settings.TIME_TO_TRAIN * QLearningOneStepSyncSettings.SPEND_EXPLORING) / 100);
        this.lastExplorationRatioUpdate = System.currentTimeMillis();
        this.start = System.currentTimeMillis();
    }

    public double getExplorationRatio() {
        return explorationRatio;
    }

    public void countIncrease() {
        if (System.currentTimeMillis() - lastExplorationRatioUpdate > intervalToDecreaseExplorationRatio) {
            explorationRatio -= decreaseExplorationRatioBy;
            if (explorationRatio <= QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN) {
                explorationRatio = QLearningOneStepSyncSettings.EXPLORATION_RATIO_MIN;
            }
            lastExplorationRatioUpdate = System.currentTimeMillis();
        }

    }
}
