package reinforcementLearning.async.a3c;

import agent.Agent;
import agent.deep4j.Deep4jAgentAIActorCritic;
import environment.EnvironmentFactory;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.gradient.Gradient;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.async.AgentWorker;
import reinforcementLearning.sync.a3c.A3CLearningSync;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;
import settings.ExecutorServiceHandler;
import settings.Settings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

public class A3CLearningAsync extends A3CLearningSync {
    private ComputationGraph globalNetwork;
    private static ReentrantLock lock = new ReentrantLock();

    public A3CLearningAsync() {
        super();
    }

    public A3CLearningAsync(ComputationGraph globalNetwork) {
        super();
        this.globalNetwork = globalNetwork;
        this.network = globalNetwork.clone();
    }

    @Override
    public ReinforcementLearning clone() {
        return new A3CLearningAsync(globalNetwork);
    }

    @Override
    public void epochEnded(Experience experience) {
        super.epochEnded(experience);
        lock.lock();
        network = globalNetwork.clone();
        lock.unlock();
    }

    @Override
    protected void fit(INDArray[] inputs, INDArray[] labels) {
        Gradient[] gradients = calculateGradient(inputs[0], labels, network);

        lock.lock();
        applyGradient(gradients, storedExperiences, globalNetwork);
        lock.unlock();
    }

    private Gradient[] calculateGradient(INDArray input, INDArray labels[], ComputationGraph cg) {
        cg.setInput(0, input);
        cg.setLabels(labels);
        cg.computeGradientAndScore();
        Collection<IterationListener> iterationListeners = cg.getListeners();
        if (iterationListeners != null && iterationListeners.size() > 0) {
            for (IterationListener l : iterationListeners) {
                if (l instanceof TrainingListener) {
                    ((TrainingListener) l).onGradientCalculation(cg);
                }
            }
        }
        return new Gradient[]{cg.gradient()};
    }

    private void applyGradient(Gradient[] gradient, int batchSize, ComputationGraph cg) {
        ComputationGraphConfiguration cgConf = cg.getConfiguration();
        int iterationCount = cgConf.getIterationCount();
        int epochCount = cgConf.getEpochCount();
        cg.getUpdater().update(gradient[0], iterationCount, epochCount, batchSize);
        cg.params().subi(gradient[0].gradient());
        Collection<IterationListener> iterationListeners = cg.getListeners();
        if (iterationListeners != null && iterationListeners.size() > 0) {
            for (IterationListener listener : iterationListeners) {
                listener.iterationDone(cg, iterationCount, epochCount);
            }
        }
        cgConf.setIterationCount(iterationCount + 1);
    }

    @Override
    public void train(EnvironmentFactory environmentFactory) {
        long start = System.currentTimeMillis();
        Agent agentToTrain = environmentFactory.getAgentToTrain();
        agentToTrain.setTrain(true);
        globalNetwork = ((Deep4jAgentAIActorCritic) agentToTrain.getAI()).getNetwork();
        int numberOfAgents = Settings.THREADS;

        List<Future> workers = new ArrayList<>();
        List<AgentWorker> agentWorkers = new ArrayList<>();
        for (int i = 0; i < numberOfAgents; i++) {
            AgentWorker agentWorker = new AgentWorker(environmentFactory.clone(), i);
            agentWorkers.add(agentWorker);
            Future<?> future = ExecutorServiceHandler.submitTask(agentWorker);
            workers.add(future);
        }

        ExecutorServiceHandler.waitForTasksToFinish(workers);

        int stepsTrained = 0;
        for (AgentWorker agentWorker : agentWorkers) {
            stepsTrained += agentWorker.getEnvironmentFactory().getAgentToTrain().getAI().getReinforcementLearning().getCount();
        }


        globalNetwork = null;
        agentToTrain.setTrain(false);
        long end = System.currentTimeMillis();
        System.out.println("Finished training with speed: " + (stepsTrained * 1000 / (double) (end - start)) + " steps/ms");
    }
}
