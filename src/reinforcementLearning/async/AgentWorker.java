package reinforcementLearning.async;

import agent.Agent;
import environment.Environment;
import environment.EnvironmentFactory;
import reinforcementLearning.ReinforcementLearning;
import settings.Settings;

public class AgentWorker implements Runnable {
    private int index;
    private EnvironmentFactory environmentFactory;

    public AgentWorker(EnvironmentFactory environmentFactory, int index) {
        this.environmentFactory = environmentFactory;
        this.index = index;
    }

    public EnvironmentFactory getEnvironmentFactory() {
        return environmentFactory;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        Agent agentToTrain = environmentFactory.getAgentToTrain();
        Environment environment = environmentFactory.environment();
        ReinforcementLearning reinforcementLearning = agentToTrain.getAI().getReinforcementLearning();
        while (System.currentTimeMillis() - start < Settings.TIME_TO_TRAIN) {
            Agent agentToAct = environment.getAgentToAct();
            int action = agentToAct.getAction(environment);
            environment.playSingleMove(action);
            if (environment.needNewInstance()) {
                environment = environmentFactory.environment();
            }
            if (reinforcementLearning.getCount() % 100 == 0 && agentToAct.equals(agentToTrain)) {
                System.out.println("Worker: " + index + ". Step: " + reinforcementLearning.getCount());
            }
        }
    }
}
