package reinforcementLearning.async.qLearning;

import agent.Agent;
import agent.deep4j.Deep4jAgentAI;
import environment.EnvironmentFactory;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.gradient.Gradient;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.SelectActionResult;
import reinforcementLearning.async.AgentWorker;
import reinforcementLearning.sync.qLearning.nStep.QLearningNStepSync;
import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSyncSettings;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;
import settings.ExecutorServiceHandler;
import settings.Settings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

public class QLearningNStepAsync extends QLearningNStepSync {
    private MultiLayerNetwork globalNetwork;
    private static ReentrantLock lock = new ReentrantLock();

    public QLearningNStepAsync() {
        super();
    }

    public QLearningNStepAsync(MultiLayerNetwork globalNetwork) {
        super();
        this.globalNetwork = globalNetwork;
        this.currentNetwork = globalNetwork.clone();
    }

    @Override
    public SelectActionResult selectAction(double[] currentState) {
        return super.selectAction(currentState);
    }

    @Override
    public ReinforcementLearning clone() {
        return new QLearningNStepAsync(globalNetwork);
    }

    @Override
    public void epochEnded(Experience experience) {
        super.epochEnded(experience);
        lock.lock();
        this.currentNetwork = globalNetwork.clone();
        lock.unlock();
    }

    @Override
    protected void fit(INDArray inputs, INDArray targets) {
        Gradient[] gradients = calculateGradient(inputs, targets, currentNetwork);
        lock.lock();
        applyGradient(gradients, experiences.size(), globalNetwork);
        lock.unlock();
    }

    private Gradient[] calculateGradient(INDArray input, INDArray labels, MultiLayerNetwork mln) {
        mln.setInput(input);
        mln.setLabels(labels);
        mln.computeGradientAndScore();
        Collection<IterationListener> iterationListeners = mln.getListeners();
        if (iterationListeners != null && iterationListeners.size() > 0) {
            for (IterationListener l : iterationListeners) {
                if (l instanceof TrainingListener) {
                    ((TrainingListener) l).onGradientCalculation(mln);
                }
            }
        }
        return new Gradient[]{mln.gradient()};
    }

    private void applyGradient(Gradient[] gradient, int batchSize, MultiLayerNetwork mln) {
        MultiLayerConfiguration mlnConf = mln.getLayerWiseConfigurations();
        int iterationCount = mlnConf.getIterationCount();
        int epochCount = mlnConf.getEpochCount();
        mln.getUpdater().update(mln, gradient[0], iterationCount, epochCount, batchSize);
        mln.params().subi(gradient[0].gradient());
        Collection<IterationListener> iterationListeners = mln.getListeners();
        if (iterationListeners != null && iterationListeners.size() > 0) {
            for (IterationListener listener : iterationListeners) {
                listener.iterationDone(mln, iterationCount, epochCount);
            }
        }
        mlnConf.setIterationCount(iterationCount + 1);
    }

    @Override
    public void train(EnvironmentFactory environmentFactory) {
        long start = System.currentTimeMillis();
        Agent agentToTrain = environmentFactory.getAgentToTrain();
        agentToTrain.setTrain(true);
        globalNetwork = ((Deep4jAgentAI) agentToTrain.getAI()).getNetwork();
        int numberOfAgents = Settings.THREADS;

        List<Future> workers = new ArrayList<>();
        List<AgentWorker> agentWorkers = new ArrayList<>();
        for (int i = 0; i < numberOfAgents; i++) {
            AgentWorker agentWorker = new AgentWorker(environmentFactory.clone(), i);
            agentWorkers.add(agentWorker);
            Future<?> future = ExecutorServiceHandler.submitTask(agentWorker);
            workers.add(future);
        }

        ExecutorServiceHandler.waitForTasksToFinish(workers);

        int stepsTrained = 0;
        for (AgentWorker worker : agentWorkers) {
            stepsTrained += worker.getEnvironmentFactory().getAgentToTrain().getAI().getReinforcementLearning().getCount();
        }


        globalNetwork = null;
        agentToTrain.setTrain(false);
        long end = System.currentTimeMillis();
        System.out.println("Finished training with speed: " + (stepsTrained*1000 / (double) (end - start)) + " steps/ms");
    }
}
