package agent;

import environment.Environment;
import environment.EnvironmentFactory;
import environment.EnvironmentObserver;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.util.List;

/**
 * Created by Marek on 30/06/2017.
 */
public interface Agent {
    int getAction(Environment environment);
    double[] getObservation(Environment environment, boolean addToHistory);
    void epochEnded(double reward, Environment environment);
    void setTrain(boolean train);
    void train(EnvironmentFactory environmentFactory);
    boolean isTraining();
    double getTotalReward();
    String getName();
    void resetTotalReward();
    void addToTotalReward(double reward);
    AI getAI();
    void setAI(AI ai);
    EnvironmentObserver getObserver();
    void changeLastAction(int action);
    int getLastAction();
    void setHistoryOfStates(List<ActionHistory> historyOfStates);
    List<ActionHistory> getHistoryOfStates();
}
