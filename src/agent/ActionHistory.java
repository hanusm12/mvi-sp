package agent;

import org.nd4j.linalg.api.ndarray.INDArray;

public class ActionHistory {
    private double historyState[];
    private int action;

    public ActionHistory(double historyState[], int action) {
        this.historyState = historyState;
        this.action = action;
    }

    public double[] getHistoryState() {
        return historyState;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
