package agent;

import environment.Environment;
import environment.EnvironmentFactory;
import environment.EnvironmentObserver;
import reinforcementLearning.ReinforcementLearning;
import settings.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GenericAgent implements Agent {
    protected AI ai;
    protected EnvironmentObserver observer;
    protected List<ActionHistory> historyOfStates;
    private double totalReward;
    private int lastAction;

    protected String name;

    public GenericAgent(String name, EnvironmentObserver observer, AI ai) {
        this.name = name;
        this.observer = observer;
        this.ai = ai;
        this.historyOfStates = new ArrayList<>(observer.getNumberOfHistoryStates());
    }

    @Override
    public double[] getObservation(Environment environment, boolean addToHistory) {
        double observation[] = new double[observer.getTotalStateSize()];
        observer.fillCurrentState(environment, this, observation);

        fillHistory(observation);
        if (observer.getNumberOfHistoryStates() > 0 && addToHistory) {
            double[] historyState = observer.getHistoryRepresentation(environment, this);

            historyOfStates.add(0, new ActionHistory(historyState, 0));
            if (historyOfStates.size() > observer.getNumberOfHistoryStates()) {
                historyOfStates.remove(observer.getNumberOfHistoryStates());
            }
        }
        return observation;
    }

    private void fillHistory(double state[]) {
        int index = observer.getCurrentStateSize();
        int i;
        for (i = 0; i < historyOfStates.size(); i++) {
            if (historyOfStates.get(i).getHistoryState() != null) {
                System.arraycopy(historyOfStates.get(i).getHistoryState(), 0, state, index, observer.getHistoryStateSize());
                index += observer.getHistoryStateSize();
            }
            state[index + historyOfStates.get(i).getAction()] = 1;
            index += observer.getNumberOfNetworkOutputs() + 1;
        }
        for (; i < observer.getNumberOfHistoryStates(); i++) {
            state[index] = 1;
            index += observer.getNumberOfNetworkOutputs() + 1 + observer.getHistoryStateSize();
        }
    }

    @Override
    public void epochEnded(double reward, Environment environment) {
        historyOfStates = new ArrayList<>(observer.getNumberOfHistoryStates());
        totalReward += reward;
        ai.epochEnded(reward);
    }

    @Override
    public int getAction(Environment environment) {
        double[] observation = getObservation(environment, true);
        lastAction = ai.getAction(environment.getAgentScore(this, false), observation);
        changeLastAction(lastAction);
        return lastAction;
    }

    @Override
    public void setTrain(boolean train) {
        ai.setTrain(train);
    }

    @Override
    public void train(EnvironmentFactory environmentFactory) {
        ReinforcementLearning reinforcementLearning = ai.getReinforcementLearning();
        reinforcementLearning.train(environmentFactory);
    }

    @Override
    public boolean isTraining() {
        return ai.isTraining();
    }


    @Override
    public double getTotalReward() {
        return totalReward;
    }

    @Override
    public void resetTotalReward() {
        totalReward = 0;
    }

    @Override
    public void addToTotalReward(double reward) {
        totalReward += reward;
    }

    @Override
    public EnvironmentObserver getObserver() {
        return observer;
    }

    @Override
    public void changeLastAction(int action) {
        if (observer.getNumberOfHistoryStates() > 0) {
            historyOfStates.get(0).setAction(action);
        }
    }

    @Override
    public int getLastAction() {
        return lastAction;
    }

    @Override
    public void setHistoryOfStates(List<ActionHistory> historyOfStates) {
        this.historyOfStates = historyOfStates;
    }

    @Override
    public List<ActionHistory> getHistoryOfStates() {
        return historyOfStates;
    }

    public AI getAI() {
        return ai;
    }

    @Override
    public void setAI(AI ai) {
        this.ai = ai;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericAgent that = (GenericAgent) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
