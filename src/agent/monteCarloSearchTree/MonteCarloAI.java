package agent.monteCarloSearchTree;

import agent.AI;
import agent.AbstractAI;
import agent.monteCarloSearchTree.tree.MonteCarloSearchTree;
import reinforcementLearning.ReinforcementLearning;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;

public class MonteCarloAI extends AbstractAI {
    private MonteCarloSearchTree tree = new MonteCarloSearchTree();

    @Override
    public int getAction(double reward, double[] inputs) {
        currentState = inputs;
        int move = tree.selectMove(currentState, train);
        return move;
    }

    @Override
    public void epochEnded(double reward) {
        if (train && currentState != null) {
            tree.backPropagate(reward);
        }
        tree.epochEnded();
        currentState = null;
    }

    @Override
    public int[] getActions(double[][] inputs) {
        if (inputs.length != 0) {
            throw new UnsupportedOperationException("Not implemented yet");
        }
        return new int[]{getAction(0, inputs[0])};
    }

    @Override
    public ReinforcementLearning getReinforcementLearning() {
        return null;
    }

    @Override
    public AI clone() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void save(OutputStream outputStream) throws IOException {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public AI load(ObjectInputStream objectInputStream) throws IOException {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
