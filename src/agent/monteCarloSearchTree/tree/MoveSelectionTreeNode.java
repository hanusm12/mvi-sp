package agent.monteCarloSearchTree.tree;

import javafx.fxml.FXML;
import settings.Settings;
import sun.reflect.generics.tree.Tree;

import java.util.HashMap;
import java.util.Map;

public class MoveSelectionTreeNode extends TreeNode {

    private TreeNode[] moves;

    public MoveSelectionTreeNode(TreeNode parent) {
        super(parent);
        moves = new TreeNode[4];
        moves[0] = new PossibleOutcomeTreeNode(this);
        moves[1] = new PossibleOutcomeTreeNode(this);
        moves[2] = new PossibleOutcomeTreeNode(this);
        moves[3] = new PossibleOutcomeTreeNode(this);
    }

    @Override
    public TreeNode getNode(DoubleArray doubleArray, int action) {
        return moves[action].getNode(doubleArray, action);
    }

    public int getMostProfitableMove() {
        int bestMove = 0;
        double bestMoveValue = moves[0].getExpectedProfit();
        for (int i = 1; i < moves.length; i++) {
            double expectedProfit = moves[i].getExpectedProfit();
            if (expectedProfit > bestMoveValue) {
                bestMoveValue = expectedProfit;
                bestMove = i;
            }
        }
        return bestMove;
    }

    public int getTrainingMove() {
        double probabilities[] = new double[moves.length];
        double sum = 0;
        for (int i = 0; i < moves.length; i++) {
            double prob;
            if (moves[i].getCount() == 0) {
                prob = 100;
            } else {
                prob = (moves[i].getExpectedProfit() + 1) * 0.5 + 1.5 * Math.sqrt(Math.log(count) / moves[i].getCount());
            }
            probabilities[i] = prob;
            sum += prob;
        }
        double choice = Settings.RANDOM.nextDouble() * sum;
        sum = 0;
        for (int i = 0; i < moves.length; i++) {
            sum += probabilities[i];
            if (sum >= choice) {
                return i;
            }
        }
        return moves.length - 1;
    }

    public TreeNode epochEnded(int lastAction) {
        return moves[lastAction].getNode(new DoubleArray(new double[]{-10000}), lastAction);
    }

}
