package agent.monteCarloSearchTree.tree;

public class MonteCarloSearchTree {
    private TreeNode root;
    private TreeNode currentNode;
    private int lastAction;

    public MonteCarloSearchTree() {
        root = new PossibleOutcomeTreeNode(null);
        currentNode = root;
    }


    public int selectMove(double[] inputs, boolean train) {
        DoubleArray doubleArray = new DoubleArray(inputs);
        currentNode = currentNode.getNode(doubleArray, lastAction);
        if (train) {
            lastAction = ((MoveSelectionTreeNode) currentNode).getTrainingMove();
        } else {
            lastAction = ((MoveSelectionTreeNode) currentNode).getMostProfitableMove();
        }
        return lastAction;
    }


    public void backPropagate(double reward) {
        currentNode = ((MoveSelectionTreeNode) currentNode).epochEnded(lastAction);
        currentNode.backPropagate(reward);
    }

    public void epochEnded() {
        currentNode = root;
    }
}
