package agent.monteCarloSearchTree.tree;

import java.util.HashMap;
import java.util.Map;

public abstract class TreeNode {
    private TreeNode parent;

    private double expectedProfit;
    protected int count;

    public TreeNode(TreeNode parent) {
        this.parent = parent;
    }

    public abstract TreeNode getNode(DoubleArray doubleArray, int action);

    public void backPropagate(double value) {
        expectedProfit += value;
        count++;
        if (parent != null) {
            parent.backPropagate(value);
        }
    }

    public double getExpectedProfit() {
        if (count == 0) {
            return 0;
        }
        return expectedProfit / (double) count;
    }

    public int getCount() {
        return count;
    }
}


