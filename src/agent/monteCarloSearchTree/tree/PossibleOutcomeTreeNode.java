package agent.monteCarloSearchTree.tree;

import java.util.HashMap;
import java.util.Map;

public class PossibleOutcomeTreeNode extends TreeNode {
    private Map<DoubleArray, TreeNode> children;

    public PossibleOutcomeTreeNode(TreeNode parent) {
        super(parent);
        children = new HashMap<>();
    }

    @Override
    public TreeNode getNode(DoubleArray doubleArray, int action) {
        TreeNode treeNode = children.get(doubleArray);
        if (treeNode == null) {
            treeNode = new MoveSelectionTreeNode(this);
            children.put(doubleArray, treeNode);
        }
        return treeNode;
    }


}
