package agent.deep4j;

import agent.AI;
import agent.AbstractAI;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.indexaccum.IMax;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.SelectActionResult;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;
import settings.Utils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;

/**
 * Created by Marek on 30/06/2017.
 */
public class Deep4jAgentAIActorCritic extends AbstractAI {
    protected ComputationGraph network;
    protected ReinforcementLearning reinforcementLearning;

    protected double[] currentState;

    protected int action;

    protected double lastExperienceReward = 0;

    protected INDArray[] networkOutput;


    public Deep4jAgentAIActorCritic(ComputationGraph network, ReinforcementLearning reinforcementLearning) {
        this.network = network;
        this.reinforcementLearning = reinforcementLearning;
    }

    public Deep4jAgentAIActorCritic(Deep4jAgentAIActorCritic ai) {
        this.network = ai.getNetwork();
        this.reinforcementLearning = ai.getReinforcementLearning();
    }


    public ComputationGraph getNetwork() {
        return network;
    }

    public void setNetwork(ComputationGraph network) {
        this.network = network;
    }

    public int getLastAction() {
        return action;
    }

    @Override
    public int getAction(double reward, double[] inputs) {
        if (train && currentState != null) {
            //There was already action before this one -> save it into memory, since now we have future state
            reinforcementLearning.moveEnded(new Experience(currentState, inputs, networkOutput, action, reward - lastExperienceReward));
            lastExperienceReward = reward;
        }
        currentState = inputs;

        if (train) {
            SelectActionResult selectActionResult = reinforcementLearning.selectAction(currentState);
            action = selectActionResult.action;
            networkOutput = selectActionResult.outputs;
        } else {
            networkOutput = network.output(Nd4j.create(currentState));
            action = Utils.getIndexOfBiggest(networkOutput[1]);
        }

        return action;
    }

    @Override
    public void epochEnded(double reward) {
        if (train && currentState != null) {
            reinforcementLearning.epochEnded(new Experience(currentState, null, networkOutput, action, reward - lastExperienceReward));
        }
        lastExperienceReward = 0;
        currentState = null;
    }

    @Override
    public int[] getActions(double[][] inputs) {
        int[] actions = new int[inputs.length];
        INDArray[] output = network.output(Nd4j.create(inputs));
        INDArray argMax = Nd4j.getExecutioner().exec(new IMax(output[1]), 1);
        for (int i = 0; i < actions.length; i++) {
            actions[i] = (int) argMax.getDouble(i);
        }
        return actions;
    }

    @Override
    public ReinforcementLearning getReinforcementLearning() {
        return reinforcementLearning;
    }

    @Override
    public AI clone() {
        Deep4jAgentAIActorCritic res;
        if (train) {
            res = new Deep4jAgentAIActorCritic(network, reinforcementLearning.clone());
            res.setTrain(true);
        } else {
            res = new Deep4jAgentAIActorCritic(network.clone(), reinforcementLearning);
        }
        return res;
    }

    @Override
    public void save(OutputStream outputStream) throws IOException {
        ModelSerializer.writeModel(network, outputStream, false);
    }

    @Override
    public AI load(ObjectInputStream objectInputStream) throws IOException {
        ComputationGraph multiLayerNetwork = ModelSerializer.restoreComputationGraph(objectInputStream);
        AI ai = new Deep4jAgentAIActorCritic(multiLayerNetwork, null);
        return ai;
    }

}
