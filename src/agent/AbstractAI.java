package agent;

import environment.EnvironmentFactory;

public abstract class AbstractAI implements AI{
    protected boolean train;
    protected double[] currentState;

    @Override
    public void setTrain(boolean train) {
        this.train = train;
    }

    @Override
    public boolean isTraining() {
        return train;
    }

    @Override
    public AI clone() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
