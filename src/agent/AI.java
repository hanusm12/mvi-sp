package agent;

import reinforcementLearning.ReinforcementLearning;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;

public interface AI {
    int getAction(double reward, double[] inputs);
    void epochEnded(double reward);
    int[] getActions(double [][] inputs);
    void setTrain(boolean train);
    boolean isTraining();
    ReinforcementLearning getReinforcementLearning();
    AI clone();
    void save(OutputStream outputStream) throws IOException;
    AI load(ObjectInputStream objectInputStream) throws IOException;
}
