package visualization;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.text.Text;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import poker.game.Card;
import settings.Settings;
import settings.Utils;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private List<Text> profits = new ArrayList<>();

    @FXML
    private Text steps;

    @FXML
    private Text score;

    @FXML
    private GridPane gridPane;


    public void refreshGridPane(INDArray networkOutputs) {
        Platform.runLater(() -> {
            for (int i = 0; i < profits.size(); i++) {
                Text text = profits.get(i);
                INDArray row = networkOutputs.getRow(i);
                int index = Utils.getIndexOfBiggest(row);
                double max = row.getDouble(index);
                if (index == 0) {
                    text.setFill(Color.INDIANRED);
                } else if (index == 1) {
                    text.setFill(Color.GREY);
                } else if (index == 2) {
                    text.setFill(Color.GREENYELLOW);
                }
                text.setText(String.valueOf(String.format("%.1f", max)));
            }
            steps.setText(String.valueOf(Integer.valueOf(steps.getText()) + 100));
        });

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Settings.VISUALIZATION_CONTROLLER = this;
        for (int i = 0; i < 13; i++) {
            gridPane.add(new Text(new Card(i + 2, poker.enums.Color.CLUB).valueAsString()), i + 1, 0);
            gridPane.add(new Text(new Card(i + 2, poker.enums.Color.CLUB).valueAsString()), 0, i + 1);

        }

        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 13; j++) {
                Text text = new Text("");
                gridPane.add(text, i + 1, j + 1);
                profits.add(text);
                //Pane pane = new Pane();
                //pane.setStyle("-fx-border-color: black");
                //pane.setBackground(new Background(new BackgroundFill(new Color((j / (double) (100)) * 5, (i / (double) (100)) * 5, 0, 1), CornerRadii.EMPTY, Insets.EMPTY)));
                //gridPane.add(pane, i, j);
            }
        }
    }
}
