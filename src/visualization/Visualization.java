package visualization;

import gui.GameController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import neuralnetwork.NeuralNetworks;
import settings.Settings;
import sun.applet.Main;

import java.io.File;
import java.net.URL;

public class Visualization extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("Poker-Bot/src/visualization/visualization.fxml").toURL();
        Parent root = FXMLLoader.load(url);

        Scene scene = new Scene(root);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);
        primaryStage.show();


        new MainThread().start();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
