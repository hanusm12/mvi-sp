package visualization;

import neuralnetwork.NeuralNetworks;

public class MainThread extends Thread {

    @Override
    public void run() {
        try {
            NeuralNetworks.main(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
