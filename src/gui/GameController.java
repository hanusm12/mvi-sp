package gui;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import gui.poker.game.GameWithUIThread;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import poker.game.Card;
import poker.game.Game;
import poker.player.Player;
import settings.Settings;

public class GameController implements Initializable {
    @FXML
    private Pane mainGamePane;
    @FXML
    private ListView<String> chatBox;
    @FXML
    private Label chipBalance;
    @FXML
    private Label playerName1;
    @FXML
    private Label playerChips1;
    @FXML
    private Label playerName2;
    @FXML
    private Label playerChips2;
    @FXML
    private Label playerName3;
    @FXML
    private Label playerChips3;
    @FXML
    private Label playerName4;
    @FXML
    private Label playerChips4;
    @FXML
    private Label playerName5;
    @FXML
    private Label playerChips5;
    @FXML
    private Label playerName6;
    @FXML
    private Label playerChips6;
    @FXML
    private Label playerChipsIngameThisRound1;
    @FXML
    private Label playerChipsIngameThisRound2;
    @FXML
    private Label playerChipsIngameThisRound3;
    @FXML
    private Label playerChipsIngameThisRound4;
    @FXML
    private Label playerChipsIngameThisRound5;
    @FXML
    private Label playerChipsIngameThisRound6;
    @FXML
    private ImageView flop1;
    @FXML
    private ImageView flop2;
    @FXML
    private ImageView flop3;
    @FXML
    private ImageView turn;
    @FXML
    private ImageView river;


    @FXML
    private ImageView plr1Card1;
    @FXML
    private ImageView plr1Card2;
    @FXML
    private ImageView plr2Card1;
    @FXML
    private ImageView plr2Card2;
    @FXML
    private ImageView plr3Card1;
    @FXML
    private ImageView plr3Card2;
    @FXML
    private ImageView plr4Card1;
    @FXML
    private ImageView plr4Card2;
    @FXML
    private ImageView plr5Card1;
    @FXML
    private ImageView plr5Card2;
    @FXML
    private ImageView plr6Card1;
    @FXML
    private ImageView plr6Card2;
    @FXML
    private Label potSize;
    @FXML
    private Button checkCall;
    @FXML
    private Button fold;
    @FXML
    private Button bet50;
    @FXML
    private Button bet75;
    @FXML
    private Button bet100;
    @FXML
    private Button startGameButton;

    private final String FOLDER = "gui/images/";
    private final String fileExtension = ".png";

    public static List<Player> players;
    private Map<Player, PlayerInfoComponents> playerToTheirComponentsMap;

    public static int playerMove = -1;
    public static int playerMoveSize = -1;

    private GameWithUIThread gameThread;

    @FXML
    private void startGame(ActionEvent event) {
        gameThread = new GameWithUIThread(players, this);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(gameThread);
        //for (int i = 0; i < Settings.HANDS_PER_ROUND / Settings.HANDS_PER_TABLE; i++) {
        //	executorService.submit(gameThread);
        //}
        startGameButton.setVisible(false);

    }

    @FXML
    private void foldButton(ActionEvent event) {
        playerMove = 0;
        setVisibilityForPlayerActions(false);
    }

    @FXML
    private void checkCallButton(ActionEvent event) {
        playerMove = 1;
        setVisibilityForPlayerActions(false);
    }

    @FXML
    private void bet50Button(ActionEvent event) {
        playerMove = 2;
        setVisibilityForPlayerActions(false);
    }

    @FXML
    private void bet75Button(ActionEvent event) {
        playerMove = 3;
        setVisibilityForPlayerActions(false);
    }

    @FXML
    private void bet100Button(ActionEvent event) {
        playerMove = 4;
        setVisibilityForPlayerActions(false);
    }


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setVisibilityForPlayerActions(false);

        Settings.controller = this;
        //players.add(0, new Player("You", new PlayerAI(new HumanControlledPredictionModel(null))));
        players.get(1).setName("Opponent 1");
        players.get(2).setName("Opponent 2");
        players.get(3).setName("Opponent 3");
        players.get(4).setName("Opponent 4");
        players.get(5).setName("Opponent 5");


        playerToTheirComponentsMap = new HashMap<>();
        playerToTheirComponentsMap.put(players.get(0), new PlayerInfoComponents(plr1Card1, plr1Card2, playerName1,
                playerChips1, playerChipsIngameThisRound1, players.get(0), true));
        playerToTheirComponentsMap.put(players.get(1), new PlayerInfoComponents(plr2Card1, plr2Card2, playerName2,
                playerChips2, playerChipsIngameThisRound2, players.get(1), false));
        playerToTheirComponentsMap.put(players.get(2), new PlayerInfoComponents(plr3Card1, plr3Card2, playerName3,
                playerChips3, playerChipsIngameThisRound3, players.get(2), false));
        playerToTheirComponentsMap.put(players.get(3), new PlayerInfoComponents(plr4Card1, plr4Card2, playerName4,
                playerChips4, playerChipsIngameThisRound4, players.get(3), false));
        playerToTheirComponentsMap.put(players.get(4), new PlayerInfoComponents(plr5Card1, plr5Card2, playerName5,
                playerChips5, playerChipsIngameThisRound5, players.get(4), false));
        playerToTheirComponentsMap.put(players.get(5), new PlayerInfoComponents(plr6Card1, plr6Card2, playerName6,
                playerChips6, playerChipsIngameThisRound6, players.get(5), false));

        Settings.ONLY_SIMULATION_WITHOUT_TRAINING = true;
        Settings.LOG_GAME = true;
        Settings.HANDS_PER_TABLE = 10000;
    }

    public void setLabels(Player player) {
        playerToTheirComponentsMap.get(player).setLabels();
    }

    public void setPlayerCards(Player player, boolean showdown) {
        playerToTheirComponentsMap.get(player).setCards(showdown);
    }

    public void addChatMessage(final String message) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                chatBox.getItems().add(message);

            }
        });

    }

    public void setPlayerButtons(final Game game, final Player player) {
        setVisibilityForPlayerActions(true);
        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                if (game.getMinimumSize() == player.getChipsIngameThisRound()) {
                    checkCall.setText("Check");
                } else {
                    checkCall.setText("Call");
                }

                int minimum = game.getMinimumSize();
                int totalPot = game.getPotSize();
                int toCall = minimum - player.getChipsIngameThisRound();

                int raiseSize = minimum + ((totalPot + toCall) / 2);
                if (player.getChipsIngameThisRound() <= minimum) {
                    bet50.setText("Bet " + raiseSize);
                } else {
                    bet50.setText("Raise to " + raiseSize);
                }

                raiseSize = (int) (minimum + ((totalPot + toCall) * 0.75));
                if (player.getChipsIngameThisRound() <= minimum) {
                    bet75.setText("Bet " + raiseSize);
                } else {
                    bet75.setText("Raise to " + raiseSize);
                }

                raiseSize = minimum + ((totalPot + toCall));
                if (player.getChipsIngameThisRound() <= minimum) {
                    bet100.setText("Bet " + raiseSize);
                } else {
                    bet100.setText("Raise to " + raiseSize);
                }


            }
        });
    }

    private void setVisibilityForPlayerActions(final boolean visibility) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                fold.setVisible(visibility);
                checkCall.setVisible(visibility);
                bet50.setVisible(visibility);
                bet75.setVisible(visibility);
                bet100.setVisible(visibility);

            }
        });
    }

    public void setTurn(final Card card) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                turn.setImage(new Image(FOLDER + card.toString() + fileExtension));

            }
        });

    }

    public void refresh(final Game game, boolean showCards) {
        final List<Player> players = game.getPlayers();
        for (Player player : players) {
            PlayerInfoComponents playerInfoComponents = playerToTheirComponentsMap.get(players.get(players.indexOf(player)));
            playerInfoComponents.setCards(showCards);
            playerInfoComponents.setLabels();
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GameController.this.potSize.setText("Pot size: " + game.getPotSize());

                chipBalance.setText(String.valueOf(players.get(0).getTotalBalance()));

                if (game.getBoard().getFlop() != null) {
                    flop1.setImage(new Image("@../../" + game.getBoard().getFlop()[0] + fileExtension));
                    flop2.setImage(new Image("@../../" + game.getBoard().getFlop()[1] + fileExtension));
                    flop3.setImage(new Image("@../../" + game.getBoard().getFlop()[2] + fileExtension));
                } else {
                    flop1.setImage(null);
                    flop2.setImage(null);
                    flop3.setImage(null);
                }

                if (game.getBoard().getTurn() != null) {
                    turn.setImage(new Image("@../../" + game.getBoard().getTurn() + fileExtension));
                } else {
                    turn.setImage(null);
                }


                if (game.getBoard().getRiver() != null ){
                    river.setImage(new Image("@../../" + game.getBoard().getRiver() + fileExtension));
                } else {
                    river.setImage(null);
                }


            }
        });


    }

    public void setPotSize(final int potSize) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                GameController.this.potSize.setText("Pot size: " + potSize);

            }
        });
    }

}
