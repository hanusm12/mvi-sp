package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import poker.player.PlayersLoader;

import java.io.File;
import java.net.URL;


public class GUIapplication extends Application {
    @Override
    public void start(Stage stage) throws Exception {
    	//GameController.players = PlayersLoader.loadSavedPlayers();
    	while(GameController.players.size() > 5) {
    	    GameController.players.remove(5);
        }
        URL url = new File("Poker-Bot/src/gui/Game.fxml").toURL();
        Parent root = FXMLLoader.load(url);
        //Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("/gui/Game.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                Platform.exit();
            }
        });
    }


	public static void main(String[] args) {
		launch(args);
	}
}
