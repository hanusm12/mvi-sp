package gui;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import poker.player.Player;

import java.io.File;
import java.net.MalformedURLException;

public class PlayerInfoComponents {
    private final String FOLDER = "";
    private final String fileExtension = ".png";

    private final String CARD_BACK_NAME = "card_back";

    private ImageView card1, card2;
    private Label name, chips, chipsIngameThisRound;
    private Player player;
    private boolean isHuman;

    public PlayerInfoComponents(ImageView card1, ImageView card2, Label name, Label chips, Label chipsIngameThisRound,
                                Player player, boolean isHuman) {
        super();
        this.card1 = card1;
        this.card2 = card2;
        this.name = name;
        this.chips = chips;
        this.chipsIngameThisRound = chipsIngameThisRound;
        this.player = player;
        this.isHuman = isHuman;
    }

    public void setCards(final boolean showdown) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (player.isIsIngame()) {
                    if (isHuman || showdown) {
                        card1.setImage(new Image("@../../" + player.getFirstCard().toString() + fileExtension));
                        card2.setImage(new Image("@../../" + player.getSecondCard().toString() + fileExtension));
                    } else {
                        card1.setImage(new Image("@../../" + CARD_BACK_NAME + fileExtension));
                        card2.setImage(new Image("@../../" + CARD_BACK_NAME + fileExtension));
                    }
                } else {
                    card1.setImage(null);
                    card2.setImage(null);
                }

            }
        });
    }

    public void setLabels() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                name.setText(player.getName());
                chips.setText(String.valueOf(player.getChips()));
                if (player.isIsIngame() && player.getChipsIngameThisRound() > 0) {
                    chipsIngameThisRound.setVisible(true);
                    chipsIngameThisRound.setText(String.valueOf(player.getChipsIngameThisRound()));
                } else {
                    chipsIngameThisRound.setVisible(false);
                }
            }
        });
    }

}
