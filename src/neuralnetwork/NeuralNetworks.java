package neuralnetwork;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import poker.game.*;
import poker.game.cardDealer.CardDealerFactory;
import poker.game.cardDealer.RandomCardDealerFactory;
import poker.game.cardDealer.SpecifiedCardDealerFactory;
import poker.player.Player;
import poker.player.PlayersLoader;
import settings.ExecutorServiceHandler;
import settings.Settings;

import java.util.*;

import static org.nd4j.linalg.ops.transforms.Transforms.abs;


public class NeuralNetworks {

    public static void main(String[] args)
            throws Exception {
        //test();

        ExecutorServiceHandler.start();

        List<Player> players = PlayersLoader.loadPlayers();


        for (int i = 0; i < 1; i++) {

            GameRunner environmentRunner = new GameRunner();
            CardDealerFactory cardDealerFactory = new SpecifiedCardDealerFactory(Settings.HAND1VAL, Settings.HAND2VAL, Settings.SUITED == 1);
            //CardDealerFactory cardDealerFactory = new RandomCardDealerFactory();

            Player worstPlayer = players.get(5);
            //Player worstPlayer = environmentRunner.findWorstPlayer(players, 25000, cardDealerFactory);


            //Player newPlayer = PlayersLoader.createNewPlayer(worstPlayer.getName(), true);
            Player newPlayer = PlayersLoader.createNewPlayerUsingA3C(worstPlayer.getName(), true);


            players.remove(worstPlayer);
            players.add(newPlayer);

            Settings.playerToTrain = newPlayer;
            newPlayer.train(new GameFactory(players, newPlayer, cardDealerFactory));

            boolean newPlayerIsBetter = environmentRunner.evaluateSpecifiedAgent(players, newPlayer, cardDealerFactory);
            if (newPlayerIsBetter) {
                PlayersLoader.savePlayer(newPlayer);
            } else {
                players.remove(newPlayer);
                players.add(worstPlayer);
            }
            Settings.playerToTrain = null;
            System.out.println();
        }


        ExecutorServiceHandler.stop();
    }


    private static void test() {
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new Sgd(0.02))
                .seed(8)
                .weightInit(WeightInit.XAVIER)
                .list(
                        new DenseLayer.Builder()
                                .nIn(5)
                                .nOut(50)
                                .activation(Activation.RELU)
                                .build(),
                        new DenseLayer.Builder()
                                .nIn(50)
                                .nOut(25)
                                .activation(Activation.RELU)
                                .build(),
                        new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                                .nIn(25)
                                .nOut(1)
                                .activation(Activation.IDENTITY)
                                .build()
                )
                .pretrain(false).backprop(true) //use backpropagation to adjust weights
                .build();
        MultiLayerNetwork network = new MultiLayerNetwork(conf);
        network.init();


        INDArray inputs = Nd4j.rand(new int[]{5000, 5}, 8);
        INDArray targets = Nd4j.rand(new int[]{5000, 1}, 8);
        DataSet dataSet = new DataSet(inputs, targets);


        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            network.fit(dataSet);
            System.out.println("Iteration: " + i + ". Score: " + network.score());
        }
        long end = System.currentTimeMillis();
        System.out.println("Calculation took: " + (end - start));


    }
}
