package environment;

import java.io.Serializable;

/**
 * Created by Marek on 24/07/2017.
 */
public abstract class AbstractEnvironmentObserver implements EnvironmentObserver, Serializable {
    protected int numberOfHistoryStates;
    protected int currentStateSize;
    protected int historyStateSize;
    protected int numberOfNetworkOutputs;

    public AbstractEnvironmentObserver(int numberOfHistoryStates, int currentStateSize, int historyStateSize, int numberOfNetworkOutputs) {
        this.numberOfHistoryStates = numberOfHistoryStates;
        this.currentStateSize = currentStateSize;
        this.historyStateSize = historyStateSize;
        this.numberOfNetworkOutputs = numberOfNetworkOutputs;
    }

    public void setNumberOfHistoryStates(int numberOfHistoryStates) {
        this.numberOfHistoryStates = numberOfHistoryStates;
    }

    public void setCurrentStateSize(int currentStateSize) {
        this.currentStateSize = currentStateSize;
    }

    public void setHistoryStateSize(int historyStateSize) {
        this.historyStateSize = historyStateSize;
    }

    public void setNumberOfNetworkOutputs(int numberOfNetworkOutputs) {
        this.numberOfNetworkOutputs = numberOfNetworkOutputs;
    }

    @Override
    public int getCurrentStateSize() {
        return currentStateSize;
    }

    @Override
    public int getHistoryStateSize() {
        return historyStateSize;
    }

    @Override
    public int getNumberOfHistoryStates() {
        return numberOfHistoryStates;
    }

    @Override
    public int getNumberOfNetworkOutputs() {
        return numberOfNetworkOutputs;
    }

    @Override
    public int getTotalStateSize() {
        return currentStateSize + numberOfHistoryStates * (historyStateSize + numberOfNetworkOutputs + 1);
    }
}
