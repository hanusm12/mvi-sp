package environment;

import agent.Agent;

public interface EnvironmentRunner {
    boolean evaluateSpecifiedAgent(Object context, Agent agent, int steps);
    void train(Object context, Agent agent, int steps);
}
