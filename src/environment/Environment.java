package environment;

import agent.Agent;

/**
 * Created by Marek on 30/06/2017.
 */
public interface Environment {
    void playSingleMove(int moveIndex);

    boolean needNewInstance();

    boolean epochEnded();

    void initializeBeforeEpoch();

    double getAgentScore(Agent agent, boolean endOfEpoch);

    Agent getAgentToAct();


}
