package environment;

import agent.Agent;

/**
 * Created by Marek on 12/08/2017.
 */
public class SingleMoveResult {
    private double reward;
    private boolean epochEnd;

    public SingleMoveResult() {
    }

    public SingleMoveResult(double reward, boolean epochEnd) {
        this.reward = reward;
        this.epochEnd = epochEnd;
    }

    public double getReward() {
        return reward;
    }

    public void setReward(double reward) {
        this.reward = reward;
    }

    public boolean isEpochEnd() {
        return epochEnd;
    }

    public void setEpochEnd(boolean epochEnd) {
        this.epochEnd = epochEnd;
    }
}
