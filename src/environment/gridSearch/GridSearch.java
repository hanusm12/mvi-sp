package environment.gridSearch;

import agent.Agent;
import environment.Environment;

import java.util.Random;

/**
 * Created by Marek on 30/06/2017.
 */
public class GridSearch implements Environment {
    private Cell[][] grid;
    private int agentXPosition, agentYPosition;
    private int targetXPosition, targetYPosition;
    private int moves;
    private double score;
    private Agent agent;
    private boolean epochEnded;

    public GridSearch(Agent agent) {
        this.agent = agent;
        initGrid();
    }

    private void initGrid() {
        grid = new Cell[GridSearchSettings.GRID_SIZE][GridSearchSettings.GRID_SIZE];
        //Init everything to empty
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Cell.EMPTY;
            }
        }
        Random random = new Random();


        //Place target randomly
        targetXPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
        targetYPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
        grid[targetXPosition][targetYPosition] = Cell.TARGET;

        //Place agent randomly
        agentXPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
        agentYPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
        while (agentXPosition == targetXPosition && agentYPosition == targetYPosition) {
            agentXPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
            agentYPosition = random.nextInt(GridSearchSettings.GRID_SIZE);
        }
        grid[agentXPosition][agentYPosition] = Cell.AGENT;


/*
        agentXPosition = 2;
        agentYPosition = 2;
        targetXPosition = 0;
        targetYPosition = 0;

        grid[agentXPosition][agentYPosition] = Cell.AGENT;
        grid[targetXPosition][targetYPosition] = Cell.TARGET;
*/

        //Place random walls, which when entered ends the game
        score = 0;
        moves = 0;
    }

    @Override
    public void playSingleMove(int moveIndex) {
        moves++;
        epochEnded = false;



        boolean endGame = processAgentAction(moveIndex);



        if (moves > 15) {
            score = -1;
            endGame = true;
        }
        if (endGame) {
            epochEnded = true;
            agent.epochEnded(getAgentScore(agent, true), this);
            initializeBeforeEpoch();
        }
    }

    @Override
    public boolean needNewInstance() {
        return false;
    }

    @Override
    public boolean epochEnded() {
        return epochEnded;
    }

    @Override
    public void initializeBeforeEpoch() {
        initGrid();
    }


    @Override
    public double getAgentScore(Agent agent, boolean endOfEpoch) {
        return score;
    }

    @Override
    public Agent getAgentToAct() {
        return agent;
    }

    private boolean processAgentAction(int agentAction) {
        switch (agentAction) {
            // UP
            case 0: {
                return processChanges(0, -1);
            }

            //DOWN
            case 1: {
                return processChanges(0, 1);
            }

            //LEFT
            case 2: {
                return processChanges(-1, 0);
            }
            //RIGHT
            case 3: {
                return processChanges(1, 0);
            }
        }
        return false;
    }

    private boolean processChanges(int xOffset, int yOffset) {
        grid[agentXPosition][agentYPosition] = Cell.EMPTY;
        agentXPosition += xOffset;
        agentYPosition += yOffset;
        if (agentXPosition < 0 || agentXPosition >= GridSearchSettings.GRID_SIZE || agentYPosition < 0 || agentYPosition >= GridSearchSettings.GRID_SIZE) {
            score = -1;
            return true;
        }
        if (grid[agentXPosition][agentYPosition] == Cell.TARGET) {
            score = 1;
            return true;
        }
        grid[agentXPosition][agentYPosition] = Cell.AGENT;
        return false;
    }

    public Cell[][] getGrid() {
        return grid;
    }

    public void printGrid() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (j > 0) {
                    System.out.print(" ");
                }
                char c = ' ';
                switch (grid[j][i]) {
                    case AGENT:
                        c = 'A';
                        break;
                    case EMPTY:
                        c = '-';
                        break;
                    case TARGET:
                        c = 'T';
                        break;
                }
                System.out.print(c);
            }
            System.out.println();
        }
        System.out.println();
    }

    public int getAgentXPosition() {
        return agentXPosition;
    }

    public int getAgentYPosition() {
        return agentYPosition;
    }

    public int getTargetXPosition() {
        return targetXPosition;
    }

    public int getTargetYPosition() {
        return targetYPosition;
    }

}
