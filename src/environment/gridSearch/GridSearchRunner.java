package environment.gridSearch;

import agent.Agent;
import environment.Environment;
import environment.EnvironmentRunner;
import reinforcementLearning.ReinforcementLearning;

/**
 * Created by Marek on 15/08/2017.
 */
public class GridSearchRunner implements EnvironmentRunner {

    @Override
    public boolean evaluateSpecifiedAgent(Object context, Agent agent, int steps) {
        Environment gridSearch = new GridSearch(agent);
        agent.setTrain(false);
        agent.resetTotalReward();
        for (int i = 0; i < steps; i++) {
            while (true) {
                int action = agent.getAction(gridSearch);
                gridSearch.playSingleMove(action);
                if (gridSearch.epochEnded()) {
                    break;
                }
            }
        }
        return true;
    }

    @Override
    public void train(Object context, Agent agent, int steps) {
        Environment gridSearch = new GridSearch(agent);
        agent.setTrain(true);

        ReinforcementLearning reinforcementLearning = agent.getAI().getReinforcementLearning();

        while (reinforcementLearning.getCount() < steps) {
            if (reinforcementLearning.getCount() % 10 == 0) {
                System.out.println("Step: " + reinforcementLearning.getCount());
            }
            int action = agent.getAction(gridSearch);
            gridSearch.playSingleMove(action);
        }
    }
}
