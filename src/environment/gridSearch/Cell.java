package environment.gridSearch;

/**
 * Created by Marek on 30/06/2017.
 */
public enum Cell {
    EMPTY, AGENT, TARGET
}
