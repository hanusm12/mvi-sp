package environment.gridSearch;

import agent.Agent;
import environment.Environment;
import environment.EnvironmentFactory;

public class GridSeachEnvironmentFactory implements EnvironmentFactory {
    @Override
    public Environment environment() {
        return new GridSearch(null);
    }

    @Override
    public Agent getAgentToTrain() {
        return null;
    }

    @Override
    public EnvironmentFactory clone() {
        GridSeachEnvironmentFactory newInstance = new GridSeachEnvironmentFactory();
        return newInstance;
    }
}
