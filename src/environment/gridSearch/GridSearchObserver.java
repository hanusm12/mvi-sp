package environment.gridSearch;

import agent.Agent;
import environment.AbstractEnvironmentObserver;
import environment.Environment;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 * Created by Marek on 30/06/2017.
 */
public class GridSearchObserver extends AbstractEnvironmentObserver {

    public GridSearchObserver(int historySize) {
        super(historySize, 20, 0, 4);
    }

    @Override
    public void fillCurrentState(Environment environment, Agent agent, double[] currentState) {
        GridSearch gridSearch = (GridSearch) environment;
        Cell[][] grid = gridSearch.getGrid();

        int index = 0;
        /*for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                currentState.putScalar(0, index + grid[i][j].ordinal(), 1);
                index += 3;
            }
        }*/
        //int index = 0;
        currentState[index + gridSearch.getAgentXPosition()] = 1;
        index+=5;
        currentState[index + gridSearch.getAgentYPosition()] = 1;
        index+=5;
        currentState[index + gridSearch.getTargetXPosition()] = 1;
        index+=5;
        currentState[index + gridSearch.getTargetYPosition()] = 1;
    }

    @Override
    public double[] getHistoryRepresentation(Environment environment, Agent agent) {
        return null;
    }
}
