package environment.gridSearch;

import agent.GenericAgent;
import agent.Agent;
import agent.deep4j.Deep4jAgentAIActorCritic;
import environment.EnvironmentObserver;
import environment.EnvironmentRunner;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;
import poker.player.ActorCriticLoss;
import reinforcementLearning.sync.a3c.A3CLearningSync;
import settings.Settings;
import settings.Utils;

/**
 * Created by Marek on 30/06/2017.
 */
public class GridSearchLearning {

    public static void main(String[] args) {
        int stepsToTrain = 15000;
        Settings.STEPS_TO_TRAIN = stepsToTrain;

        EnvironmentRunner gridSearchRunner = new GridSearchRunner();
        EnvironmentObserver observer = new GridSearchObserver(0);
        MultiLayerNetwork network = createNetwork(observer);
        ComputationGraph networkForActorCritic = createNetworkForActorCritic(observer);


        //Agent agent = new GenericAgent(observer, new MonteCarloAI());
        //Agent agent = new GenericAgent(observer, new Deep4jAgentAI(network, new QLearningNStepSync(network, stepsToTrain)));
        Agent agent = new GenericAgent("", observer, new Deep4jAgentAIActorCritic(networkForActorCritic, new A3CLearningSync()));

        gridSearchRunner.train(null, agent, stepsToTrain);

        gridSearchRunner.evaluateSpecifiedAgent(null, agent, 100);
        System.out.println("Score: " + agent.getTotalReward());
    }

    private static MultiLayerNetwork createNetwork(EnvironmentObserver environmentObserver) {
        MultiLayerConfiguration conf = null;


        conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .weightInit(WeightInit.XAVIER)
                .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, Utils.createScheduleMap(0.01, 0.0005, (int) (Settings.STEPS_TO_TRAIN * 1)))))
                .list()
                .layer(0, new DenseLayer.Builder()
                        .nIn(environmentObserver.getTotalStateSize())
                        .nOut(50)
                        .activation(Activation.RELU)
                        .build())
                .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .nIn(50)
                        .nOut(environmentObserver.getNumberOfNetworkOutputs())
                        .activation(Activation.IDENTITY)
                        .build())
                .pretrain(false).backprop(true)
                .build();


        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        return model;
    }

    public static ComputationGraph createNetworkForActorCritic(EnvironmentObserver observer) {
        ComputationGraphConfiguration.GraphBuilder confB =
                new NeuralNetConfiguration.Builder()
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, Utils.createScheduleMap(0.01, 0.0005, (int) (Settings.STEPS_TO_TRAIN * 1)))))
                        .weightInit(WeightInit.XAVIER)
                        .graphBuilder()
                        .setInputTypes(InputType.feedForward(observer.getTotalStateSize())).addInputs("input")
                        .addLayer("0",
                                new DenseLayer.Builder()
                                        .nIn(observer.getTotalStateSize())
                                        .nOut(50)
                                        .activation(Activation.RELU)
                                        .build(),
                                "input")
                        .addLayer("value",
                                new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                                        .nIn(50)
                                        .nOut(1)
                                        .activation(Activation.IDENTITY)
                                        .build(),
                                "0")
                        .addLayer("softmax",
                                new OutputLayer.Builder(new ActorCriticLoss())
                                        .nIn(50)
                                        .nOut(observer.getNumberOfNetworkOutputs())
                                        .activation(Activation.SOFTMAX)
                                        .build(),
                                "0");

        confB.setOutputs("value", "softmax");
        ComputationGraphConfiguration cgconf = confB.pretrain(false).backprop(true).build();
        ComputationGraph model = new ComputationGraph(cgconf);
        model.init();
        return model;

    }
}
