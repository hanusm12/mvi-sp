package environment;

import agent.Agent;

public interface EnvironmentFactory {
    Environment environment();
    Agent getAgentToTrain();
    EnvironmentFactory clone();
}
