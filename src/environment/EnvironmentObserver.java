package environment;

import agent.Agent;

/**
 * Created by Marek on 30/06/2017.
 */
public interface EnvironmentObserver {
    void fillCurrentState(Environment environment, Agent agent, double[] currentState);
    double[] getHistoryRepresentation(Environment environment, Agent agent);
    int getCurrentStateSize();
    int getHistoryStateSize();
    int getNumberOfHistoryStates();
    int getNumberOfNetworkOutputs();
    int getTotalStateSize();
}
