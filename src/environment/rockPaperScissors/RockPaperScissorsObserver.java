package environment.rockPaperScissors;

import agent.Agent;
import environment.AbstractEnvironmentObserver;
import environment.Environment;
import environment.EnvironmentObserver;

public class RockPaperScissorsObserver extends AbstractEnvironmentObserver {

    public RockPaperScissorsObserver() {
        super(0, 1, 0, 3);
    }

    @Override
    public void fillCurrentState(Environment environment, Agent agent, double[] currentState) {
        currentState[0] = 1;
    }

    @Override
    public double[] getHistoryRepresentation(Environment environment, Agent agent) {
        return new double[0];
    }
}
