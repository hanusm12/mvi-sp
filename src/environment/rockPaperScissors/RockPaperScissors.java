package environment.rockPaperScissors;

import agent.Agent;
import environment.Environment;

public class RockPaperScissors implements Environment {
    private enum Move {
        ROCK, PAPER, SCISSORS
    }


    private Agent firstAgent;
    private Agent secondAgent;
    private boolean isFirstAgentActing = true;
    private Move firstAgentMove;
    private Move secondAgentMove;

    private boolean needNewInstance = false;
    private boolean epochEnded;


    public RockPaperScissors(Agent firstAgent, Agent secondAgent) {
        this.firstAgent = firstAgent;
        this.secondAgent = secondAgent;
    }

    @Override
    public void playSingleMove(int moveIndex) {
        epochEnded = false;
        Move move = null;
        switch (moveIndex) {
            case 0: {
                move = Move.ROCK;
            }
            break;
            case 1: {
                move = Move.PAPER;
            }
            break;
            case 2: {
                move = Move.SCISSORS;
            }
            break;
        }
        if (isFirstAgentActing) {
            firstAgentMove = move;
            isFirstAgentActing = false;
        } else {
            secondAgentMove = move;
            epochEnded = true;
            needNewInstance = true;

            if (firstAgentMove == secondAgentMove) {
                draw();
            } else {
                if (firstAgentMove == Move.ROCK) {
                    if (secondAgentMove == Move.PAPER) {
                        secondAgentWon();
                    } else {
                        firstAgentWon();
                    }
                } else if (firstAgentMove == Move.PAPER) {
                    if (secondAgentMove == Move.ROCK) {
                        firstAgentWon();
                    } else {
                        secondAgentWon();
                    }

                } else if (firstAgentMove == Move.SCISSORS) {
                    if (secondAgentMove == Move.ROCK) {
                        secondAgentWon();
                    } else {
                        firstAgentWon();
                    }
                }
            }

        }
    }

    private void firstAgentWon() {
        firstAgent.epochEnded(1, this);
        secondAgent.epochEnded(-1, this);
    }

    private void secondAgentWon() {
        firstAgent.epochEnded(-1, this);
        secondAgent.epochEnded(1, this);
    }

    private void draw() {
        firstAgent.epochEnded(0, this);
        secondAgent.epochEnded(0, this);
    }

    @Override
    public boolean needNewInstance() {
        return needNewInstance;
    }

    @Override
    public boolean epochEnded() {
        return epochEnded;
    }

    @Override
    public void initializeBeforeEpoch() {

    }

    @Override
    public double getAgentScore(Agent agent, boolean endOfEpoch) {
        return 0;
    }

    @Override
    public Agent getAgentToAct() {
        if (isFirstAgentActing) {
            return firstAgent;
        }
        return secondAgent;
    }
}
