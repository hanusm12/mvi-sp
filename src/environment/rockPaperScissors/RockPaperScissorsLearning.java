package environment.rockPaperScissors;

import agent.Agent;
import agent.GenericAgent;
import agent.deep4j.Deep4jAgentAIActorCritic;
import environment.Environment;
import environment.EnvironmentFactory;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;
import poker.player.ActorCriticLoss;
import reinforcementLearning.sync.a3c.A3CLearningSync;
import settings.Settings;
import settings.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RockPaperScissorsLearning {

    public static void main(String[] args) {
        List<Agent> agents = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ComputationGraph network = createNetwork();
            Agent agent = new GenericAgent("" + i, new RockPaperScissorsObserver(), new Deep4jAgentAIActorCritic(network, new A3CLearningSync()));
            agents.add(agent);
        }


        for (int i = 0; i < 100; i++) {
            Agent worstAgent = findWorstAgent(agents);
            findWorstAgent(agents);
            findWorstAgent(agents);

            agents.remove(worstAgent);

            ComputationGraph network = createNetwork();
            Agent newAgent = new GenericAgent(worstAgent.getName(), new RockPaperScissorsObserver(), new Deep4jAgentAIActorCritic(network, new A3CLearningSync()));
            newAgent.train(new RockPaperScissortFactory(agents, newAgent));


            agents.add(newAgent);
            Agent worstAgentAfterTraining = findWorstAgent(agents);
            if (worstAgent.getName().equals(worstAgentAfterTraining.getName())) {
                agents.remove(newAgent);
                agents.add(worstAgent);
                System.out.println("New agent is not better");
            } else {
                System.out.println("New agent IS better");
            }


        }

    }

    private static Agent findWorstAgent(List<Agent> agents) {
        EnvironmentFactory environmentFactory = new RockPaperScissortFactory(agents);
        for (Agent agent : agents) {
            agent.resetTotalReward();
        }

        Map<Agent, INDArray> probabilities = new HashMap<>();

        for (int i = 0; i < agents.size(); i++) {
            Agent agent = agents.get(i);
            Deep4jAgentAIActorCritic ai = (Deep4jAgentAIActorCritic) agent.getAI();
            ComputationGraph network = ai.getNetwork();
            INDArray res = network.output(Nd4j.create(new double[]{1.0}))[1];
            probabilities.put(agent, res);
            System.out.println(res);
        }


        for (int i = 0; i < 1000000; i++) {
            Environment environment = environmentFactory.environment();

            for (int j = 0; j < 2; j++) {
                Agent agentToAct = environment.getAgentToAct();
                INDArray prob = probabilities.get(agentToAct);
                int action = selectAction(prob);
                environment.playSingleMove(action);
            }
        }

        Agent worstAgent = agents.get(0);
        double worstScore = worstAgent.getTotalReward();

        for (int i = 1; i < agents.size(); i++) {
            Agent agent = agents.get(i);
            if (agent.getTotalReward() < worstScore) {
                worstAgent = agent;
                worstScore = agent.getTotalReward();
            }
        }


        System.out.println(worstAgent.getName() + " - " + worstAgent.getTotalReward() / (double) 1000000);

        return worstAgent;
    }

    private static int selectAction(INDArray prob) {
        double val = Settings.RANDOM.nextDouble();
        for (int i = 0; i < prob.length(); i++) {
            if (val < prob.getDouble(i)) {
                return i;
            } else {
                val -= prob.getDouble(i);
            }
        }
        throw new RuntimeException("There was an error somewhere");
    }

    public static ComputationGraph createNetwork() {
        ComputationGraphConfiguration.GraphBuilder confB =
                new NeuralNetConfiguration.Builder()
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, Utils.createScheduleMap(0.01, 0.0005, (int) (Settings.STEPS_TO_TRAIN * 1)))))
                        .weightInit(WeightInit.XAVIER)
                        .graphBuilder()
                        .setInputTypes(InputType.feedForward(1)).addInputs("input")
                        .addLayer("0",
                                new DenseLayer.Builder()
                                        .nIn(1)
                                        .nOut(1)
                                        .activation(Activation.RELU)
                                        .build(),
                                "input")
                        .addLayer("value",
                                new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                                        .nIn(1)
                                        .nOut(1)
                                        .activation(Activation.IDENTITY)
                                        .build(),
                                "0")
                        .addLayer("softmax",
                                new OutputLayer.Builder(new ActorCriticLoss())
                                        .nIn(1)
                                        .nOut(3)
                                        .weightInit(WeightInit.RELU)
                                        .activation(Activation.SOFTMAX)
                                        .build(),
                                "0");

        confB.setOutputs("value", "softmax");
        ComputationGraphConfiguration cgconf = confB.pretrain(false).backprop(true).build();
        ComputationGraph model = new ComputationGraph(cgconf);
        model.init();
        return model;
    }
}
