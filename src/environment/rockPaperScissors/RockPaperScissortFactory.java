package environment.rockPaperScissors;

import agent.Agent;
import environment.Environment;
import environment.EnvironmentFactory;

import java.util.Collections;
import java.util.List;

public class RockPaperScissortFactory implements EnvironmentFactory {

    private List<Agent> opponents;
    private Agent agentToTrain;

    public RockPaperScissortFactory(List<Agent> agents) {
        opponents = agents;
        agentToTrain = null;
    }

    public RockPaperScissortFactory(List<Agent> opponents, Agent agentToTrain) {
        this.opponents = opponents;
        this.agentToTrain = agentToTrain;
    }

    @Override
    public Environment environment() {
        Collections.shuffle(opponents);
        if (agentToTrain != null) {
            return new RockPaperScissors(agentToTrain, opponents.get(0));
        } else {
            return new RockPaperScissors(opponents.get(0), opponents.get(1));
        }
    }

    @Override
    public Agent getAgentToTrain() {
        return agentToTrain;
    }

    @Override
    public EnvironmentFactory clone() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
