/**
 * Created by mhanus on 5/8/2017.
 */

package settings.deep4jExamples;

import org.nd4j.linalg.api.ndarray.INDArray;

public interface MathFunction {
    INDArray getFunctionValues(INDArray x);

    String getName();
}


