/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;


import com.google.common.util.concurrent.AtomicDouble;
import environment.EnvironmentObserver;
import gui.GameController;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.nd4j.linalg.api.ndarray.INDArray;
import poker.game.*;
import poker.game.observer.networkForEach.GameObserverForEach1;
import poker.player.Player;
import reinforcementLearning.ReinforcementLearningType;
import visualization.Controller;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author bimbo
 */
public class Settings {
    public static int HANDS_PER_TABLE = 6;
    public static int PLAYERS_PER_TABLE = 6;

    public static final int THREADS = Runtime.getRuntime().availableProcessors();
    //public static final int THREADS = 1;


    public static Player playerToTrain = null;

    public static boolean ONLY_SIMULATION_WITHOUT_TRAINING = false;
    public static boolean LOG_GAME = false;

    public static boolean CALCULATE_EXACT_WINRATE = true;

    public static int NUMBER_OF_PLAYERS = 30;


    public static int MINIMUM_TABLES_TO_RUN = 15000;
    public static final int MAXIMUM_TABLES_TO_RUN = 200000;
    public static int STEPS_TO_TRAIN = 100000;
    public static long TIME_TO_TRAIN = 180000;

    public static final int DEFAULT_STACK = 1000;
    public static final int STARTING_SMALL_BLIND = 5;

    public static final int ALLIN_SIMULATIONS_PER_HAND_MISSING = 20;

    public static final EnvironmentObserver DEFAULT_OBSERVER = new GameObserver4_potOdds(2);

    public static int HAND1VAL = 10;
    public static int HAND2VAL = 11;
    public static int SUITED = 1;

    public static final String FOLDER_WITH_NETWORKS = "networks/";

    public static final String NETWORK_SUFFIX = ".net";
    public static final String OBSERVER_SUFFIX = ".ob";

    public static Random RANDOM = new Random();

    public static GameController controller;

    public static Controller VISUALIZATION_CONTROLLER;


}
