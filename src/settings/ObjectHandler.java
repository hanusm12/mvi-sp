package settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ObjectHandler {

	public static void save(Serializable object, String fileName) throws IOException {
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		fos = new FileOutputStream(fileName);
		out = new ObjectOutputStream(fos);
		out.writeObject(object);
		out.close();
	}

	public static Object load(String fileName) throws IOException, ClassNotFoundException {
		Object result;
		FileInputStream fis = null;
		ObjectInputStream in = null;
		fis = new FileInputStream(fileName);
		in = new ObjectInputStream(fis);
		result = in.readObject();
		in.close();
		return result;
	}
}
