package settings;

import environment.EnvironmentObserver;

import org.mockito.Mockito;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import poker.enums.Color;
import poker.game.*;
import poker.player.Player;

import java.util.*;

public class Utils {
    public static int indexOfBiggestInArray(double[] array) {
        double biggestValue = array[0];
        int biggestIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] >= biggestValue) {
                biggestIndex = i;
                biggestValue = array[i];
            }
        }
        return biggestIndex;
    }

    public static int indexOfBiggestInArray(int[] array) {
        int biggestValue = array[0];
        int biggestIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] >= biggestValue) {
                biggestIndex = i;
                biggestValue = array[i];
            }
        }
        return biggestIndex;
    }

    public static double valueOfBiggestInArray(double[] array) {
        double biggestValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] >= biggestValue) {
                biggestValue = array[i];
            }
        }
        return biggestValue;
    }

    public static int valueOfBiggestInArray(int[] array) {
        int biggestValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] >= biggestValue) {
                biggestValue = array[i];
            }
        }
        return biggestValue;
    }

    public static int valueOfLowestInArray(int[] array) {
        int lowestValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < lowestValue) {
                lowestValue = array[i];
            }
        }
        return lowestValue;
    }

    public static double valueOfLowestInArray(double[] array) {
        double lowestValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < lowestValue) {
                lowestValue = array[i];
            }
        }
        return lowestValue;
    }

    public static void preparePlayersForEvaluating(List<Player> players) {
        for (Player player : players) {
            player.setTotalBalance(0);
            player.setHandsPlayed(0);
        }
    }

    public static void preparePlayersForLearning(List<Player> players) {
        for (Player player : players) {
            player.setTotalBalance(0);
            player.setHandsPlayed(0);
            //    NeuralNetworksDeep4J predictionModel = (NeuralNetworksDeep4J) player.getPlayerAI().getPredictionModel();
            //    player.setqLearningNeuralNetworks(new QLearningNeuralNetworks(predictionModel.getNetwork()));
        }
    }


    public static void printPlayersProfis(List<Player> players) {
        Collections.sort(players);
        for (Player player : players) {
            System.out.println(player.getName() + " : " + calculateBigBlindsPer100Hands(player) + "bb/100. Hands played: " + player.getHandsPlayed());
        }
    }

    public static double calculateBigBlindsPer100Hands(Player player) {
        double chipsPer100Hands = (player.getTotalBalance() * 100 / (double) (player.getHandsPlayed()));
        double bigBlindsPer100Hands = chipsPer100Hands / (double) (2 * Settings.STARTING_SMALL_BLIND);
        return bigBlindsPer100Hands;
    }

    public static double[] bitSetToDouble(BitSet bitSet) {
        double result[] = new double[bitSet.size()];
        for (int nextBit = 0; nextBit < bitSet.size(); nextBit++) {
            nextBit = bitSet.nextSetBit(nextBit);
            if (nextBit == -1) {
                return result;
            } else {
                result[nextBit] = 1;
            }
        }
        return result;
    }

    public static INDArray createINDArrayFromBitSet(BitSet bitSet, int size) {
        INDArray result = Nd4j.zeros(1, size);
        for (int nextBit = 0; nextBit < bitSet.size(); nextBit++) {
            nextBit = bitSet.nextSetBit(nextBit);
            if (nextBit == -1) {
                return result;
            } else {
                result.putScalar(new int[]{0, nextBit}, 1);
            }
        }
        return result;
    }

    public static double normalizeValue(double value, double min, double max) {
        return ((value - min) / (double) (max - min)) * 2 - 1;
    }


    public static Map<Player, Double> getPlayersProfits(List<Player> players) {
        Map<Player, Double> playersProfits = new HashMap<>();
        for (Player player : players) {
            playersProfits.put(player, calculateBigBlindsPer100Hands(player));
        }
        return playersProfits;
    }

    public static int getTotalNetworkSize(int currentStateSize, int historySize, int numberOfPossibleActions) {
        return currentStateSize + historySize * (currentStateSize + numberOfPossibleActions);
    }

    public static int getIndexOfBiggest(INDArray array) {
        return (int) Nd4j.argMax(array).getDouble(0);
    }


    public static void printBasicVisualization(Player player) {
        EnvironmentObserver observer = player.getObserver();
        Game game = Mockito.mock(Game.class);
        Mockito.when(game.getState()).thenReturn(0);
        Mockito.when(game.getBoard()).thenReturn(null);
        Mockito.when(game.getMinimumSize()).thenReturn(10);
        Mockito.when(game.getPotSize()).thenReturn(15);


        double inputs[][] = new double[169][];
        int index = 0;

        for (int i = 2; i <= 14; i++) {
            for (int j = 2; j <= 14; j++) {
                Player testPlayer = new Player(player);
                if (j < i) {
                    testPlayer.addHoleCards(new Card(i, Color.CLUB), new Card(j, Color.CLUB));
                } else {
                    testPlayer.addHoleCards(new Card(i, Color.CLUB), new Card(j, Color.DIAMOND));
                }

                inputs[index] = testPlayer.getObservation(game, false);
                index++;
            }
        }

        /*
        INDArray output = player.getNetwork().output(Nd4j.create(inputs), false);
        for (int i = 0; i < 169 * observer.getNumberOfNetworkOutputs(); i++) {
            output.putScalar(i, output.getDouble(i) * 100);
        }
        Settings.VISUALIZATION_CONTROLLER.refreshGridPane(output);
*/
    }

    public static Map<Integer, Double> createScheduleMap(double max, double min, int steps) {
        Map<Integer, Double> result = new HashMap<>();
        int PARTS = 1000;
        double difference = ( min - max) / PARTS;
        double ratio = max;

        for (int step = 0; step < steps; step += (steps / PARTS)) {
            result.put(step, ratio);
            ratio += difference;
        }

        return result;
    }
}
