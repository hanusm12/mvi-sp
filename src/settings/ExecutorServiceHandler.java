package settings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceHandler {

    private static ExecutorService executorService;
    private static List<Future> futures;

    public static Future<?> submitTask(Runnable task) {
        return executorService.submit(task);
    }

    public static Future submitTask(Callable task) {
        return executorService.submit(task);
    }

    public static void start() {
        executorService =
                Executors.newFixedThreadPool(Settings.THREADS);
        futures = new ArrayList<>();
    }

    public static void stop() {
        executorService.shutdownNow();
    }

    public static List<Object> waitForTasksToFinish(List<Future> tasks) {
        List<Object> res = new ArrayList<>();
        try {
            for (Future task : tasks) {
                res.add(task.get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return res;
    }


}
