package settings;

public class HandRanking implements Comparable<HandRanking> {
    private static String[] HAND_RANKS = {"BAD!!", "High Card", "Pair", "Two Pair", "Three of a Kind",
            "Straight", "Flush", "Full House", "Four of a Kind", "Straight Flush"};

    public int category;
    public int rankWithinCategory;

    public HandRanking(int twoPlusTwoResult) {
        this.category = twoPlusTwoResult >> 12;
        this.rankWithinCategory = twoPlusTwoResult & 0x00000FFF;
    }

    public HandRanking(int category, int rankWithinCategory) {
        this.category = category;
        this.rankWithinCategory = rankWithinCategory;
    }

    @Override
    public int compareTo(HandRanking o) {
        int res = -Integer.compare(category, o.category);
        if (res == 0) {
            res = -Integer.compare(rankWithinCategory, o.rankWithinCategory);
        }
        return res;
    }

    @Override
    public String toString() {
        return HAND_RANKS[category];
    }
}
