/*package poker.player;

import environment.Environment;
import environment.EnvironmentObserver;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.sync.qLearning.oneStep.experienceReplay.Experience;
import sun.nio.ch.AbstractPollArrayWrapper;

public class PlayerNetworkForEachStartingHand extends Player {
    private MultiLayerNetwork[][][] networks;

    public PlayerNetworkForEachStartingHand(String name, MultiLayerNetwork networks[][][], EnvironmentObserver observer, ReinforcementLearning reinforcementLearning) {
        super(name, null, observer, reinforcementLearning);
        this.networks = networks;
    }

    public PlayerNetworkForEachStartingHand(PlayerNetworkForEachStartingHand player) {
        super(player);
        this.networks = player.networks;
    }

    @Override
    public int getAction(Environment environment) {
        if (train && currentState != null) {
            //There was already action before this one -> save it into memory, since now we have future state
            double newCurrentState[] = getObservation(environment, true);
            double currentReward = environment.getAgentScore(this, false);
            reinforcementLearning.moveEnded(new Experience(currentState, newCurrentState, networkOutput, action, currentReward - lastExperienceReward));
            lastExperienceReward = currentReward;
            currentState = newCurrentState;
        } else {
            currentState = getObservation(environment, true);
        }


        networkOutput = getCorrectNetwork().output(Nd4j.create(currentState), false);

        int action = selectAction(networkOutput);
        changeLastAction(action);
        return action;
    }

    public MultiLayerNetwork getCorrectNetwork() {
        return networks[getHigherCard().getValueForArrays()][getLowerCard().getValueForArrays()][isHoleCardsSuited()];
    }

    public MultiLayerNetwork[][][] getNetworks() {
        return networks;
    }
}
*/