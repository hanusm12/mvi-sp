package poker.player.inputDataCreation;

import poker.game.Game;
import poker.game.PlayerActionHistory;
import poker.player.Player;

import java.io.Serializable;
import java.util.List;

public class DataCreator implements Serializable {

    private int historySize;
    private List<CurrentStateInformation> currentStateInformationList;
    private int currentStateSize;
    private boolean learnyOnFly;

    public DataCreator(int historySize, List<CurrentStateInformation> currentStateInformationList, boolean learnyOnFly) {
        this.historySize = historySize;
        this.currentStateInformationList = currentStateInformationList;
        this.currentStateSize = 0;
        for (CurrentStateInformation currentStateInformation : currentStateInformationList) {
            currentStateSize += currentStateInformation.getSize();
        }
        this.learnyOnFly = learnyOnFly;
    }

    public State getCurrentState(Game game, Player player, List<PlayerActionHistory> playerActions) {
        double result[] = new double[getTotalSize()];
        fillCurrentState(game, player, result);
        fillHistory(playerActions, result);
        return new State(result);
    }

    public void fillHistory(List<PlayerActionHistory> playerActions, double[] result) {
        int arrayIndex = currentStateSize;
        int historyIndex = playerActions.size() - 1;
        int historyEntriesAdded = 0;
        while (historyEntriesAdded < playerActions.size() && historyEntriesAdded < historySize) {
            if (historyIndex >= 0) {
                PlayerActionHistory playerActionHistory = playerActions.get(historyIndex);

                for (int i = 0; i < currentStateSize; i++) {
                    try {
                        result[arrayIndex] = playerActionHistory.getCurrentState().getMyNetworkInput()[i];
                    } catch (Exception e) {
                        System.out.println("e");
                    }
                    arrayIndex++;
                }

                result[arrayIndex + playerActionHistory.getMove()] = 1;
                arrayIndex += 5;
            } else {
                break;
            }
            historyIndex--;
            historyEntriesAdded++;
        }
    }

    private void fillCurrentState(Game game, Player player, double result[]) {
        int index = 0;
        for (CurrentStateInformation currentStateInformation : currentStateInformationList) {
            currentStateInformation.fillCurrentStateIntoArray(game, player, index, result);
            index += currentStateInformation.getSize();
        }
    }

    public int getTotalSize() {
        return currentStateSize + historySize * (currentStateSize + 5);
    }

    public boolean isLearnyOnFly() {
        return learnyOnFly;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (learnyOnFly) {
            builder.append("iterative-");
        } else {
            builder.append("bulk-");
        }
        builder.append(historySize + "-");
        builder.append(currentStateInformationList.get(0).toString());
        for (int i = 1; i < currentStateInformationList.size(); i++) {
            builder.append("+" + currentStateInformationList.get(i).toString());
        }
        return builder.toString();
    }
}
