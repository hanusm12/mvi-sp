package poker.player.inputDataCreation;

import poker.game.Game;
import poker.player.Player;

import java.io.Serializable;

public class PotOdds implements CurrentStateInformation, Serializable {
    @Override
    public int getSize() {
        return 6;
    }

    @Override
    public void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double[] array) {
        int toCall = game.getMinimumSize() - player.getChipsIngameThisRound();
        double potOdds = toCall / (double) (toCall + game.getPotSize());

        //split into buckets
        double buckets[] = new double[]{0, 0.1, 0.2, 0.3, 0.4, 0.5};
        int index = 0;
        for (int i = 0; i < buckets.length; i++) {
            if (potOdds <= buckets[i]) {
                index = i;
                break;
            }
        }
        array[startIndex + index] = 1;

    }

    @Override
    public String toString() {
        return "PotOdds";
    }
}
