package poker.player.inputDataCreation;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.Arrays;

public class State {
    private double[] inputs;

    public State(double[] inputs) {
        this.inputs = inputs;
    }

    public INDArray getDeep4jInput() {
        INDArray result = Nd4j.create(inputs);
        return result;
    }

    public double[] getMyNetworkInput() {
        return inputs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        return Arrays.equals(inputs, state.inputs);

    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(inputs);
    }
}
