package poker.player.inputDataCreation;

import poker.game.Game;
import poker.player.Player;

import java.io.Serializable;

public class OpponentsToAct implements CurrentStateInformation, Serializable{

    @Override
    public int getSize() {
        return 6;
    }

    @Override
    public void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double[] array) {
        int playersToAct = game.getPlayersToAct() - 1;
        array[startIndex+playersToAct] = 1;
    }

    @Override
    public String toString() {
        return "OpponentsToAct";
    }
}
