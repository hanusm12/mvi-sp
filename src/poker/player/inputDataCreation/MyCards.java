package poker.player.inputDataCreation;

import poker.game.Game;
import poker.player.Player;
import settings.Utils;

import java.io.Serializable;

public class MyCards implements CurrentStateInformation, Serializable {

    @Override
    public int getSize() {
        return 28;
    }

    @Override
    public void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double[] array) {
        array[startIndex + player.getHigherCard().getValueForArrays()] = 1;
        startIndex += 13;
        array[startIndex + player.getLowerCard().getValueForArrays()] = 1;
        startIndex += 13;
        if (player.isHoleCardsSuited() == 1) {
            array[startIndex] = 1;
        } else {
            array[startIndex + 1] = 1;
        }
    }

    @Override
    public String toString() {
        return "MyCards";
    }
}
