package poker.player.inputDataCreation;

import poker.game.Game;
import poker.player.Player;

public interface CurrentStateInformation {
    int getSize();
    void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double array[]);
}
