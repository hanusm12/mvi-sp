package poker.player.inputDataCreation;

import poker.game.Board;
import poker.game.Card;
import poker.game.Game;
import poker.player.Player;
import settings.TwoPlusTwo;
import settings.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BoardStructureWithHand implements CurrentStateInformation, Serializable {

    @Override
    public int getSize() {
        return 93;
    }

    @Override
    public void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double[] array) {
        Board board = game.getBoard();
        List<Integer> cardList = new ArrayList<>();
        cardList.add(player.getHigherCard().getIntRepresentationForTwoPlusTwo());
        cardList.add(player.getLowerCard().getIntRepresentationForTwoPlusTwo());

        if (board.getFlop() != null) {
            array[startIndex + board.getFlop()[0].getValue() - 1] = 1;
            startIndex += 14;
            array[startIndex + board.getFlop()[1].getValue() - 1] = 1;
            startIndex += 14;
            array[startIndex + board.getFlop()[2].getValue() - 1] = 1;
            startIndex += 14;
            cardList.add(board.getFlop()[0].getIntRepresentationForTwoPlusTwo());
            cardList.add(board.getFlop()[1].getIntRepresentationForTwoPlusTwo());
            cardList.add(board.getFlop()[2].getIntRepresentationForTwoPlusTwo());
        } else {
            array[startIndex] = 1;
            startIndex += 14;
            array[startIndex] = 1;
            startIndex += 14;
            array[startIndex] = 1;
            startIndex += 14;
        }
        if (board.getTurn() != null) {
            array[startIndex + board.getTurn().getValue() - 1] = 1;
            startIndex += 14;
            cardList.add(board.getTurn().getIntRepresentationForTwoPlusTwo());
        } else {
            array[startIndex] = 1;
            startIndex += 14;
        }

        if (board.getRiver() != null) {
            array[startIndex + board.getRiver().getValue() - 1] = 1;
            startIndex += 14;
            cardList.add(board.getRiver().getIntRepresentationForTwoPlusTwo());
        } else {
            array[startIndex] = 1;
            startIndex += 14;
        }

        int flushCards[] = getFlushCards(board);
        //there can be 0-5 cards of same color
        array[startIndex + Utils.valueOfBiggestInArray(flushCards)] = 1;
        startIndex += 6;

        flushCards[player.getHigherCard().getColor().ordinal()]++;
        flushCards[player.getLowerCard().getColor().ordinal()]++;

        //there can be 1-7, so we make it 0-6
        array[startIndex + Utils.valueOfBiggestInArray(flushCards) - 1] = 1;
        startIndex += 7;

        if (cardList.size() == 2) {
            //Preflop
            array[startIndex] = 1;
        } else {
            int handRank = 0;
            if (cardList.size() == 5) {
                handRank = TwoPlusTwo.lookupHand5(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4)});
            } else if (cardList.size() == 6) {
                handRank = TwoPlusTwo.lookupHand6(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4), cardList.get(5)});
            } else if (cardList.size() == 7) {
                handRank = TwoPlusTwo.lookupHand7(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4), cardList.get(5), cardList.get(6)});
            }
            int handCategory = handRank >> 12;
            array[startIndex + handCategory] = 1;

        }


    }

    private int[] getFlushCards(Board board) {
        int cardsOfThatColor[] = new int[4];
        if (board.getFlop() != null) {
            cardsOfThatColor[board.getFlop()[0].getColor().ordinal()]++;
            cardsOfThatColor[board.getFlop()[1].getColor().ordinal()]++;
            cardsOfThatColor[board.getFlop()[2].getColor().ordinal()]++;
            if (board.getTurn() != null) {
                cardsOfThatColor[board.getTurn().getColor().ordinal()]++;
                if (board.getRiver() != null) {
                    cardsOfThatColor[board.getRiver().getColor().ordinal()]++;
                }
            }
        }

        return cardsOfThatColor;
    }

    @Override
    public String toString() {
        return "BoardStructureWithHand";
    }
}
