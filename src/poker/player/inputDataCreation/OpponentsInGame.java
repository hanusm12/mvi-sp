package poker.player.inputDataCreation;

import poker.game.Game;
import poker.player.Player;

import java.io.Serializable;

/**
 * Created by mhanus on 4/15/2017.
 */
public class OpponentsInGame implements CurrentStateInformation, Serializable {

    @Override
    public int getSize() {
        return 5;
    }

    @Override
    public void fillCurrentStateIntoArray(Game game, Player player, int startIndex, double[] array) {
        array[startIndex+game.getPlayersInGame()-2] = 1;
    }

    @Override
    public String toString() {
        return "OpponentsInGame";
    }
}
