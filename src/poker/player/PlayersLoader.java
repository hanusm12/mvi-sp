package poker.player;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;

import agent.deep4j.Deep4jAgentAI;
import agent.deep4j.Deep4jAgentAIActorCritic;
import environment.EnvironmentObserver;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.async.a3c.A3CLearningAsync;
import reinforcementLearning.async.qLearning.QLearningNStepAsync;
import reinforcementLearning.sync.a3c.A3CLearningSync;
import reinforcementLearning.sync.qLearning.nStep.QLearningNStepSync;
import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSync;
import settings.ObjectHandler;
import settings.Settings;
import settings.Utils;


public class PlayersLoader {

    public static void savePlayer(Player player) {
        try {
            EnvironmentObserver observer = player.getObserver();
            ObjectHandler.save((Serializable) observer, Settings.FOLDER_WITH_NETWORKS + player.getName() + Settings.OBSERVER_SUFFIX);
            Deep4jAgentAI ai = (Deep4jAgentAI) player.getAI();
            ModelSerializer.writeModel(ai.getNetwork(), Settings.FOLDER_WITH_NETWORKS + player.getName() + Settings.NETWORK_SUFFIX, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Player loadPlayer(String name) {
        try {
            EnvironmentObserver observer = (EnvironmentObserver) ObjectHandler.load(Settings.FOLDER_WITH_NETWORKS + name + Settings.OBSERVER_SUFFIX);
            MultiLayerNetwork multiLayerNetwork = ModelSerializer.restoreMultiLayerNetwork(Settings.FOLDER_WITH_NETWORKS + name + Settings.NETWORK_SUFFIX);
            Player player = new Player(name, observer, new Deep4jAgentAI(multiLayerNetwork, null));
            return player;
        } catch (Exception e) {
            System.out.println("Couldn't load player with name: " + name);
        }
        return null;
    }

    public static List<Player> loadPlayers() {
        List<Player> players = new ArrayList<>();

        for (int i = 0; i < Settings.NUMBER_OF_PLAYERS; i++) {
            Player player = loadPlayer("plr" + i);
            if (player == null) {
                player = createNewPlayer("plr" + i, false);
            }
            players.add(player);
        }

        return players;
    }

    public static Player createNewPlayer(String name, boolean train) {
        MultiLayerNetwork multiLayerNetwork = createNetwork(Settings.DEFAULT_OBSERVER);
        ReinforcementLearning reinforcementLearning = null;
        if (train) {
            reinforcementLearning = new QLearningNStepAsync();
        }
        return new Player(name, Settings.DEFAULT_OBSERVER, new Deep4jAgentAI(multiLayerNetwork, reinforcementLearning));
    }

    public static Player createNewPlayerUsingA3C(String name, boolean train) {
        ComputationGraph networkForActorCritic = createNetworkForActorCritic(Settings.DEFAULT_OBSERVER);
        ReinforcementLearning reinforcementLearning = null;
        if (train) {
            reinforcementLearning = new A3CLearningAsync();
        }
        return new Player(name, Settings.DEFAULT_OBSERVER, new Deep4jAgentAIActorCritic(networkForActorCritic, reinforcementLearning));
    }

    public static ComputationGraph createNetworkForActorCritic(EnvironmentObserver observer) {
        ComputationGraphConfiguration.GraphBuilder confB =
                new NeuralNetConfiguration.Builder()
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, Utils.createScheduleMap(0.01, 0.0005, (int) (Settings.STEPS_TO_TRAIN * 1)))))
                        .weightInit(WeightInit.XAVIER)
                        .graphBuilder()
                        .setInputTypes(InputType.feedForward(observer.getTotalStateSize())).addInputs("input")
                        .addLayer("0",
                                new DenseLayer.Builder()
                                        .nIn(observer.getTotalStateSize())
                                        .nOut(50)
                                        .activation(Activation.RELU)
                                        .build(),
                                "input")
                        .addLayer("1",
                                new DenseLayer.Builder()
                                        .nIn(50)
                                        .nOut(50)
                                        .activation(Activation.RELU)
                                        .build(),
                                "0")
                        .addLayer("value",
                                new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                                        .nIn(50)
                                        .nOut(1)
                                        .activation(Activation.IDENTITY)
                                        .build(),
                                "1")
                        .addLayer("softmax",
                                new OutputLayer.Builder(new ActorCriticLoss())
                                        .nIn(50)
                                        .nOut(observer.getNumberOfNetworkOutputs())
                                        .activation(Activation.SOFTMAX)
                                        .build(),
                                "1");

        confB.setOutputs("value", "softmax");
        ComputationGraphConfiguration cgconf = confB.pretrain(false).backprop(true).build();
        ComputationGraph model = new ComputationGraph(cgconf);
        model.init();
        return model;

    }

    public static MultiLayerNetwork createNetwork(EnvironmentObserver observer) {
        MultiLayerConfiguration conf = null;


        conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .weightInit(WeightInit.XAVIER)
                //Updater uses iteration schedule type, due to Epochs not incrementing...or I dont know how they are incrementing
                .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, Utils.createScheduleMap(0.01, 0.0005, (int) (Settings.STEPS_TO_TRAIN * 1)))))
                .list()
                .layer(0, new DenseLayer.Builder()
                        .nIn(observer.getTotalStateSize())
                        .nOut(50)
                        .activation(Activation.RELU)
                        .build())
                .layer(1, new DenseLayer.Builder()
                        .nIn(50)
                        .nOut(50)
                        .activation(Activation.RELU)
                        .build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .nIn(50)
                        .nOut(observer.getNumberOfNetworkOutputs())
                        .activation(Activation.IDENTITY)
                        .build())
                .pretrain(false).backprop(true)
                .build();


        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        return model;

    }
}
