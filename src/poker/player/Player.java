package poker.player;

import java.io.Serializable;

import agent.AI;
import agent.GenericAgent;
import agent.Agent;
import agent.GenericAgent;
import environment.Environment;
import environment.EnvironmentObserver;
import poker.enums.Action;
import poker.game.*;
import settings.Settings;

public class Player extends GenericAgent implements Comparable<Player>, Serializable {
    protected int chips, chipsIngameThisRound, chipsInGame, positionOnTable;
    protected long totalBalance;
    protected long handsPlayed;
    protected Card holeCards[];
    protected Card higherCard, lowerCard;
    protected boolean isIngame, isAllIn, actedThisBettingRound;

    protected Agent agent;

    public Player(String name, EnvironmentObserver observer, AI ai) {
        super(name, observer, ai);
        holeCards = new Card[2];
        totalBalance = 0;
        this.name = name;
    }

    public Player(Player player) {
        super(player.getName(), player.observer, player.ai);
        holeCards = new Card[2];
        this.name = player.getName();
    }

    public Player clone() {
        return new Player(name, observer, ai.clone());
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActedThisBettingRound() {
        return actedThisBettingRound;
    }

    public void setActedThisBettingRound(boolean actedThisBettingRound) {
        this.actedThisBettingRound = actedThisBettingRound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void epochEnded(double reward, Environment environment) {
        super.epochEnded(reward, environment);
    }

    public String getName() {
        return name;
    }

    public boolean isIsAllIn() {
        return isAllIn;
    }

    public void setTotalBalance(long totalBalance) {
        this.totalBalance = totalBalance;
    }

    public void setIsAllIn(boolean isAllIn) {
        this.isAllIn = isAllIn;
    }

    public void addTotalBalance(long balance) {
        this.totalBalance += balance;
    }

    public void setIsIngame(boolean isIngame) {
        this.isIngame = isIngame;
    }

    public long getTotalBalance() {
        return this.totalBalance;
    }

    public long getHandsPlayed() {
        return handsPlayed;
    }

    public int getChipsIngameThisRound() {
        return chipsIngameThisRound;
    }

    private boolean putChipsInGame(int chipsToPutToGame, Game game) {
        if (chips > chipsToPutToGame) {
            chips -= chipsToPutToGame;
            chipsIngameThisRound += chipsToPutToGame;
            chipsInGame += chipsToPutToGame;
            return false;
        } else {
            chipsIngameThisRound += chips;
            chipsInGame += chips;
            chips = 0;
            isAllIn = true;
            game.incPlayersAllIn();
            return true;
        }
    }

    public boolean isIsIngame() {
        return isIngame;
    }

    public void setChips(int chips) {
        this.chips = chips;
    }

    public void handEnded() {
        int chipsWon = chips - Settings.DEFAULT_STACK;
        totalBalance += chipsWon;
        chips = Settings.DEFAULT_STACK;
        handsPlayed++;

    }

    public Move playSelectedMove(Game game, int move) {
        int original, toCall, raiseSize, toRaise;
        int minimum = game.getMinimumSize(), totalPot = game.getPotSize();

        switch (move) {
            case 0: {
                if (minimum == chipsIngameThisRound) {
                    return new Move(Action.CHECK, chipsIngameThisRound, chipsIngameThisRound);
                }
                // FOLD
                isIngame = false;
                return new Move(Action.FOLD, chipsIngameThisRound,
                        chipsIngameThisRound);
            }
            case 1: {
                // CHECK / CALL
                if (minimum == chipsIngameThisRound) {
                    return new Move(Action.CHECK, chipsIngameThisRound, chipsIngameThisRound);
                } else {
                    toCall = minimum - chipsIngameThisRound;
                    original = chipsIngameThisRound;
                    putChipsInGame(toCall, game);
                    return new Move(Action.CALL, original, chipsIngameThisRound);
                }
            }
            case 2: {
                //BET 75%
                toCall = minimum - chipsIngameThisRound;
                original = chipsIngameThisRound;
                if (toCall >= chips) {
                    putChipsInGame(toCall, game);
                    return new Move(Action.CALL, original, chipsIngameThisRound);
                } else {
                    raiseSize = minimum + (int) ((totalPot + toCall) * 0.75);
                    toRaise = raiseSize - chipsIngameThisRound;
                    putChipsInGame(toRaise, game);
                    return new Move(Action.RAISE, original, chipsIngameThisRound);
                }
            }
        }
        return null;
    }

    public int getChipsInGame() {
        return chipsInGame;
    }

    public void setChipsInGame(int chipsInGame) {
        this.chipsInGame = chipsInGame;
    }

    public void setChipsIngameThisRound(int chipsIngameThisRound) {
        this.chipsIngameThisRound = chipsIngameThisRound;
    }

    public void addChipsIngameThisRound(int chips) {
        this.chipsIngameThisRound += chips;
    }

    public void addChipsIngame(int chips) {
        this.chipsInGame += chips;
    }

    public void PostBlind(int size, Game game) {
        putChipsInGame(size, game);
    }

    public void addHoleCards(Card card1, Card card2) {
        holeCards[0] = card1;
        holeCards[1] = card2;
        if (card1.getValue() > card2.getValue()) {
            higherCard = card1;
            lowerCard = card2;
        } else {
            higherCard = card2;
            lowerCard = card1;
        }
    }

    public void clearHoleCards() {
        holeCards[0] = null;
        holeCards[1] = null;
    }

    public void addHandsPlayed(long handsPlayed) {
        this.handsPlayed += handsPlayed;
    }

    public int getChips() {
        return chips;
    }

    public Card getFirstCard() {
        return holeCards[0];
    }

    public Card getSecondCard() {
        return holeCards[1];
    }

    public Card getHigherCard() {
        return higherCard;
    }

    public Card getLowerCard() {
        return lowerCard;
    }

    public int isHoleCardsSuited() {
        if (holeCards[0].getColor() == holeCards[1].getColor()) {
            return 1;
        }
        return 0;
    }

    public Card[] getHoleCards() {
        return holeCards;
    }

    public void addChips(int chips) {
        this.chips += chips;
    }

    @Override
    public int compareTo(Player t) {
        if (this.totalBalance > t.getTotalBalance()) {
            return -1;
        } else if (this.totalBalance < t.getTotalBalance()) {
            return 1;
        } else {
            return 0;
        }
    }


    public void setHandsPlayed(long handsPlayed) {
        this.handsPlayed = handsPlayed;
    }

    public int getPositionOnTable() {
        return positionOnTable;
    }

    public void nextPositionOnTable() {
        if (positionOnTable == 0) {
            positionOnTable = 5;
        } else {
            positionOnTable--;
        }
    }



    public void setPositionOnTable(int positionOnTable) {
        this.positionOnTable = positionOnTable;
    }

    /*public Gradient calculateGradient(Experience experience) {

        //First, get outputs for all states in experiences
        INDArray inputs = Nd4j.zeros(experience.getSize(), getDataCreator().getTotalSize());
        INDArray outputs = Nd4j.create(experience.getSize(), 5);

        int index = 0;
        List<State> states = experience.getStates();
        for (State state : states) {
            inputs.putRow(index, state.getDeep4jInput());
            outputs.putRow(index, experience.getOutputs().get(index));
            index++;
        }


        //Now lets set correct values for our chosen actions
        //TODO: use discount factor as a constant
        index = 0;
        double reward = 0;
        for (int j = 0; j < experience.getSize(); j++) {
            reward = experience.getImmediateReward(j) + 0.99 * reward;
            outputs.putScalar(index, experience.getAction(j), reward);
            index++;
        }

        cloneNetwork.setInput(inputs);
        cloneNetwork.setLabels(outputs);
        cloneNetwork.computeGradientAndScore();
        if (cloneNetwork.score() > 1000000) {
            System.out.println("WTF?");
        }
        Settings.descriptiveStatistics.addValue(cloneNetwork.score());
        Settings.COUNTER.increment();
        if (Settings.COUNTER.intValue() % 250 == 0) {
            double mean = Settings.descriptiveStatistics.getMean();
            System.out.println(mean);
        }

        return cloneNetwork.gradient();
    }*/

    /*public void applyGradient(Gradient gradient, int batchSize, INDArray test) {
        synchronized (getDeep4jNetwork()) {
            //System.out.println("Before: " + getDeep4jNetwork().output(test));
            getDeep4jNetwork().getUpdater().update(getDeep4jNetwork(), gradient, 1, batchSize);
            getDeep4jNetwork().params().subi(gradient.gradient());
            //System.out.println("After: " + getDeep4jNetwork().output(test));

        }
    }*/


}
