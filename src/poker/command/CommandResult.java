package poker.command;

public class CommandResult {
    private GameStage nextGameStage;
    private boolean continueInSimulation;

    public CommandResult(GameStage nextGameStage, boolean continueInSimulation) {
        this.nextGameStage = nextGameStage;
        this.continueInSimulation = continueInSimulation;
    }

    public GameStage getNextGameStage() {
        return nextGameStage;
    }

    public void setNextGameStage(GameStage nextGameStage) {
        this.nextGameStage = nextGameStage;
    }

    public boolean isContinueInSimulation() {
        return continueInSimulation;
    }

    public void setContinueInSimulation(boolean continueInSimulation) {
        this.continueInSimulation = continueInSimulation;
    }
}
