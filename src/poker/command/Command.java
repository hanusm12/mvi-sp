package poker.command;

import poker.game.Game;

public interface Command {
    CommandResult exec(Game game);
}
