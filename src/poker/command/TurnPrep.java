package poker.command;

import poker.game.Game;

public class TurnPrep implements Command {
    @Override
    public CommandResult exec(Game game) {
        game.turnInitialization();
        return new CommandResult(GameStage.TURN_ACTIONS, false);
    }
}
