package poker.command;

import poker.game.Game;

public class GamePrep implements Command {


    @Override
    public CommandResult exec(Game game) {
        game.preGameInitialization();
        return new CommandResult(GameStage.PF_PREP, true);
    }
}
