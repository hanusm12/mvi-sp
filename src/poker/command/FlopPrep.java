package poker.command;

import poker.game.Game;

public class FlopPrep implements Command {

    @Override
    public CommandResult exec(Game game) {
        game.flopInitialization();
        return new CommandResult(GameStage.FLOP_ACTIONS, false);
    }
}
