package poker.command;

import poker.game.Game;

public class TurnActions implements Command {
    @Override
    public CommandResult exec(Game game) {
        if (game.playerActions()) {
            return new CommandResult(GameStage.TURN_ACTIONS, false);
        } else {
            if (game.getPlayersInGame() - game.getPlayersAllIn() <= 1 || game.isEndHandNow()) {
                return new CommandResult(GameStage.END_GAME, false);
            }
            return new CommandResult(GameStage.RIVER_PREP, true);
        }
    }
}
