package poker.command;

import poker.game.Game;

public class RiverPrep implements Command {
    @Override
    public CommandResult exec(Game game) {
        game.riverInitialization();
        return new CommandResult(GameStage.RIVER_ACTIONS, false);
    }
}
