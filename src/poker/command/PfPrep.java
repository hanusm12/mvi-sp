package poker.command;

import poker.game.Game;

public class PfPrep implements Command {

    @Override
    public CommandResult exec(Game game) {
        game.preHandInitialization();
        return new CommandResult(GameStage.PF_ACTIONS, true);
    }
}
