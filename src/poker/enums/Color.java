/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poker.enums;

import java.io.Serializable;

/**
 * @author bimbo
 */
public enum Color implements Serializable {
    SPADE, HEART, CLUB, DIAMOND
}
