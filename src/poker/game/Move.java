/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poker.game;

import poker.enums.Action;

/**
 *
 * @author Bimbo
 */
public class Move {

    private Action action;
    private int startChipsInPot, totalChipsInPot;

    public Move(Action action, int startChipsInPot, int totalChipsInPot) {
        this.action = action;
        this.startChipsInPot = startChipsInPot;
        this.totalChipsInPot = totalChipsInPot;
    }

    public Action getAction() {
        return action;
    }

    public int getStartChipsInPot() {
        return startChipsInPot;
    }

    public int getTotalChipsInPot() {
        return totalChipsInPot;
    }
}
