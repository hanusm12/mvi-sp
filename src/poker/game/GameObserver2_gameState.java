package poker.game;

import agent.Agent;
import environment.AbstractEnvironmentObserver;
import environment.Environment;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import poker.player.Player;

import java.io.Serializable;

public class GameObserver2_gameState extends AbstractEnvironmentObserver implements Serializable {

    public GameObserver2_gameState(int numberOfHistoryStates) {
        super(numberOfHistoryStates, 32, 4, 3);
    }

    @Override
    public void fillCurrentState(Environment environment, Agent agent, double[] currentState) {
        Player player = (Player) agent;
        Game game = (Game) environment;

        int startIndex = 0;
        currentState[startIndex + player.getHigherCard().getValueForArrays()] = 1;
        startIndex += 13;
        currentState[startIndex + player.getLowerCard().getValueForArrays()] = 1;
        startIndex += 13;
        if (player.isHoleCardsSuited() == 1) {
            currentState[startIndex] = 1;
        } else {
            currentState[startIndex + 1] = 1;
        }
        startIndex += 2;

        currentState[startIndex + game.getState()] = 1;
        startIndex += 4;
    }

    @Override
    public double[] getHistoryRepresentation(Environment environment, Agent agent) {
        Game game = (Game) environment;
        int startIndex = 0;
        double[] historyState = new double[getHistoryStateSize()];

        historyState[startIndex + game.getState()] = 1;
        startIndex += 4;
        return historyState;
    }
}
