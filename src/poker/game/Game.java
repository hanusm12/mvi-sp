/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poker.game;

import java.io.Serializable;
import java.util.*;

import agent.Agent;
import environment.Environment;
import org.apache.commons.net.ProtocolCommandSupport;
import poker.command.*;
import poker.game.cardDealer.CardDealer;
import poker.player.Player;
import settings.*;


/**
 * @author bimbo
 */
public class Game implements Serializable, Environment {
    protected static EnumMap<GameStage, Command> COMMANDS_MAP;

    protected Player playerOfInterest = null;
    protected Player actingPlayer = null;
    protected List<Player> players;
    protected int dealer = -1;
    protected int currentPlayer, playersInGame, playersAllIn, potSize, minimumSize, round = 0, playersToAct;
    protected Board board;
    protected Random random = new Random();
    protected boolean endHandNow = false;
    protected CardDealer cardDealer;
    protected ExactWinCalculator exactWinCalculator;

    protected int currentAgentMove;
    protected CommandResult currentStage;
    protected boolean needNewInstance = false;
    protected boolean epochEnded = false;

    static {
        COMMANDS_MAP = new EnumMap<>(GameStage.class);

        COMMANDS_MAP.put(GameStage.GAME_PREP, new GamePrep());
        COMMANDS_MAP.put(GameStage.PF_PREP, new PfPrep());
        COMMANDS_MAP.put(GameStage.PF_ACTIONS, new PfActions());
        COMMANDS_MAP.put(GameStage.FLOP_PREP, new FlopPrep());
        COMMANDS_MAP.put(GameStage.FLOP_ACTIONS, new FlopActions());
        COMMANDS_MAP.put(GameStage.TURN_PREP, new TurnPrep());
        COMMANDS_MAP.put(GameStage.TURN_ACTIONS, new TurnActions());
        COMMANDS_MAP.put(GameStage.RIVER_PREP, new RiverPrep());
        COMMANDS_MAP.put(GameStage.RIVER_ACTIONS, new RiverActions());
    }


    public Game(List<Player> plrs, CardDealer cardDealer) {
        players = new ArrayList<>(plrs);
        for (Player player : players) {
            if (player.equals(Settings.playerToTrain)) {
                playerOfInterest = player;
                break;
            }
        }
        this.cardDealer = cardDealer;
        preGameInitialization();
    }

    public int getMinimumSize() {
        return minimumSize;
    }

    public Board getBoard() {
        return board;
    }

    public void incPlayersAllIn() {
        playersAllIn++;
    }


    public CardDealer getCardDealer() {
        return cardDealer;
    }

    public int getPlayersInGame() {
        return playersInGame;
    }

    public int getPotSize() {
        return potSize;
    }

    public int getPlayersAllIn() {
        return playersAllIn;
    }

    public List<Player> getPlayers() {
        return players;
    }

    protected Player nextPlayer() {
        if (currentPlayer >= players.size() - 1) {
            currentPlayer = 0;
        } else {
            currentPlayer++;
        }
        return players.get(currentPlayer);
    }

    protected Player nextInGamePlayer() {
        while (true) {
            Player player = nextPlayer();
            if (player.isIsIngame() && !player.isIsAllIn()) {
                return player;
            }
        }
    }

    protected void selectWinner() {
        if (playersInGame == 1) {
            for (Player player : players) {
                if (player.isIsIngame()) {
                    player.addChips(potSize);
                    if (Settings.LOG_GAME) {
                        System.out.println(player + " has won " + potSize + " chips.");
                    }
                    break;
                }
            }
        } else {
            createPotAndSelectWinner();
        }
    }


    private void createPotAndSelectWinner() {
        Map<Player, Integer> chipDistribution = new HashMap<>();
        Pot pot = new Pot(board);
        boolean playerToTrainInPot = false;
        for (Player player : players) {
            if (player.isIsIngame()) {
                pot.addPlayer(player);
                chipDistribution.put(player, 0);
                if (player.equals(Settings.playerToTrain)) {
                    playerToTrainInPot = true;
                }
            }
        }
        pot.setPotSize(potSize);


        if (playerToTrainInPot && (board.getRiver() == null)) {
            int simulationsToRun = 1;
            if (board.getFlop() == null) {
                simulationsToRun = 5 * Settings.ALLIN_SIMULATIONS_PER_HAND_MISSING;
            } else if (board.getTurn() == null) {
                simulationsToRun = 2 * Settings.ALLIN_SIMULATIONS_PER_HAND_MISSING;
            } else if (board.getRiver() == null) {
                simulationsToRun = Settings.ALLIN_SIMULATIONS_PER_HAND_MISSING;
            }

            for (int i = 0; i < simulationsToRun; i++) {
                pot.getChipDistribution(Settings.LOG_GAME, this, chipDistribution);
            }
            for (Map.Entry<Player, Integer> entry : chipDistribution.entrySet()) {
                Integer originalValue = entry.getValue();
                entry.setValue(originalValue / simulationsToRun);
            }
        } else {
            pot.getChipDistribution(Settings.LOG_GAME, this, chipDistribution);
        }


        for (Map.Entry<Player, Integer> entry : chipDistribution.entrySet()) {
            entry.getKey().addChips(entry.getValue() - entry.getKey().getChips());
        }
    }

    protected void postBlinds() {
        Player tmp = nextPlayer();
        tmp.PostBlind(Settings.STARTING_SMALL_BLIND, this);
        potSize += tmp.getChipsIngameThisRound();
        if (Settings.LOG_GAME) {
            System.out.println(tmp.toString() + " has posted small blind of " + Settings.STARTING_SMALL_BLIND + " chips");
        }
        tmp = nextPlayer();
        tmp.PostBlind(Settings.STARTING_SMALL_BLIND * 2, this);
        potSize += tmp.getChipsIngameThisRound();
        if (Settings.LOG_GAME) {
            System.out.println(tmp.toString() + " has posted big blind of " + Settings.STARTING_SMALL_BLIND * 2 + " chips");
        }
    }

    private void fold(Player plr) {
        playersInGame--;
        playersToAct--;
        if (Settings.LOG_GAME) {
            System.out.println(plr + " has Folded with " + plr.getHoleCards()[0] + " " + plr.getHoleCards()[1]);
        }
    }

    private void check(Player plr) {
        playersToAct--;
        if (Settings.LOG_GAME) {
            System.out.println(plr + " has Checked with " + plr.getHoleCards()[0] + " " + plr.getHoleCards()[1]);
        }
    }

    private void call(Move move, Player plr) {
        playersToAct--;
        potSize += move.getTotalChipsInPot() - move.getStartChipsInPot();
        if (Settings.LOG_GAME) {
            System.out.println(plr + " has Called for " + (move.getTotalChipsInPot() - move.getStartChipsInPot())
                    + " chips  with " + plr.getHoleCards()[0] + " " + plr.getHoleCards()[1] + " Pot size: "
                    + potSize);
        }
    }


    private void raise(Move move, Player plr) {
        minimumSize = move.getTotalChipsInPot();
        potSize += move.getTotalChipsInPot() - move.getStartChipsInPot();

        if (plr.isIsAllIn()) {
            playersToAct = playersInGame - playersAllIn;
        } else {
            playersToAct = playersInGame - playersAllIn - 1;
        }

        if (Settings.LOG_GAME) {
            System.out.println(
                    plr + " has Raised to " + plr.getChipsIngameThisRound() + " chips with " + plr.getHoleCards()[0]
                            + " " + plr.getHoleCards()[1] + " Pot size: " + potSize);
        }
    }

    public int getPlayersToAct() {

        return playersToAct;
    }


    public boolean playerActions() {
        if (playersToAct > 0) {
            Move move = actingPlayer.playSelectedMove(this, currentAgentMove);
            if (exactWinCalculator != null) {
                exactWinCalculator.agentDidAction(actingPlayer, currentAgentMove);
            }
            processMove(actingPlayer, move);
            if (actingPlayer.equals(Settings.playerToTrain) && !actingPlayer.isIsIngame()) {
                endHandNow = true;
                return false;
            }
            if (playersInGame == 1) {
                return false;
            }
            if (playersToAct > 0) {
                actingPlayer = nextInGamePlayer();
                return true;
            }
        }
        return false;
    }

    public boolean isEndHandNow() {
        return endHandNow;
    }

    protected void processMove(Player player, Move move) {
        switch (move.getAction()) {
            case FOLD: {
                fold(player);
                break;
            }
            case CHECK: {
                check(player);
                break;
            }
            case CALL: {
                call(move, player);
                break;
            }
            case RAISE: {
                raise(move, player);
                break;
            }
        }
    }

    protected void moveDealer() {
        if (dealer >= players.size() - 1) {
            dealer = 0;
        } else {
            dealer++;
        }
        for (Player player : players) {
            player.nextPositionOnTable();
        }
    }

    public int getDealer() {
        return dealer;
    }

    public void dealFlop() {
        Card flop[] = new Card[3];
        flop[0] = cardDealer.getBoardCard();
        flop[1] = cardDealer.getBoardCard();
        flop[2] = cardDealer.getBoardCard();
        board.setFlop(flop);
        if (Settings.LOG_GAME) {
            System.out.println("***********FLOP************: " + board.getFlop()[0] + " " + board.getFlop()[1] + " "
                    + board.getFlop()[2]);
        }
    }

    public void dealTurn() {
        board.setTurn(cardDealer.getBoardCard());
        if (Settings.LOG_GAME) {
            System.out.println("***********TURN************: " + board.getTurn());
        }
    }

    public void dealRiver() {
        board.setRiver(cardDealer.getBoardCard());
        if (Settings.LOG_GAME) {
            System.out.println("***********RIVER***********: " + board.getRiver());
        }
    }

    protected void dealHoleCards(Player player) {
        Card cards[] = cardDealer.getHoleCards(player);

        player.addHoleCards(cards[0], cards[1]);
        if (Settings.LOG_GAME) {
            System.out.println(player.toString() + " received " + cards[0] + " " + cards[1] + " as hole cards.");
        }
    }

    protected void setPlayersChipsToDefaultStack() {
        for (Player player : players) {
            player.setChips(Settings.DEFAULT_STACK);
        }
    }

    public void riverInitialization() {
        minimumSize = 0;
        currentPlayer = dealer;
        dealRiver();
        playersToAct = playersInGame - playersAllIn;

        for (Player plr : players) {
            plr.setChipsIngameThisRound(0);
            plr.setActedThisBettingRound(false);
        }

        actingPlayer = nextInGamePlayer();
    }

    public void flopInitialization() {
        minimumSize = 0;
        currentPlayer = dealer;
        dealFlop();
        playersToAct = playersInGame - playersAllIn;

        for (Player plr : players) {
            plr.setChipsIngameThisRound(0);
            plr.setActedThisBettingRound(false);
        }
        actingPlayer = nextInGamePlayer();
    }

    public void turnInitialization() {
        minimumSize = 0;
        currentPlayer = dealer;
        dealTurn();
        playersToAct = playersInGame - playersAllIn;

        for (Player plr : players) {
            plr.setChipsIngameThisRound(0);
            plr.setActedThisBettingRound(false);
        }
        actingPlayer = nextInGamePlayer();
    }


    public void preHandInitialization() {
        if (Settings.playerToTrain != null && Settings.playerToTrain.isTraining() && Settings.CALCULATE_EXACT_WINRATE) {
            // player of interest is always the first one
            exactWinCalculator = new ExactWinCalculator(players.get(0));
        }
        if (Settings.LOG_GAME) {
            System.out.println();
            System.out.println();
        }
        moveDealer();
        setPlayersChipsToDefaultStack();

        cardDealer.reset();

        minimumSize = Settings.STARTING_SMALL_BLIND * 2;
        currentPlayer = dealer;
        potSize = 0;
        board = new Board();
        playersInGame = players.size();
        playersToAct = players.size();
        playersAllIn = 0;
        if (playerOfInterest != null) {
            playerOfInterest.setChipsIngameThisRound(0);
            playerOfInterest.setIsIngame(true);
            playerOfInterest.setIsAllIn(false);
            playerOfInterest.setChipsInGame(0);
            dealHoleCards(playerOfInterest);
            playerOfInterest.setActedThisBettingRound(false);
        }
        for (Player plr : players) {
            if (plr != playerOfInterest) {
                plr.setChipsIngameThisRound(0);
                plr.setIsIngame(true);
                plr.setIsAllIn(false);
                plr.setChipsInGame(0);
                dealHoleCards(plr);
                plr.setActedThisBettingRound(false);
            }
        }
        postBlinds();
        endHandNow = false;
        epochEnded = false;

        actingPlayer = nextInGamePlayer();

    }

    public void preGameInitialization() {
        dealer = random.nextInt(players.size());
        currentPlayer = dealer;
        for (int i = 0; i < 6; i++) {
            Player player = nextPlayer();
            player.setPositionOnTable(i);
        }
        currentStage = COMMANDS_MAP.get(GameStage.PF_PREP).exec(this);
    }

    @Override
    public void playSingleMove(int moveIndex) {
        while (currentStage.isContinueInSimulation()) {
            this.currentAgentMove = moveIndex;
            currentStage = COMMANDS_MAP.get(currentStage.getNextGameStage()).exec(this);
        }

        currentStage.setContinueInSimulation(true);
        if (currentStage.getNextGameStage() == GameStage.END_GAME) {
            round++;
            epochEnded = true;
            if (!endHandNow) {
                selectWinner();
            }
            for (Player player : players) {
                player.epochEnded(getAgentScore(player, true), this);
            }
            if (round == Settings.HANDS_PER_TABLE) {
                needNewInstance = true;
            }
        }

    }

    @Override
    public boolean needNewInstance() {
        return needNewInstance;
    }

    @Override
    public boolean epochEnded() {
        return epochEnded;
    }

    public int getRound() {
        return round;
    }

    @Override
    public void initializeBeforeEpoch() {
        COMMANDS_MAP.get(GameStage.PF_PREP).exec(this);
        currentStage = new CommandResult(GameStage.PF_ACTIONS, true);
    }

    @Override
    public double getAgentScore(Agent agent, boolean endOfEpoch) {
        if (endOfEpoch
                && agent.isTraining()
                && Settings.CALCULATE_EXACT_WINRATE
                && agent.getLastAction() != 0
                && playersInGame > 1) {
            return exactWinCalculator.gameEnded(this);
        }

        return (((Player) agent).getChips() - Settings.DEFAULT_STACK) / (double) 1000;
    }

    @Override
    public Agent getAgentToAct() {
        if (epochEnded) {
            currentStage = COMMANDS_MAP.get(GameStage.PF_PREP).exec(this);
        }
        return actingPlayer;
    }

    public int getState() {
        switch (currentStage.getNextGameStage()) {
            case PF_ACTIONS:
                return 0;
            case FLOP_ACTIONS:
                return 1;
            case TURN_ACTIONS:
                return 2;
            case RIVER_ACTIONS:
                return 3;
        }
        return -200000;
    }

}
