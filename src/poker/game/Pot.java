package poker.game;

import java.io.InterruptedIOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.Callable;

import poker.player.Player;
import settings.Settings;
import settings.TwoPlusTwo;

/**
 * @author bimbo
 */
public class Pot implements Serializable {

    private List<Player> players;
    private int potSize;
    private Board board;

    public Pot(Board brd) {
        players = new ArrayList<>();
        potSize = 0;
        board = brd;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player x) {
        players.add(x);
    }

    public void addChipsFromEachPlayer(int size) {
        potSize = players.size() * size;
    }

    public void setPotSize(int potSize) {
        this.potSize = potSize;
    }

    public void getChipDistribution(boolean logGame, Game game, Map<Player, Integer> chips) {
        boolean addedFlop = false, addedTurn = false, addedRiver = false;
        if (board.getFlop() == null) {
            game.dealFlop();
            addedFlop = true;
        }
        if (board.getTurn() == null) {
            game.dealTurn();
            addedTurn = true;
        }
        if (board.getRiver() == null) {
            game.dealRiver();
            addedRiver = true;
        }

        Hand hands[] = calculatePlayersHands(players);

        if (hands[0].compareTo(hands[1]) == 0) {
            int playersWithSameHand = getPlayersWithSameHand(players, hands);
            splitPotBetweenPlayers(hands, playersWithSameHand, logGame, chips);
        } else {
            for (int i = 0; i < hands.length; i++) {
                Integer chipsSoFar = chips.get(hands[i].player);
                if (i > 0) {
                    chips.put(hands[i].player, chipsSoFar + hands[i].player.getChips());
                } else {
                    chips.put(hands[i].player, chipsSoFar + hands[i].player.getChips() + potSize);
                }
            }

            if (logGame) {
                System.out.println(hands[0].player + " has won " + potSize + " chips.");
            }
        }

        if (addedFlop) {
            game.getCardDealer().cardNotInPlay(board.getFlop()[0]);
            game.getCardDealer().cardNotInPlay(board.getFlop()[1]);
            game.getCardDealer().cardNotInPlay(board.getFlop()[2]);
            board.setFlop(null);
        }
        if (addedTurn) {
            game.getCardDealer().cardNotInPlay(board.getTurn());
            board.setTurn(null);
        }
        if (addedRiver) {
            game.getCardDealer().cardNotInPlay(board.getRiver());
            board.setRiver(null);
        }

    }

    private void splitPotBetweenPlayers(Hand[] hands, int playersWithSameHand, boolean logGame, Map<Player, Integer> chips) {
        int chipsForEachPlayer = potSize / playersWithSameHand;
        for (int i = 0; i < hands.length; i++) {
            if (i < playersWithSameHand) {
                chips.put(hands[i].player, chips.get(hands[i].player) + hands[i].player.getChips() + chipsForEachPlayer);
                if (logGame) {
                    System.out.println(hands[i].player + " has won " + chipsForEachPlayer + " chips from splited pot.");
                }
            } else {
                chips.put(hands[i].player, chips.get(hands[i].player) + hands[i].player.getChips());
            }
        }

        if (potSize % playersWithSameHand != 0) {
            Random random = new Random();
            int handIndex = random.nextInt(playersWithSameHand);
            chips.put(hands[handIndex].player, chips.get(hands[handIndex].player) + (potSize % playersWithSameHand));
        }
    }

    private int getPlayersWithSameHand(List<Player> players, Hand hands[]) {
        int playersWithSameHand = 2;
        for (int j = 2; j < players.size(); j++) {
            if (hands[0].compareTo(hands[j]) == 0) {
                playersWithSameHand++;
            } else {
                break;
            }
        }
        return playersWithSameHand;
    }

    private Hand[] calculatePlayersHands(List<Player> players) {
        Hand hands[] = new Hand[players.size()];
        for (int i = 0; i < players.size(); i++) {
            int result = 0;
            Player player = players.get(i);
            try {
                result = TwoPlusTwo.lookupHand7(new int[]{player.getFirstCard().getIntRepresentationForTwoPlusTwo(), player.getSecondCard().getIntRepresentationForTwoPlusTwo(), board.getFlop()[0].getIntRepresentationForTwoPlusTwo(), board.getFlop()[1].getIntRepresentationForTwoPlusTwo(), board.getFlop()[2].getIntRepresentationForTwoPlusTwo(), board.getTurn().getIntRepresentationForTwoPlusTwo(), board.getRiver().getIntRepresentationForTwoPlusTwo()});
            } catch (NullPointerException e) {
                System.out.println(e);
            }
            hands[i] = new Hand(player, result >> 12, result & 0x00000FFF);
        }
        Arrays.sort(hands);
        return hands;
    }


}


