package poker.game;

import agent.AI;
import agent.Agent;
import agent.deep4j.Deep4jAgentAI;
import environment.Environment;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.util.FastMath;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import poker.game.cardDealer.CardDealerFactory;
import poker.game.cardDealer.SpecifiedCardDealerFactory;
import poker.player.Player;
import poker.player.PlayersLoader;
import reinforcementLearning.ReinforcementLearning;
import reinforcementLearning.sync.qLearning.nStep.QLearningNStepSync;
import reinforcementLearning.sync.qLearning.oneStep.QLearningOneStepSync;
import settings.Settings;
import settings.Utils;

import java.util.*;

public class GameRunner {
    private List<CalculateRequest> requests = new ArrayList<>();
    private Map<Agent, CalculateRequest> calculateRequestMap = new HashMap<>();


    public Map<Player, Double> findPlayersProfits(List<Player> players, int tablesToRun, CardDealerFactory cardDealerFactory) {
        Settings.playerToTrain = null;
        Map<Player, Double> profits = new HashMap<>();
        int startedTables = 0;

        int finishedTables = 0;


        for (Player player : players) {
            player.setTrain(false);
            calculateRequestMap.put(player, new CalculateRequest());
            profits.put(player, (double) 0);
            player.resetTotalReward();
        }

        long start = System.currentTimeMillis();


        while (finishedTables < tablesToRun) {
            if (!requests.isEmpty()) {
                CalculateRequest calculateRequest = requests.remove(0);
                int[] moves = calcuateMoves(calculateRequest);


                for (int i = 0; i < calculateRequest.getIndex(); i++) {
                    Game game = (Game) calculateRequest.getEnvironments()[i];
                    game.playSingleMove(moves[i]);

                    if (game.needNewInstance) {

                        for (Player player : game.getPlayers()) {
                            double playerScore = game.getAgentScore(player, true);
                            Double val = profits.get(player);
                            profits.put(player, val + playerScore);
                        }

                        if (game.getRound() == 6) {
                            finishedTables++;
                        } else {
                            game.initializeBeforeEpoch();
                            addEnvironemntMove(game);
                        }
                    } else {
                        addEnvironemntMove(game);
                    }
                }
            } else {
                if (startedTables < tablesToRun) {
                    Game game = new Game(createPlayersForGame(players, null), cardDealerFactory.cardDealer());
                    addEnvironemntMove(game);
                    startedTables++;
                } else {
                    boolean nonFullRequestAdded = false;
                    for (Agent agent : players) {
                        CalculateRequest calculateRequest = calculateRequestMap.get(agent);
                        if (calculateRequest.getIndex() > 0) {
                            requests.add(calculateRequest);
                            calculateRequestMap.put(agent, new CalculateRequest());
                            nonFullRequestAdded = true;
                        }
                    }
                }
            }
        }

        return profits;

    }

    public Player findWorstPlayer(List<Player> players, int tablesToRun, CardDealerFactory cardDealerFactory) {
        //Settings.LOG_GAME = true;
        //CalculateRequest.BATCH_SIZE = 1;

        Settings.playerToTrain = null;
        int startedTables = 0;

        int finishedTables = 0;

        Map<Player, StandardDeviation> standardDeviations = new HashMap<>();
        Map<Player, Mean> means = new HashMap<>();

        for (Player player : players) {
            player.setTrain(false);
            calculateRequestMap.put(player, new CalculateRequest());
            player.resetTotalReward();
            standardDeviations.put(player, new StandardDeviation());
            means.put(player, new Mean());
        }

        long start = System.currentTimeMillis();


        while (finishedTables < tablesToRun) {
            if (!requests.isEmpty()) {
                CalculateRequest calculateRequest = requests.remove(0);
                int[] moves = calcuateMoves(calculateRequest);


                for (int i = 0; i < calculateRequest.getIndex(); i++) {
                    Game game = (Game) calculateRequest.getEnvironments()[i];
                    game.playSingleMove(moves[i]);

                    if (game.needNewInstance) {

                        for (Player player : game.getPlayers()) {
                            double playerScore = game.getAgentScore(player, true) * 100;
                            standardDeviations.get(player).increment(playerScore);
                            means.get(player).increment(playerScore);
                        }

                        if (game.getRound() == 6) {
                            finishedTables++;
                        } else {
                            game.initializeBeforeEpoch();
                            addEnvironemntMove(game);
                        }
                    } else {
                        addEnvironemntMove(game);
                    }
                }
            } else {
                if (startedTables < tablesToRun) {
                    Game game = new Game(createPlayersForGame(players, null), cardDealerFactory.cardDealer());
                    addEnvironemntMove(game);
                    startedTables++;
                } else {
                    boolean nonFullRequestAdded = false;
                    for (Agent agent : players) {
                        CalculateRequest calculateRequest = calculateRequestMap.get(agent);
                        if (calculateRequest.getIndex() > 0) {
                            requests.add(calculateRequest);
                            calculateRequestMap.put(agent, new CalculateRequest());
                            nonFullRequestAdded = true;
                        }
                    }
                    if (!nonFullRequestAdded) {
                        throw new UnsupportedOperationException("WTF?");
                    }
                }
            }
        }

        double minMaxInterval = Double.MAX_VALUE;
        Player worstPlayer = null;

        double maxMinInterval = -Double.MAX_VALUE;
        Player bestPlayer = null;
        for (Player player : players) {
            StandardDeviation standardDeviation = standardDeviations.get(player);
            Mean mean = means.get(player);
            double maxInterval = mean.getResult() + 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
            double minInterval = mean.getResult() - 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
            if (maxInterval < minMaxInterval) {
                worstPlayer = player;
                minMaxInterval = maxInterval;
            }
            if (minInterval > maxMinInterval) {
                maxMinInterval = minInterval;
                bestPlayer = player;
            }
        }
        System.out.println("Best player is: " + bestPlayer.getName() + ". Minimum winrate: " + maxMinInterval);
        System.out.println("Worst player is: " + worstPlayer.getName() + ". Maximum winrate: " + minMaxInterval);
        return worstPlayer;
    }

    private int[] calcuateMoves(CalculateRequest calculateRequest) {
        double inputs[][] = new double[calculateRequest.getIndex()][];
        for (int i = 0; i < calculateRequest.getIndex(); i++) {
            Agent agent = calculateRequest.getAgents()[i];
            Environment environment = calculateRequest.getEnvironments()[i];

            inputs[i] = agent.getObservation(environment, true);
        }

        /*
        if (calculateRequest.getAgents()[0] instanceof PlayerNetworkForEachStartingHand) {
            PlayerNetworkForEachStartingHand player = (PlayerNetworkForEachStartingHand) calculateRequest.getAgents()[0];
            output = player.getCorrectNetwork().output(Nd4j.create(inputs), false);
        } else {
            output = calculateRequest.getAgents()[0].getNetwork().output(Nd4j.create(inputs), false);
        }*/

        int[] actions = calculateRequest.getAgents()[0].getAI().getActions(inputs);

        for (int i = 0; i < calculateRequest.getIndex(); i++) {
            calculateRequest.getAgents()[i].changeLastAction(actions[i]);
        }
        return actions;
    }

    private void addEnvironemntMove(Environment environment) {
        Agent agentToAct = environment.getAgentToAct();
        CalculateRequest calculateRequest = calculateRequestMap.get(agentToAct);
        calculateRequest.add(agentToAct, environment);
        if (calculateRequest.isComplete()) {
            requests.add(calculateRequest);
            calculateRequestMap.put(agentToAct, new CalculateRequest());
        }
    }

    public List<Player> createPlayersForGame(List<Player> players, Player playerOfInterest) {
        List<Player> newPlayersList = new ArrayList<>();

        Collections.shuffle(players);
        if (playerOfInterest == null) {
            for (int i = 0; i < Settings.PLAYERS_PER_TABLE; i++) {
                newPlayersList.add(copyOfPlayer(players.get(i)));
            }
        } else {
            newPlayersList.add(copyOfPlayer(playerOfInterest));
            int index = 0;
            while (newPlayersList.size() < 6) {
                Player player = players.get(index++);
                if (!player.equals(playerOfInterest)) {
                    newPlayersList.add(copyOfPlayer(player));
                }
            }
        }

        return newPlayersList;
    }

    private Player copyOfPlayer(Player player) {
        //if (player instanceof PlayerNetworkForEachStartingHand) {
        //    return new PlayerNetworkForEachStartingHand((PlayerNetworkForEachStartingHand) player);
        //} else
        //if (player instanceof PlayerGeneratingInputs) {
        //    return new PlayerGeneratingInputs((PlayerGeneratingInputs) player);
        //}
        return new Player(player);
    }

    public boolean evaluateSpecifiedAgent(Object context, Agent agent, CardDealerFactory cardDealerFactory) {
        //CalculateRequest.BATCH_SIZE = 1;
        //Settings.LOG_GAME = true;


        List<Player> players = (List<Player>) context;
        Player newPlayer = (Player) agent;
        Settings.playerToTrain = newPlayer;

        requests.clear();

        for (Player player : players) {
            player.setTrain(false);
            calculateRequestMap.put(player, new CalculateRequest());
            player.resetTotalReward();
        }


        int startedTables = 0;
        int tablesToRun = Settings.MAXIMUM_TABLES_TO_RUN;
        int finishedTables = 0;

        StandardDeviation standardDeviation = new StandardDeviation();
        Mean mean = new Mean();

        long start = System.currentTimeMillis();

        int handsSinceLastIncrement = 0;
        double agentScore = 0;

        while (finishedTables < tablesToRun) {
            if (!requests.isEmpty()) {
                CalculateRequest calculateRequest = requests.remove(0);
                int[] moves = calcuateMoves(calculateRequest);


                for (int i = 0; i < calculateRequest.getIndex(); i++) {
                    Game game = (Game) calculateRequest.getEnvironments()[i];
                    game.playSingleMove(moves[i]);

                    if (game.epochEnded) {
                        Player player = game.getPlayers().get(game.getPlayers().indexOf(newPlayer));
                        double agentScoreFromCurrentGame = game.getAgentScore(player, true) * 100;
                        agentScore += agentScoreFromCurrentGame;
                        handsSinceLastIncrement++;
                        if (handsSinceLastIncrement == 100) {
                            standardDeviation.increment(agentScore);
                            mean.increment(agentScore);
                            agentScore = 0;
                            handsSinceLastIncrement = 0;
                        }

                        newPlayer.addToTotalReward(agentScoreFromCurrentGame);
                    }
                    if (game.needNewInstance) {
                        if (game.getRound() == 6) {
                            finishedTables++;
                            /*if (finishedTables > Settings.MINIMUM_TABLES_TO_RUN) {
                                double minInterval = mean.getResult() - 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
                                double maxInterval = mean.getResult() + 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));

                                if (minInterval >= 0) {
                                    long end = System.currentTimeMillis();
                                    System.out.println("New player is better");
                                    System.out.println("His expected profit is: " + minInterval + " - " + maxInterval + " bb/100");
                                    System.out.println("Simulation speed was: " + (mean.getN() * 100 / (double) ((end - start) / 1000)) + "/s");
                                    System.out.println("His expected profit is: " + mean.getResult());
                                    return true;
                                }
                                if (maxInterval < 0) {
                                    long end = System.currentTimeMillis();
                                    System.out.println("New player is NOT better, I am sorry.");
                                    System.out.println("His expected profit is: " + minInterval + " - " + maxInterval + " bb/100");
                                    System.out.println("Simulation speed was: " + (mean.getN() * 100 / (double) ((end - start) / 1000)) + "/s");
                                    System.out.println("His expected profit is: " + mean.getResult());

                                    return false;
                                }
                            }*/
                        } else {
                            game.initializeBeforeEpoch();
                            addEnvironemntMove(game);
                        }
                    } else {
                        addEnvironemntMove(game);
                    }
                }
            } else {
                if (startedTables < tablesToRun) {
                    Game game = new Game(createPlayersForGame(players, newPlayer), cardDealerFactory.cardDealer());
                    addEnvironemntMove(game);
                    startedTables++;
                } else {
                    for (Agent plr : players) {
                        CalculateRequest calculateRequest = calculateRequestMap.get(plr);
                        if (calculateRequest.getIndex() > 0) {
                            requests.add(calculateRequest);
                            calculateRequestMap.put(plr, new CalculateRequest());
                        }
                    }
                }
            }


        }
        System.out.println("Couldn't decide if new player was better or worse");
        double minInterval = mean.getResult() - 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
        double maxInterval = mean.getResult() + 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
        System.out.println("His expected profit is: " + minInterval + " - " + maxInterval + " bb/100");
        long end = System.currentTimeMillis();
        System.out.println("Simulation speed was: " + (mean.getN() * 100 / (double) ((end - start) / 1000)) + "/s");
        System.out.println("His expected profit is: " + mean.getResult());

        return false;
    }


    public double getPlayerWinrateApprox(Object context, Agent agent, CardDealerFactory cardDealerFactory, int tablesToRun) {


        List<Player> players = (List<Player>) context;
        Player newPlayer = (Player) agent;
        Settings.playerToTrain = newPlayer;
        for (Player player : players) {
            player.setTrain(false);
            calculateRequestMap.put(player, new CalculateRequest());
            player.resetTotalReward();
        }


        int startedTables = 0;
        int finishedTables = 0;

        StandardDeviation standardDeviation = new StandardDeviation();
        Mean mean = new Mean();

        long start = System.currentTimeMillis();

        int handsSinceLastIncrement = 0;
        double agentScore = 0;

        while (finishedTables < tablesToRun) {
            if (!requests.isEmpty()) {
                CalculateRequest calculateRequest = requests.remove(0);
                int[] moves = calcuateMoves(calculateRequest);


                for (int i = 0; i < calculateRequest.getIndex(); i++) {
                    Game game = (Game) calculateRequest.getEnvironments()[i];
                    game.playSingleMove(moves[i]);

                    if (game.needNewInstance) {

                        Player player = game.getPlayers().get(game.getPlayers().indexOf(newPlayer));
                        double agentScoreFromCurrentGame = game.getAgentScore(player, true) * 100;
                        agentScore += agentScoreFromCurrentGame;
                        handsSinceLastIncrement++;
                        if (handsSinceLastIncrement == 100) {
                            standardDeviation.increment(agentScore);
                            mean.increment(agentScore);
                            agentScore = 0;
                            handsSinceLastIncrement = 0;
                        }

                        newPlayer.addToTotalReward(agentScoreFromCurrentGame);


                        if (game.getRound() == 6) {
                            finishedTables++;

                        } else {
                            game.initializeBeforeEpoch();
                            addEnvironemntMove(game);
                        }
                    } else {
                        addEnvironemntMove(game);
                    }
                }
            } else {
                if (startedTables < tablesToRun) {
                    Game game = new Game(createPlayersForGame(players, newPlayer), cardDealerFactory.cardDealer());
                    addEnvironemntMove(game);
                    startedTables++;
                } else {
                    for (Agent plr : players) {
                        CalculateRequest calculateRequest = calculateRequestMap.get(plr);
                        if (calculateRequest.getIndex() > 0) {
                            requests.add(calculateRequest);
                            calculateRequestMap.put(plr, new CalculateRequest());
                        }
                    }
                }
            }


        }

        return mean.getResult() - 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));

    }

    public double getPlayerWinrate(Object context, Agent agent, CardDealerFactory cardDealerFactory) {
        List<Player> players = (List<Player>) context;
        Player newPlayer = (Player) agent;
        Settings.playerToTrain = newPlayer;
        for (Player player : players) {
            player.setTrain(false);
            calculateRequestMap.put(player, new CalculateRequest());
            player.resetTotalReward();
        }
        if (agent != null) {
            newPlayer.setTrain(false);
            calculateRequestMap.put(newPlayer, new CalculateRequest());
            newPlayer.resetTotalReward();
        }


        int startedTables = 0;
        int tablesToRun = Settings.MAXIMUM_TABLES_TO_RUN;
        int finishedTables = 0;

        StandardDeviation standardDeviation = new StandardDeviation();
        Mean mean = new Mean();

        long start = System.currentTimeMillis();

        int handsSinceLastIncrement = 0;
        double agentScore = 0;

        while (finishedTables < tablesToRun) {
            if (!requests.isEmpty()) {
                CalculateRequest calculateRequest = requests.remove(0);
                int[] moves = calcuateMoves(calculateRequest);


                for (int i = 0; i < calculateRequest.getIndex(); i++) {
                    Game game = (Game) calculateRequest.getEnvironments()[i];
                    game.playSingleMove(moves[i]);

                    if (game.needNewInstance) {

                        Player player = game.getPlayers().get(game.getPlayers().indexOf(newPlayer));
                        double agentScoreFromCurrentGame = game.getAgentScore(player, true) * 100;
                        agentScore += agentScoreFromCurrentGame;
                        handsSinceLastIncrement++;
                        if (handsSinceLastIncrement == 100) {
                            standardDeviation.increment(agentScore);
                            mean.increment(agentScore);
                            agentScore = 0;
                            handsSinceLastIncrement = 0;
                        }

                        newPlayer.addToTotalReward(agentScoreFromCurrentGame);


                        if (game.getRound() == 6) {
                            finishedTables++;
                            if (finishedTables > Settings.MINIMUM_TABLES_TO_RUN) {
                                double minInterval = mean.getResult() - 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));
                                double maxInterval = mean.getResult() + 3.291 * (standardDeviation.getResult() / FastMath.sqrt(standardDeviation.getN()));

                                if (minInterval >= 0) {
                                    return mean.getResult();
                                }
                                if (maxInterval < 0) {
                                    return mean.getResult();
                                }
                            }
                        } else {
                            game.initializeBeforeEpoch();
                            addEnvironemntMove(game);
                        }
                    } else {
                        addEnvironemntMove(game);
                    }
                }
            } else {
                if (startedTables < tablesToRun) {
                    Game game = new Game(createPlayersForGame(players, newPlayer), cardDealerFactory.cardDealer());
                    addEnvironemntMove(game);
                    startedTables++;
                } else {
                    boolean nonFullRequestAdded = false;
                    for (Agent plr : players) {
                        CalculateRequest calculateRequest = calculateRequestMap.get(plr);
                        if (calculateRequest.getIndex() > 0) {
                            requests.add(calculateRequest);
                            calculateRequestMap.put(plr, new CalculateRequest());
                            nonFullRequestAdded = true;
                        }
                    }
                }
            }


        }
        return mean.getResult();
    }

    public void simplePlayout(Object context, Agent agent, int steps, CardDealerFactory cardDealerFactory) {
        List<Player> players = (List<Player>) context;
        Player newAgent = (Player) agent;
        Settings.playerToTrain = newAgent;

        for (Player player : players) {
            if (player.equals(newAgent)) {
                player.setTrain(true);
            } else {
                player.setTrain(false);
            }

            player.resetTotalReward();

        }

        Game game = new Game(createPlayersForGame(players, newAgent), cardDealerFactory.cardDealer());

        for (int i = 0; i < steps; i++) {
            Agent agentToAct = game.getAgentToAct();
            int action = agentToAct.getAction(game);

            game.playSingleMove(action);
            if (game.needNewInstance) {
                game = new Game(createPlayersForGame(players, newAgent), cardDealerFactory.cardDealer());
            }
            if (!agentToAct.equals(newAgent)) {
                i--;
            }
        }
    }

}
