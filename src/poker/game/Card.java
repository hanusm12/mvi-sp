package poker.game;

import java.io.Serializable;
import java.util.List;

import poker.enums.Color;

/**
 * @author bimbo
 */
public class Card implements Comparable<Card>, Serializable {

    public static Card[][] cards;
    private final int value;
    private final Color color;

    static {
        cards = new Card[4][15];
        for (int i = 2; i < 15; i++) {
            cards[0][i] = new Card(i, Color.SPADE);
            cards[1][i] = new Card(i, Color.HEART);
            cards[2][i] = new Card(i, Color.CLUB);
            cards[3][i] = new Card(i, Color.DIAMOND);
        }
    }

    public Card(int value, Color color) {
        if (value < 2 || value > 14) {
            throw new IllegalArgumentException("Value of card has to be in range 2-14");
        }
        this.value = value;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        if (value != card.value) return false;
        return color == card.color;

    }

    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        switch (value) {
            case 10:
                out.append("T");
                break;
            case 11:
                out.append("J");
                break;
            case 12:
                out.append("Q");
                break;
            case 13:
                out.append("K");
                break;
            case 14:
                out.append("A");
                break;
            default:
                out.append(value);
                break;
        }
        switch (color) {
            case CLUB:
                out.append("c");
                break;
            case DIAMOND:
                out.append("d");
                break;
            case HEART:
                out.append("h");
                break;
            case SPADE:
                out.append("s");
                break;
        }
        return out.toString();
    }

    public String valueAsString() {
        switch (value) {
            case 10:
                return ("T");
            case 11:
                return("J");
            case 12:
                return("Q");
            case 13:
                return("K");
            case 14:
                return("A");
            default:
                return(String.valueOf(value));
        }
    }

    public int getValue() {
        return value;
    }

    public int getValueForArrays() {
        return value-2;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public int compareTo(Card o) {
        if (value==o.getValue()) {
            return color.compareTo(o.getColor());
        }
        return (Integer.compare(value, o.getValue()));
    }

    public int getIntRepresentationForTwoPlusTwo() {
        return (value-2)*4 + color.ordinal() + 1;
    }
}
