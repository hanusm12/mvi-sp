package poker.game;

import agent.Agent;
import environment.Environment;
import org.nd4j.linalg.api.ndarray.INDArray;

public class CalculateRequest {
    public static int BATCH_SIZE = 512;

    private Agent agents[] = new Agent[BATCH_SIZE];
    private Environment environments[] = new Environment[BATCH_SIZE];
    private int index = 0;

    public void add(Agent agent, Environment environment) {
        agents[index] = agent;
        environments[index] = environment;
        index++;
    }

    public boolean isComplete() {
        return index == BATCH_SIZE;
    }

    public Agent[] getAgents() {
        return agents;
    }

    public int getIndex() {
        return index;
    }

    public void setAgents(Agent[] agents) {
        this.agents = agents;
    }

    public Environment[] getEnvironments() {
        return environments;
    }

    public void setEnvironments(Environment[] environments) {
        this.environments = environments;
    }
}
