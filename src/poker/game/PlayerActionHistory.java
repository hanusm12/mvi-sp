package poker.game;

import org.nd4j.linalg.api.ndarray.INDArray;
import poker.player.Player;
import poker.player.inputDataCreation.State;

import java.util.Arrays;
import java.util.BitSet;

public class PlayerActionHistory {
    private int move;
    private State currentState;
    private int chipsAtTheTime;
    private Player player;
    private INDArray output;

    public PlayerActionHistory(int move, State inputs, int chipsAtTheTime, Player player, INDArray output) {
        this.move = move;
        this.currentState = inputs;
        this.chipsAtTheTime = chipsAtTheTime;
        this.player = player;
        this.output = output;
    }

    public INDArray getOutput() {
        return output;
    }

    public void setOutput(INDArray output) {
        this.output = output;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getMove() {
        return move;
    }

    public int getChipsAtTheTime() {
        return chipsAtTheTime;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }


}
