/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poker.game;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author bimbo
 */
public class Board implements Serializable {

    private Card flop[];
    private Card turn;
    private Card river;


    @Override
    public String toString() {
        if (flop == null) {
            return "";
        }
        return flop[0] + " " + flop[1] + " " + flop[2] + " " + turn + " " + river;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(flop, board.flop)) return false;
        if (turn != null && !turn.equals(board.turn)) return false;
        if (river != null && !river.equals(board.river)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(flop);
        if (turn != null) {
            result = 31 * result + turn.hashCode();
        }
        if (river != null) {
            result = 31 * result + river.hashCode();
        }
        return result;
    }

    public Board() {
    }

    public Card[] getFlop() {
        return flop;
    }

    public void setFlop(Card[] flop) {
        this.flop = flop;
        sortFlop();
    }

    private void sortFlop() {
        /*
        Since we are sorting only 3 values, there is no need to to call Arrays.sort (it is slower than this!)
         */
        if (flop != null) {
            //First, swap first and second element
            if (this.flop[0].getValue() > this.flop[1].getValue()) {
                Card tmp = this.flop[0];
                this.flop[0] = this.flop[1];
                this.flop[1] = tmp;
            }
            //Then, swap second and third
            if (this.flop[1].getValue() > this.flop[2].getValue()) {
                Card tmp = this.flop[1];
                this.flop[1] = this.flop[2];
                this.flop[2] = tmp;

                //And if we swapped second and third, lets swap first and second again
                if (this.flop[0].getValue() > this.flop[1].getValue()) {
                    tmp = this.flop[0];
                    this.flop[0] = this.flop[1];
                    this.flop[1] = tmp;
                }
            }

        }
    }

    public Card getTurn() {
        return turn;
    }

    public void setTurn(Card turn) {
        this.turn = turn;
    }

    public Card getRiver() {
        return river;
    }

    public void setRiver(Card river) {
        this.river = river;
    }
}
