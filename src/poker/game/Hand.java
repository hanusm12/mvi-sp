package poker.game;

import poker.player.Player;

public class Hand implements Comparable<Hand> {
    public Player player;
    public int handCategory;
    public int rankWithinCategory;

    public Hand(Player player, int handCategory, int rankWithinCategory) {
        this.player = player;
        this.handCategory = handCategory;
        this.rankWithinCategory = rankWithinCategory;
    }

    @Override
    public int compareTo(Hand o) {
        int res = -Integer.compare(handCategory, o.handCategory);
        if (res == 0) {
            res = -Integer.compare(rankWithinCategory, o.rankWithinCategory);
        }
        return res;
    }
}
