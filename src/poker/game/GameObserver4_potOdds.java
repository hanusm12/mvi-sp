package poker.game;

import agent.Agent;
import environment.AbstractEnvironmentObserver;
import environment.Environment;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import poker.player.Player;
import settings.TwoPlusTwo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameObserver4_potOdds extends AbstractEnvironmentObserver implements Serializable {

    public GameObserver4_potOdds(int numberOfHistoryStates) {
        super(numberOfHistoryStates, 47, 19, 3);
    }

    @Override
    public void fillCurrentState(Environment environment, Agent agent, double[] currentState) {
        Player player = (Player) agent;
        Game game = (Game) environment;

        int startIndex = 0;
        currentState[startIndex + player.getHigherCard().getValueForArrays()] = 1;
        startIndex += 13;
        currentState[startIndex + player.getLowerCard().getValueForArrays()] = 1;
        startIndex += 13;
        if (player.isHoleCardsSuited() == 1) {
            currentState[startIndex] = 1;
        } else {
            currentState[startIndex + 1] = 1;
        }
        startIndex += 2;

        currentState[startIndex + game.getState()] = 1;
        startIndex += 4;

        startIndex = fillHandCategory(currentState, startIndex, game, player);
        startIndex = fillPotOdds(currentState, startIndex, game, player);
    }

    private int fillPotOdds(double[] currentState, int startIndex, Game game, Player player) {
        int toCall = game.getMinimumSize() - player.getChipsIngameThisRound();
        double potOdds = toCall / (double) (toCall + game.getPotSize());

        //split into buckets
        double buckets[] = new double[]{0, 0.1, 0.2, 0.3, 0.4, 0.5};
        int index = 0;
        for (int i = 0; i < buckets.length; i++) {
            if (potOdds <= buckets[i]) {
                index = i;
                break;
            }
        }
        currentState[startIndex + index] = 1;
        return startIndex + 6;
    }

    @Override
    public double[] getHistoryRepresentation(Environment environment, Agent agent) {
        Game game = (Game) environment;
        Player player = (Player) agent;
        int startIndex = 0;
        double historyState[] = new double[getHistoryStateSize()];

        historyState[startIndex + game.getState()] = 1;
        startIndex += 4;

        startIndex = fillHandCategory(historyState, startIndex, game, player);
        startIndex = fillPotOdds(historyState, startIndex, game, player);
        return historyState;
    }

    private int fillHandCategory(double[] currentState, int index, Game game, Player player) {
        List<Integer> cardList = new ArrayList<>();
        cardList.add(player.getHigherCard().getIntRepresentationForTwoPlusTwo());
        cardList.add(player.getLowerCard().getIntRepresentationForTwoPlusTwo());
        if (game.getBoard() != null && game.getBoard().getFlop() != null) {
            cardList.add(game.getBoard().getFlop()[0].getIntRepresentationForTwoPlusTwo());
            cardList.add(game.getBoard().getFlop()[1].getIntRepresentationForTwoPlusTwo());
            cardList.add(game.getBoard().getFlop()[2].getIntRepresentationForTwoPlusTwo());
            if (game.getBoard().getTurn() != null) {
                cardList.add(game.getBoard().getTurn().getIntRepresentationForTwoPlusTwo());
                if (game.getBoard().getRiver() != null) {
                    cardList.add(game.getBoard().getRiver().getIntRepresentationForTwoPlusTwo());
                }
            }
        }

        if (cardList.size() == 2) {
            //Preflop
            if (player.getHigherCard().getValue() == player.getLowerCard().getValue()) {
                currentState[index + 1] = 1;
            } else {
                currentState[index] = 1;
            }
        } else {
            int handRank = 0;
            if (cardList.size() == 5) {
                handRank = TwoPlusTwo.lookupHand5(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4)});
            } else if (cardList.size() == 6) {
                handRank = TwoPlusTwo.lookupHand6(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4), cardList.get(5)});
            } else if (cardList.size() == 7) {
                handRank = TwoPlusTwo.lookupHand7(new int[]{cardList.get(0), cardList.get(1), cardList.get(2), cardList.get(3), cardList.get(4), cardList.get(5), cardList.get(6)});
            }
            //handCategory is in 1-9 range, we need it in 0-8
            int handCategory = (handRank >> 12) - 1;
            try {
                currentState[index + handCategory] = 1;
            } catch (Exception e) {
                System.out.println("asdf");
            }
        }

        return index + 9;
    }
}
