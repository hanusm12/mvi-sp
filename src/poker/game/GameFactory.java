package poker.game;

import agent.Agent;
import environment.Environment;
import environment.EnvironmentFactory;
import poker.game.cardDealer.CardDealerFactory;
import poker.player.Player;
import settings.Settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameFactory implements EnvironmentFactory {
    private List<Player> players;
    private Player playerToTrain;
    private CardDealerFactory cardDealerFactory;

    public GameFactory(List<Player> players, Player playerToTrain, CardDealerFactory cardDealerFactory) {
        this.players = players;
        this.playerToTrain = playerToTrain;
        this.cardDealerFactory = cardDealerFactory;
    }

    @Override
    public Environment environment() {
        return new Game(createPlayersForGame(), cardDealerFactory.cardDealer());
    }

    @Override
    public Agent getAgentToTrain() {
        return playerToTrain;
    }

    @Override
    public EnvironmentFactory clone() {
        List<Player> newPlayers = new ArrayList<>();
        for (Player player : players) {
            newPlayers.add(player.clone());
        }
        Player newPlayerToTrain = newPlayers.get(newPlayers.indexOf(playerToTrain));

        return new GameFactory(newPlayers, newPlayerToTrain, cardDealerFactory.clone());
    }

    private List<Player> createPlayersForGame() {
        List<Player> newPlayersList = new ArrayList<>();

        Collections.shuffle(players);
        if (playerToTrain == null) {
            for (int i = 0; i < Settings.PLAYERS_PER_TABLE; i++) {
                newPlayersList.add(copyOfPlayer(players.get(i)));
            }
        } else {
            newPlayersList.add(copyOfPlayer(playerToTrain));
            int index = 0;
            while (newPlayersList.size() < 6) {
                Player player = players.get(index++);
                if (!player.equals(playerToTrain)) {
                    newPlayersList.add(copyOfPlayer(player));
                }
            }
        }

        return newPlayersList;
    }

    private Player copyOfPlayer(Player player) {
        return new Player(player);
    }
}
