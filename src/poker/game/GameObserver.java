package poker.game;

import agent.Agent;
import environment.AbstractEnvironmentObserver;
import environment.Environment;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import poker.player.Player;

import java.io.Serializable;

public class GameObserver extends AbstractEnvironmentObserver implements Serializable {

    public GameObserver(int numberOfHistoryStates) {
        super(numberOfHistoryStates, 28, 0, 3);
    }

    @Override
    public void fillCurrentState(Environment environment, Agent agent, double[] currentState) {
        Player player = (Player) agent;

        int startIndex = 0;
        currentState[startIndex + player.getHigherCard().getValueForArrays()] = 1;
        startIndex += 13;
        currentState[startIndex + player.getLowerCard().getValueForArrays()] = 1;
        startIndex += 13;
        if (player.isHoleCardsSuited() == 1) {
            currentState[startIndex] = 1;
        } else {
            currentState[startIndex + 1] = 1;
        }
    }

    @Override
    public double[] getHistoryRepresentation(Environment environment, Agent agent) {
        return null;
    }
}
