package poker.game;

import agent.ActionHistory;
import poker.command.GameStage;
import poker.enums.Color;
import poker.player.Player;
import settings.Settings;
import settings.TwoPlusTwo;

import java.util.*;

public class ExactWinCalculator {

    private Player playerToTrain;
    private List<AgentAction> agentActions = new ArrayList<>();
    private boolean usedHands[];
    private int handOfTrainingPlayer[];
    private int hands[][];
    private int filledCards = 0;

    public ExactWinCalculator(Player agent) {
        this.playerToTrain = agent;
    }

    public void agentDidAction(Player agentToAct, int action) {
        this.agentActions.add(new AgentAction(agentToAct, action));
    }

    public double gameEnded(Game game) {


        Map<Player, List<HandCombination>> possibleHoleCards = new HashMap<>();
        List<Player> playersInGame = new ArrayList<>();
        Map<Player, Player> originalPlayersToNewOnes = new HashMap<>();

        List<Player> players = new ArrayList<>();
        for (Player player : game.getPlayers()) {
            Player newPlayer = new Player(player);
            if (player.isIsIngame()) {
                if (!player.equals(playerToTrain)) {
                    possibleHoleCards.put(newPlayer, new ArrayList<>());
                } else {
                    playerToTrain = newPlayer;
                }
                playersInGame.add(newPlayer);
            }
            players.add(newPlayer);
            originalPlayersToNewOnes.put(player, newPlayer);
        }
        Game simulatedGame = new GameWithPredestinedCards(players, game);
        int index = 0;

        //First, lets process PF.
        for (index = 0; index < 6; index++) {
            AgentAction agentAction = agentActions.get(index);
            Player plr = originalPlayersToNewOnes.get(agentAction.agent);
            if (possibleHoleCards.containsKey(plr)) {
                //Lets find out which cards the agent would play the same
                Inputs inputs = generateAllInputs(simulatedGame, plr, agentAction.action);
                int[] actions = plr.getAI().getActions(inputs.inputs);
                List<HandCombination> possibleHoleCardsForPlayer = possibleHoleCards.get(plr);
                for (int j = 0; j < inputs.holeCards.size(); j++) {
                    if (actions[j] == agentAction.action) {
                        //So, he would play the same this hand. Lets add ALL combinations to his cards

                        HoleCards holeCards = inputs.holeCards.get(j);
                        for (Color firstCardColor : Color.values()) {
                            for (Color secondCardColor : Color.values()) {
                                Card card1 = new Card(holeCards.card1.getValue(), firstCardColor);
                                Card card2 = new Card(holeCards.card2.getValue(), secondCardColor);
                                if (isAnyCardOnBoard(game.getBoard(), card1, card2)) {
                                    continue;
                                }
                                HoleCards hc = null;
                                if (holeCards.isSuited() && firstCardColor == secondCardColor) {
                                    hc = new HoleCards(new Card(holeCards.card1.getValue(), firstCardColor), new Card(holeCards.card2.getValue(), secondCardColor));
                                } else if (!holeCards.isSuited() && firstCardColor != secondCardColor) {
                                    hc = new HoleCards(new Card(holeCards.card1.getValue(), firstCardColor), new Card(holeCards.card2.getValue(), secondCardColor));
                                }
                                if (hc != null) {
                                    ArrayList<ActionHistory> histories = new ArrayList<>(inputs.histories.get(j));
                                    possibleHoleCardsForPlayer.add(new HandCombination(hc, histories));
                                }
                            }
                        }
                    }
                }
            }
            simulatedGame.playSingleMove(agentAction.action);


        }

        //FLOP, TURN, RIVER -> 3 times
        for (int j = 0; j < 3; j++) {
            while (simulatedGame.getPlayersToAct() > 0) {
                AgentAction agentAction = agentActions.get(index++);
                Player plr = originalPlayersToNewOnes.get(agentAction.agent);

                if (possibleHoleCards.containsKey(plr)) {
                    List<HandCombination> possibleHoleCardsForPlayer = possibleHoleCards.get(plr);
                    double inputs[][] = new double[possibleHoleCardsForPlayer.size()][];
                    for (int i = 0; i < possibleHoleCardsForPlayer.size(); i++) {
                        HandCombination handCombination = possibleHoleCardsForPlayer.get(i);
                        HoleCards holeCard = handCombination.holeCards;
                        plr.addHoleCards(holeCard.card1, holeCard.card2);
                        plr.setHistoryOfStates(handCombination.historyOfState);
                        inputs[i] = plr.getObservation(simulatedGame, true);
                        plr.changeLastAction(agentAction.action);
                    }
                    int[] actions = plr.getAI().getActions(inputs);
                    int handCombinationIndex = 0;
                    for (Iterator<HandCombination> iterator = possibleHoleCardsForPlayer.iterator(); iterator.hasNext(); ) {
                        HandCombination handCombination = iterator.next();
                        if (actions[handCombinationIndex] != agentAction.action) {
                            iterator.remove();
                        }
                        handCombinationIndex++;
                    }
                }
                simulatedGame.playSingleMove(agentAction.action);
            }
        }

        double winProbability = 0;
        prepareProbabilityCalculations(possibleHoleCards.size(), simulatedGame.board);

        int opponentIndex = 0;
        int handsInTotal = 0;
        for (Map.Entry<Player, List<HandCombination>> entry : possibleHoleCards.entrySet()) {
            double winProbabilityAgainstThisPlayer = 0;
            for (HandCombination cards : entry.getValue()) {
                entry.getKey().addHoleCards(cards.holeCards.card1, cards.holeCards.card2);
                winProbabilityAgainstThisPlayer += calculateWinProbabilities(entry.getKey(), opponentIndex);
            }
            winProbabilityAgainstThisPlayer /= (double) entry.getValue().size();
            if (winProbability == 0) {
                winProbability = winProbabilityAgainstThisPlayer;
            } else {
                winProbability *= winProbabilityAgainstThisPlayer;
            }
            opponentIndex++;
        }

        double chipReward = simulatedGame.potSize * winProbability;
        playerToTrain.addChips((int) chipReward);
        return game.getAgentScore(playerToTrain, false);
    }

    private void prepareProbabilityCalculations(int numberOfOpponents, Board board) {
        usedHands = new boolean[53];
        usedHands[0] = true;

        handOfTrainingPlayer = new int[7];
        hands = new int[numberOfOpponents][7];

        if (board.getFlop() != null) {
            fillCard(board.getFlop()[0], numberOfOpponents);
            fillCard(board.getFlop()[1], numberOfOpponents);
            fillCard(board.getFlop()[2], numberOfOpponents);
        }
        fillCard(board.getTurn(), numberOfOpponents);
        fillCard(board.getRiver(), numberOfOpponents);


        int twoPlusTwo = playerToTrain.getFirstCard().getIntRepresentationForTwoPlusTwo();
        handOfTrainingPlayer[filledCards] = twoPlusTwo;
        usedHands[twoPlusTwo] = true;
        twoPlusTwo = playerToTrain.getSecondCard().getIntRepresentationForTwoPlusTwo();
        handOfTrainingPlayer[filledCards + 1] = twoPlusTwo;
        usedHands[twoPlusTwo] = true;
    }

    private void fillCard(Card card, int numberOfPlayers) {
        if (card == null) {
            return;
        }
        int twoPlusTwo = card.getIntRepresentationForTwoPlusTwo();
        usedHands[twoPlusTwo] = true;
        handOfTrainingPlayer[filledCards] = twoPlusTwo;
        for (int i = 0; i < numberOfPlayers; i++) {
            hands[i][filledCards] = twoPlusTwo;
        }
        filledCards++;
    }

    private boolean isAnyCardOnBoard(Board board, Card card1, Card card2) {
        if (board.getFlop() != null) {
            if (board.getFlop()[0].equals(card1) || board.getFlop()[0].equals(card2)) {
                return true;
            }
            if (board.getFlop()[1].equals(card1) || board.getFlop()[1].equals(card2)) {
                return true;
            }
            if (board.getFlop()[2].equals(card1) || board.getFlop()[2].equals(card2)) {
                return true;
            }
        }
        if (board.getTurn() != null) {
            if (board.getTurn().equals(card1) || board.getTurn().equals(card2)) {
                return true;
            }
        }
        if (board.getRiver() != null) {
            if (board.getRiver().equals(card1) || board.getRiver().equals(card2)) {
                return true;
            }
        }
        return false;
    }


    private double calculateWinProbabilities(Player opponent, int opponentIndex) {
        int twoPlusTwo1 = opponent.getFirstCard().getIntRepresentationForTwoPlusTwo();
        int twoPlusTwo2 = opponent.getSecondCard().getIntRepresentationForTwoPlusTwo();

        hands[opponentIndex][filledCards++] = twoPlusTwo1;
        hands[opponentIndex][filledCards++] = twoPlusTwo2;

        usedHands[twoPlusTwo1] = true;
        usedHands[twoPlusTwo2] = true;


        int handsToAdd = 7 - filledCards;
        int simulationsToRun = handsToAdd * Settings.ALLIN_SIMULATIONS_PER_HAND_MISSING;
        if (simulationsToRun == 0) {
            simulationsToRun = 1;
        }

        double sum = 0;

        for (int i = 0; i < simulationsToRun; i++) {
            while (filledCards < 7) {
                int selectedCard = Settings.RANDOM.nextInt(52) + 1;
                if (!usedHands[selectedCard]) {
                    hands[opponentIndex][filledCards] = selectedCard;
                    handOfTrainingPlayer[filledCards] = selectedCard;
                    usedHands[selectedCard] = true;
                    filledCards++;
                }
            }


            int trainingPlayerHand = TwoPlusTwo.lookupHand7(handOfTrainingPlayer);
            int opponentHand = TwoPlusTwo.lookupHand7(hands[opponentIndex]);

            int trainingPlayerHandCategory = trainingPlayerHand >> 12;
            int trainingPlayerRank = trainingPlayerHand & 0x00000FFF;

            int opponentPlayerHandCategory = opponentHand >> 12;
            int opponentPlayerRank = opponentHand & 0x00000FFF;


            if (trainingPlayerHandCategory > opponentPlayerHandCategory) {
                sum += 1;
            } else if (trainingPlayerHandCategory == opponentPlayerHandCategory) {
                if (trainingPlayerRank > opponentPlayerRank) {
                    sum += 1;
                } else if (trainingPlayerRank == opponentPlayerRank) {
                    sum += 0.5;
                }
            }


            for (int j = 0; j < handsToAdd; j++) {
                usedHands[hands[opponentIndex][6 - j]] = false;
            }
            filledCards -= handsToAdd;
        }


        usedHands[twoPlusTwo1] = false;
        usedHands[twoPlusTwo2] = false;
        filledCards -= 2;
        return sum / (double) simulationsToRun;
    }

    private Inputs generateAllInputs(Game game, Player player, int action) {

        double inputs[][] = new double[169][];
        List<HoleCards> holeCards = new ArrayList<>();
        List<List<ActionHistory>> histories = new ArrayList<>();
        int index = 0;
        for (int i = 2; i <= 14; i++) {
            for (int j = 2; j <= 14; j++) {
                Card card1;
                Card card2;
                if (j < i) {
                    card1 = new Card(i, Color.CLUB);
                    card2 = new Card(j, Color.CLUB);
                } else {
                    card1 = new Card(i, Color.CLUB);
                    card2 = new Card(j, Color.DIAMOND);
                }
                player.addHoleCards(card1, card2);
                player.setHistoryOfStates(new ArrayList<>());
                inputs[index++] = player.getObservation(game, true);
                player.changeLastAction(action);
                histories.add(player.getHistoryOfStates());
                holeCards.add(new HoleCards(card1, card2));
            }
        }
        return new Inputs(inputs, holeCards, histories);
    }

    private class Inputs {
        public double inputs[][];
        public List<HoleCards> holeCards;
        public List<List<ActionHistory>> histories;

        public Inputs(double inputs[][], List<HoleCards> holeCards, List<List<ActionHistory>> histories) {
            this.inputs = inputs;
            this.holeCards = holeCards;
            this.histories = histories;
        }
    }

    private class HandCombination {
        public HoleCards holeCards;
        public List<ActionHistory> historyOfState;

        public HandCombination(HoleCards holeCards, List<ActionHistory> historyOfState) {
            this.holeCards = holeCards;
            this.historyOfState = historyOfState;
        }
    }


    private class AgentAction {
        public Player agent;
        public int action;

        AgentAction(Player agent, int action) {
            this.agent = agent;
            this.action = action;
        }
    }

    private static class HoleCards {
        private Card card1;
        private Card card2;

        public HoleCards(Card card1, Card card2) {
            this.card1 = card1;
            this.card2 = card2;
        }

        public boolean isSuited() {
            return card1.getColor() == card2.getColor();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HoleCards holeCards = (HoleCards) o;

            if (!card1.equals(holeCards.card1)) return false;
            return card2.equals(holeCards.card2);
        }

        @Override
        public int hashCode() {
            int result = card1.hashCode();
            result = 31 * result + card2.hashCode();
            return result;
        }
    }


    private class GameWithPredestinedCards extends Game {
        private Game originalGame;

        public GameWithPredestinedCards(List<Player> plrs, Game originalGame) {
            super(plrs, originalGame.getCardDealer());
            this.originalGame = originalGame;
            copyOriginalGame();
        }

        private void copyOriginalGame() {
            dealer = originalGame.getDealer();
            currentPlayer = dealer;
            for (int i = 0; i < 6; i++) {
                Player player = nextPlayer();
                player.setPositionOnTable(i);
            }

            setPlayersChipsToDefaultStack();

            cardDealer.reset();

            minimumSize = Settings.STARTING_SMALL_BLIND * 2;
            currentPlayer = dealer;
            potSize = 0;
            board = new Board();
            playersInGame = players.size();
            playersToAct = players.size();
            playersAllIn = 0;

            for (int i = 0; i < players.size(); i++) {
                Player plr = players.get(i);
                plr.setChipsIngameThisRound(0);
                plr.setIsIngame(true);
                plr.setIsAllIn(false);
                plr.setChipsInGame(0);
                plr.addHoleCards(originalGame.getPlayers().get(i).getFirstCard(), originalGame.getPlayers().get(i).getSecondCard());
                plr.setActedThisBettingRound(false);
            }
            postBlinds();
            endHandNow = false;

            actingPlayer = nextInGamePlayer();
        }



        @Override
        public void playSingleMove(int moveIndex) {
            while (currentStage.isContinueInSimulation()) {
                this.currentAgentMove = moveIndex;
                currentStage = COMMANDS_MAP.get(currentStage.getNextGameStage()).exec(this);
            }

            currentStage.setContinueInSimulation(true);
            if (currentStage.getNextGameStage() == GameStage.END_GAME) {
                needNewInstance = true;
            }

        }

        @Override
        public void dealFlop() {
            board.setFlop(originalGame.getBoard().getFlop());
        }

        @Override
        public void dealTurn() {
            board.setTurn(originalGame.getBoard().getTurn());
        }

        @Override
        public void dealRiver() {
            board.setRiver(originalGame.getBoard().getRiver());
        }
    }
}
