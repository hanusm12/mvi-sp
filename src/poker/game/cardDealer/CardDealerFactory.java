package poker.game.cardDealer;

public interface CardDealerFactory {
    CardDealer cardDealer();
    CardDealerFactory clone();
}
