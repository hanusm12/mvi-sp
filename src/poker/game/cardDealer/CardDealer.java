package poker.game.cardDealer;

import javafx.print.PageLayout;
import poker.enums.Color;
import poker.game.Card;
import poker.player.Player;

public interface CardDealer {
    void reset();
    void cardNotInPlay(Card card);
    Card getBoardCard();
    Card[] getHoleCards(Player player);
}
