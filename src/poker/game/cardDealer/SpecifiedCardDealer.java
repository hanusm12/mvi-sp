package poker.game.cardDealer;

import org.nd4j.linalg.indexing.SpecifiedIndex;
import poker.enums.Color;
import poker.game.Card;
import poker.player.Player;
import settings.Settings;

import java.util.Random;

public class SpecifiedCardDealer extends RandomCardDealer {
    private int card1Val;
    private int card2Val;
    private boolean suited;

    public SpecifiedCardDealer(int card1Val, int card2Val, boolean suited) {
        this.card1Val = card1Val;
        this.card2Val = card2Val;
        this.suited = suited;
    }

    @Override
    public Card[] getHoleCards(Player player) {
        Card cards[] = new Card[2];
        if (player.equals(Settings.playerToTrain)) {
            if (suited) {
                int color = Settings.RANDOM.nextInt(4);
                inPlay[color][card1Val] = true;
                inPlay[color][card2Val] = true;
                cards[0] = Card.cards[color][card1Val];
                cards[1] = Card.cards[color][card2Val];
            } else {
                int color1 = Settings.RANDOM.nextInt(4);
                int color2 = Settings.RANDOM.nextInt(4);
                while (color1 == color2) {
                    color2 = Settings.RANDOM.nextInt(4);
                }

                inPlay[color1][card1Val] = true;
                inPlay[color2][card2Val] = true;
                cards[0] = Card.cards[color1][card1Val];
                cards[1] = Card.cards[color2][card2Val];
            }
        } else {
            cards[0] = getBoardCard();
            cards[1] = getBoardCard();
        }
        return cards;
    }
}
