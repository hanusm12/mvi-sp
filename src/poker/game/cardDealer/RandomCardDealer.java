package poker.game.cardDealer;

import poker.enums.Color;
import poker.game.Card;
import poker.player.Player;
import settings.Settings;

public class RandomCardDealer implements CardDealer {
    protected boolean inPlay[][] = new boolean[4][15];

    @Override
    public void reset() {
        for (int i = 2; i < 15; i++) {
            inPlay[0][i] = false;
            inPlay[1][i] = false;
            inPlay[2][i] = false;
            inPlay[3][i] = false;
        }
    }

    @Override
    public void cardNotInPlay(Card card) {
        inPlay[card.getColor().ordinal()][card.getValue()] = false;
    }

    @Override
    public Card getBoardCard() {
        int color = Settings.RANDOM.nextInt(4);
        int value = Settings.RANDOM.nextInt(13) + 2;
        while (inPlay[color][value] == true) {
            color = Settings.RANDOM.nextInt(4);
            value = Settings.RANDOM.nextInt(13) + 2;
        }
        inPlay[color][value] = true;
        return Card.cards[color][value];
    }

    @Override
    public Card[] getHoleCards(Player player) {
        Card cards[] = new Card[2];
        cards[0] = getBoardCard();
        cards[1] = getBoardCard();
        return cards;
    }
}
