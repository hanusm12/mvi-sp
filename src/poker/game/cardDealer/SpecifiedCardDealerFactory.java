package poker.game.cardDealer;

public class SpecifiedCardDealerFactory implements CardDealerFactory {
    private int card1val;
    private int card2val;
    private boolean suited;

    public SpecifiedCardDealerFactory(int card1val, int card2val, boolean suited) {
        this.card1val = card1val;
        this.card2val = card2val;
        this.suited = suited;
    }

    @Override
    public CardDealer cardDealer() {
        return new SpecifiedCardDealer(card1val, card2val, suited);
    }

    @Override
    public CardDealerFactory clone() {
        return new SpecifiedCardDealerFactory(card1val, card2val, suited);
    }
}
