package poker.game.cardDealer;

public class RandomCardDealerFactory implements CardDealerFactory {
    @Override
    public CardDealer cardDealer() {
        return new RandomCardDealer();
    }

    @Override
    public CardDealerFactory clone() {
        return new RandomCardDealerFactory();
    }
}
